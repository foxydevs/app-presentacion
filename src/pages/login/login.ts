import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';

//IMPORTAR SERVICIOS
import { AuthService } from '../../app/service/auth.service';
import { UsersService } from '../../app/service/users.service';
import { UserAuthService } from '../../app/service/user-auth.service';
import { path } from "./../../app/config.module";
import { Events } from '../../../node_modules/ionic-angular/util/events';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  //PROPIEDADES
  public btnDisabled:boolean;
  public baseId:number = path.id;
  public baseUser:any;
  public pictureLogin:string = localStorage.getItem('currentPictureLogin');
  public getOneSignalID:string = localStorage.getItem('currentOneSignalID');
  public user:any = {
    username:"",
    password: ""
  }
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public authentication: AuthService,
    public secondService: UserAuthService,
    public events: Events,
    public usersService: UsersService) {
      this.btnDisabled = false;
      this.loadSingleUser();
  }

  public loadSingleUser(){

      if((this.baseId+'')!='null'){
        this.usersService.getSingle(this.baseId)
        .then(response => {
          this.baseUser = response;
          if(response.pic1) {
            this.pictureLogin = response.pic1.toString();
          } else {
            this.pictureLogin = 'https://verdict4u.files.wordpress.com/2016/09/google-now-wallpaper-2.png'
          }
          localStorage.setItem('currentPictureLogin', response.pic1);
          localStorage.setItem('currentpictureCover', response.pic3);
          localStorage.setItem('currentPictureCategories', response.pic2);
          localStorage.setItem('currentPicture11', response.picture);
          localStorage.setItem('currentIdYoutube', response.youtube_channel);
          localStorage.setItem('currentColor', response.color);
          localStorage.setItem('currentColorButton', response.color_button);
          localStorage.setItem('currentHome', response.opcion2);
          localStorage.setItem('currentAppMembresia', response.opcion3);
          localStorage.setItem('currentDesign', response.opcion4);
          localStorage.setItem('currentOrders', response.opcion5);
          localStorage.setItem('currentDesignTwo', response.opcion6);
          localStorage.setItem('currentShopCart', response.opcion7);
          localStorage.setItem('currentDesignCategory', response.opcion8);
          localStorage.setItem('currentNew', response.opcion9);
          localStorage.setItem('currentIOS', response.opcion18);
          localStorage.setItem('currentAndroid', response.opcion17);
          localStorage.setItem('currentOpcion19', response.opcion19);
          localStorage.setItem('currentOpcion21', response.opcion21);
          localStorage.setItem('currentWhatsapp', response.opcion26);
          localStorage.setItem('currentDeposito', response.opcion27);
          localStorage.setItem('cart', JSON.stringify([]));
          this.btnColor = response.color_button;
          if(response.opcion1) {
            localStorage.setItem('currentCurrency', response.opcion1);
          } else {
            localStorage.setItem('currentCurrency', 'Q');            
          }
          //console.clear();
        }).catch(error => {
          localStorage.removeItem('currentPictureLogin');
          localStorage.removeItem('currentPictureLogin');
          localStorage.removeItem('currentpictureCover');
          localStorage.removeItem('currentPictureCategories');
          localStorage.removeItem('currentPicture11');
          localStorage.removeItem('currentIdYoutube');
        })
      }else{
        localStorage.removeItem('currentPictureLogin');
        localStorage.removeItem('currentPictureLogin');
        localStorage.removeItem('currentpictureCover');
        localStorage.removeItem('currentPictureCategories');
        localStorage.removeItem('currentPicture11');
        localStorage.removeItem('currentIdYoutube');
      }
  }

  public logIn() {
    let events = this.events;
    if(this.user.username || this.user.password) {
      if(this.user.username) {
        if(this.user.password) {
          this.btnDisabled = true;
          if(this.baseId>0){
            this.authentication.authenticationID(this.user,this.baseId)
            .then(response => {
               console.log(response);
              if(response.state > 0) {
                if(response.id && this.getOneSignalID) {
                  let data = {
                    id: response.id,
                    one_signal_id: this.getOneSignalID
                  }
                  this.update(data)
                }
                let type:string = null;
                localStorage.setItem('currentUser', response.username);
                localStorage.setItem('currentEmail', response.email);
                localStorage.setItem('currentId', response.id);
                localStorage.setItem('currentState', response.estado);                
                localStorage.setItem('currentPicture', response.picture);
                localStorage.setItem('currentFirstName', response.firstname);
                localStorage.setItem('currentLastName', response.lastname);
                localStorage.setItem('currentState', response.state);
                localStorage.setItem('currentMembresia', response.tipoNivel);
                localStorage.setItem('currentMembresiaFin', response.finMembresia)
                localStorage.setItem('currentMembresiaFin', response.finMembresia)
                localStorage.setItem('currentUUID', response.opcion15);
                localStorage.setItem('currentTypeMembership', response.opcion13);
                localStorage.setItem('currentAuthentication', 'Authentication');
                //this.secondService.createNewUser(response)
                //this.users$ = this.secondService.getUsers();
                switch(+response.type) {
                  case 1:{
                    if(response.id == this.baseId){
                      type = 'user';
                      this.loading.create({
                        content: "Iniciando Sesión Usuario...",
                        duration: 500
                      }).present();
                      localStorage.setItem('currentRolId', response.type);
                      events.publish('user:updates');
                      break;
                    } else {
                      type = 'client';
                      localStorage.setItem('currentRolId', '2');
                      events.publish('user:update');
                      break;
                    }
                  }
                  case 2:{
                    type = 'client';
                    localStorage.setItem('currentRolId', response.type);
                    events.publish('user:update');
                    break;
                  }
                  default:{
                    type = 'usuario';
                    break;
                  }
                }
                localStorage.setItem('currentType', type);
                //console.clear();
                this.btnDisabled = false;

              } else {
                this.btnDisabled = false;
                this.toast.create({
                  message: "Su usuario se encuentra deshabilitado temporalmente.",
                  duration: 3000
                }).present();
              }
            }).catch(error => {
              //console.clear();
              this.btnDisabled = false;
              if(error.status == 401){
                this.btnDisabled = false;
                this.toast.create({
                  message: "Usuario o contraseña incorrectos.",
                  duration: 3000
                }).present();
              }else
              if(error.status == 400){
                this.btnDisabled = false;
                this.toast.create({
                  message: "Su usuario no esta registrado en esta Aplicacion.",
                  duration: 3000
                }).present();
              }
            });
          }else{
            this.authentication.authentication(this.user)
            .then(response => {
              if(response.state > 0) {
                let type:string = null;
                localStorage.setItem('currentUser', response.username);
                localStorage.setItem('currentEmail', response.email);
                localStorage.setItem('currentId', response.id);
                localStorage.setItem('currentState', response.estado);
                localStorage.setItem('currentRolId', response.type);
                localStorage.setItem('currentPicture', response.picture);
                localStorage.setItem('currentFirstName', response.firstname);
                localStorage.setItem('currentLastName', response.lastname);
                localStorage.setItem('currentMembresia', response.tipoNivel);
                localStorage.setItem('currentMembresiaFin', response.finMembresia)
                switch(+response.type) {
                  case 1:{
                    type = 'user';
                    this.loading.create({
                      content: "Iniciando Sesión Usuario...",
                      duration: 500
                    }).present();
                    events.publish('user:updates');
                    break;
                  }
                  case 2:{
                    type = 'client';
                    events.publish('user:update');
                    break;
                  }
                  default:{
                    type = 'usuario';
                    break;
                  }
                }
                localStorage.setItem('currentType', type);
                this.btnDisabled = false;
              } else {
                this.btnDisabled = false;
                this.toast.create({
                  message: "Su usuario se encuentra deshabilitado temporalmente.",
                  duration: 3000
                }).present();
              }
            }).catch(error => {
              if(error.status == 401){
                this.btnDisabled = false;
                this.toast.create({
                  message: "Usuario o contraseña incorrectos.",
                  duration: 3000
                }).present();
              }
            });
          }

        } else {
          this.toast.create({
            message: "Ingrese una contraseña.",
            duration: 3000
          }).present();
        }
      } else {
        this.toast.create({
          message: "Ingrese un usuario.",
          duration: 3000
        }).present();
      }
    } else {
      this.toast.create({
        message: "Ingrese un usuario y contraseña.",
        duration: 3000
      }).present();
    }
  }

  public createAccount() {
    this.navCtrl.push('RegisterPage');
  }

  public resetPassword() {
    this.navCtrl.push('ResetPasswordPage');
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.usersService.update(formValue)
    .then(response => {
      console.log(response);
    }).catch(error => {
      console.clear
    });
  }

}

