import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, IonicPage, LoadingController, Events } from 'ionic-angular';
import { path } from '../../app/config.module';
import { UsersService } from '../../app/service/users.service';
import { AccessService } from '../../app/service/access.service';
import { StaffService } from '../../app/service/staff.service';

@IonicPage()
@Component({
  selector: 'menu-app',
  templateUrl: 'menu.html'
})
export class MenuPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  pages: Array<{icon:string, ios:string, title: string, apodo?: string, component: any, membresia?: any}>;
  data:any = [];
  baseIdClient:any = localStorage.getItem('currentId');
  baseIdApp:number = path.id;
  pictureCover:any;
  opcion2:any;
  appMembresia:any;
  authentication:any;
  rol:any;
  designCategory:any;

  constructor(public platform: Platform,
  public loading: LoadingController,
  public events: Events,
  public mainService: UsersService,
  public secondService: AccessService,
  public thirdService: StaffService,
  ) {
    //CARGAR USUARIO    
    this.getData();
    events.subscribe('user:update', res => {
      this.getModulesClient();
      this.getData();
    });
    events.subscribe('user:updates', res => {
      this.getModulesApp();
      this.getData();
    });
  }
  
  openLogIn() {
    this.nav.setRoot('LoginPage');
  }

  public getSingleUser(id:any){
    this.mainService.getSingle(id)
    .then(response => {
      this.data = response;
      localStorage.setItem("currentPicture", response.picture);
    }).catch(error => {
    })
  }

  public getSingleStaff(id:any, id2:any){
    this.thirdService.getStaffConfirmation(id, id2)
    .then(response => {
      if(response) {
        localStorage.setItem("currentStaff", response);
      }
    }).catch(error => {
      if(error.status == '404') {
        
      }
    })
  }

  public getSingleUserApp(id:any){
    this.mainService.getSingle(id)
    .then(response => {
      if(response.opcion3 == true) {
        localStorage.setItem('currentAppMembresia', '1');
      } else {
        localStorage.setItem('currentAppMembresia', '0');
      }
      this.appMembresia = localStorage.getItem('currentAppMembresia');
    }).catch(error => {
    })
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    localStorage.setItem('currentNivelMembresia', page.membresia);
    this.nav.setRoot(page.component);
  }

  logOut() {
    this.loading.create({
      content: "Cerrando Sesión...",
      duration: 1000
    }).present();
    localStorage.clear();
    this.data = []
    this.nav.setRoot('LoginPage');
    localStorage.setItem('currentAuthentication', 'NoAuthentication');
    this.authentication = localStorage.getItem('currentAuthentication');
    if(this.rol == '1') {
      this.events.publish('user:updates');
    } else {
      this.events.publish('user:update');
    }
  }

  public getModulesClient(){
    this.secondService.getAccess(this.baseIdApp)
    .then(response => {      
      if(response.permitidos.length > 0){
        this.pages = [];
        for(let x of response.permitidos) {
          if(x.modulos_show.id == '16' && x.mostrar == '1') {
            if(this.appMembresia == '1' && this.authentication == 'Authentication') {
              this.pages.push({icon: 'md-star', ios: 'ios-star', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MyMembershipClientPage', membresia: x.membresia})
            } 
          } else if(x.modulos_show.id == '1' && x.mostrar == '1') {
            if(this.designCategory!='0') {
              this.pages.push({icon: 'md-apps', ios: 'ios-apps', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CategorysCardClientPage', membresia: x.membresia})
            } else {
              this.pages.push({icon: 'md-apps', ios: 'ios-apps', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CategorysClientPage', membresia: x.membresia})
            }
          } else if(x.modulos_show.id == '2' && x.mostrar == '1') {
            this.pages.push({icon: 'md-albums', ios: 'ios-albums', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ProductsClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '3' && x.mostrar == '1' && this.authentication == 'Authentication') {
            this.pages.push({icon: 'md-cart', ios: 'ios-cart', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MyOrdersClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '4' && x.mostrar == '1') {
            this.pages.push({icon: 'md-calendar', ios: 'ios-calendar', title: x.modulos_show.nombre, apodo: x.apodo, component: 'EventsClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '5' && x.mostrar == '1') {
            this.pages.push({icon: 'logo-youtube', ios: 'logo-youtube', title: x.modulos_show.nombre, apodo: x.apodo, component: 'YoutubeClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '6' && x.mostrar == '1') {
            this.pages.push({icon: 'md-globe', ios: 'ios-globe', title: x.modulos_show.nombre, apodo: x.apodo, component: 'NewsClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '7' && x.mostrar == '1') {
            if(this.baseIdApp == 49) {
              this.pages.push({icon: 'md-nutrition', ios: 'ios-nutrition', title: x.modulos_show.nombre, apodo: x.apodo, component: 'TipsClientPage', membresia: x.membresia})
            } else {
              this.pages.push({icon: 'md-alarm', ios: 'ios-alarm', title: x.modulos_show.nombre, apodo: x.apodo, component: 'TipsClientPage', membresia: x.membresia})
            }
          } else if(x.modulos_show.id == '8' && x.mostrar == '1'  && this.authentication == 'Authentication') {
            this.pages.push({icon: 'md-send', ios: 'ios-send', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MessagesClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '9'  && this.authentication == 'Authentication') {
            this.pages.push({icon: 'md-person', ios: 'ios-person', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ProfileClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '10' && x.mostrar == '1') {
            if(this.baseIdApp == 88) {
              this.pages.push({icon: 'md-walk', ios: 'ios-walk', title: x.modulos_show.nombre, apodo: x.apodo, component: 'WorkoutsMonthClientPage', membresia: x.membresia})
            } else {
              this.pages.push({icon: 'md-walk', ios: 'ios-walk', title: x.modulos_show.nombre, apodo: x.apodo, component: 'WorkoutsClientPage', membresia: x.membresia})
            }
          } else if(x.modulos_show.id == '11' && x.mostrar == '1') {
            this.pages.push({icon: 'md-medal', ios: 'ios-medal', title: x.modulos_show.nombre, apodo: x.apodo, component: 'LeadearboardClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '12' && x.mostrar == '1') {
            this.pages.push({icon: 'logo-rss', ios: 'logo-rss', title: x.modulos_show.nombre, apodo: x.apodo, component: 'BlogClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '13' && x.mostrar == '1') {
            this.pages.push({icon: 'md-thumbs-up', ios: 'ios-thumbs-up', title: x.modulos_show.nombre, apodo: x.apodo, component: 'SocialClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '14' && x.mostrar == '1') {
            this.pages.push({icon: 'md-play', ios: 'ios-play', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ActivePauseClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '15' && x.mostrar == '1') {
            this.pages.push({icon: 'md-pricetag', ios: 'ios-pricetag', title: x.modulos_show.nombre, apodo: x.apodo, component: 'PromotionClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '17' && x.mostrar == '1') {
            this.pages.push({icon: 'md-pricetags', ios: 'ios-pricetags', title: x.modulos_show.nombre, apodo: x.apodo, component: 'BenefitsClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '18' && x.mostrar == '1') {
            this.pages.push({icon: 'md-body', ios: 'ios-body', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MyProgressClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '19' && x.mostrar == '1') {
            this.pages.push({icon: 'md-stopwatch', ios: 'ios-stopwatch', title: x.modulos_show.nombre, apodo: x.apodo, component: 'RunningClubClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '20' && x.mostrar == '1') {
            this.pages.push({icon: 'md-people', ios: 'ios-people', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ZFitClubClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '21' && x.mostrar == '1') {
            this.pages.push({icon: 'md-planet', ios: 'ios-planet', title: x.modulos_show.nombre, apodo: x.apodo, component: 'StaffClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '22' && x.mostrar == '1') {
            this.pages.push({icon: 'md-leaf', ios: 'ios-leaf', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MotivationClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '23' && x.mostrar == '1') {
            this.pages.push({icon: 'md-alarm', ios: 'ios-alarm', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CitesClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '24' && x.mostrar == '1') {
            this.pages.push({icon: 'md-information-circle', ios: 'ios-information-circle', title: x.modulos_show.nombre, apodo: x.apodo, component: 'InformationClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '25' && x.mostrar == '1') {
            this.pages.push({icon: 'md-bookmarks', ios: 'ios-bookmarks', title: x.modulos_show.nombre, apodo: x.apodo, component: 'TipsBibliaClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '26' && x.mostrar == '1') {
            this.pages.push({icon: 'md-bookmark', ios: 'ios-bookmark', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MyBooksClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '27' && x.mostrar == '1') {
            this.pages.push({icon: 'md-albums', ios: 'ios-albums', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CategorysDocumentsClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '28' && x.mostrar == '1') {
            this.pages.push({icon: 'md-document', ios: 'ios-document', title: x.modulos_show.nombre, apodo: x.apodo, component: 'DocumentsClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '29' && x.mostrar == '1') {
            this.pages.push({icon: 'md-information-circle', ios: 'ios-information-circle', title: x.modulos_show.nombre, apodo: x.apodo, component: 'InformationTwoClientPage', membresia: x.membresia})
          }
        }
      }
      console.clear
    }).catch(error => {
      console.log(error);
    })
  }

  public getModulesApp() {
    //this.pages = [];

    //this.pages.push({icon: 'md-person', ios: 'ios-person', title: 'x.modulos_show.nombre', apodo: 'x.apodo', component: 'ProfileUserPage'})

    this.secondService.getAccess(this.baseIdApp)
    .then(response => {      
      if(response.permitidos.length > 0){
        this.pages = [];
        for(let x of response.permitidos) {
          if(x.modulos_show.id == '16' && x.mostrar == '1') {
            if(this.appMembresia == '1') {
              this.pages.push({icon: 'md-star', ios: 'ios-star', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MembershipUserPage'})
            } 
          } else if(x.modulos_show.id == '1' && x.mostrar == '1') {
            if(this.designCategory!='0') {//CategorysUserPage
              this.pages.push({icon: 'md-apps', ios: 'ios-apps', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CategorysUserPage'})
            } else {
              this.pages.push({icon: 'md-apps', ios: 'ios-apps', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CategorysCardUserPage'})
            }
          } else if(x.modulos_show.id == '2' && x.mostrar == '1') {
            this.pages.push({icon: 'md-albums', ios: 'ios-albums', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ProductsUserPage'})
          } else if(x.modulos_show.id == '3' && x.mostrar == '1') {
            this.pages.push({icon: 'md-cart', ios: 'ios-cart', title: x.modulos_show.nombre, apodo: x.apodo, component: 'OrdersUserPage'})
          } else if(x.modulos_show.id == '4' && x.mostrar == '1') {
            this.pages.push({icon: 'md-calendar', ios: 'ios-calendar', title: x.modulos_show.nombre, apodo: x.apodo, component: 'EventsUserPage'})
          } else if(x.modulos_show.id == '5' && x.mostrar == '1') {
            this.pages.push({icon: 'logo-youtube', ios: 'logo-youtube', title: x.modulos_show.nombre, apodo: x.apodo, component: 'YoutubeUserPage'})
          } else if(x.modulos_show.id == '6' && x.mostrar == '1') {
            this.pages.push({icon: 'md-globe', ios: 'ios-globe', title: x.modulos_show.nombre, apodo: x.apodo, component: 'NewsUserPage'})
          } else if(x.modulos_show.id == '7' && x.mostrar == '1') {
            if(this.baseIdApp == 49) {
              this.pages.push({icon: 'md-nutrition', ios: 'ios-nutrition', title: x.modulos_show.nombre, apodo: x.apodo, component: 'TipsUserPage'})
            } else {
              this.pages.push({icon: 'md-alarm', ios: 'ios-alarm', title: x.modulos_show.nombre, apodo: x.apodo, component: 'TipsUserPage'})
            }
          } else if(x.modulos_show.id == '8' && x.mostrar == '1') {
            this.pages.push({icon: 'md-send', ios: 'ios-send', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MessagesTabPage'})
          } else if(x.modulos_show.id == '9') {
            this.pages.push({icon: 'md-person', ios: 'ios-person', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ProfileUserPage'})
          } else if(x.modulos_show.id == '10' && x.mostrar == '1') {
            this.pages.push({icon: 'md-walk', ios: 'ios-walk', title: x.modulos_show.nombre, apodo: x.apodo, component: 'WorkoutsUserPage'})
          } else if(x.modulos_show.id == '11' && x.mostrar == '1') {
            this.pages.push({icon: 'md-medal', ios: 'ios-medal', title: x.modulos_show.nombre, apodo: x.apodo, component: 'LeadearboardUserPage'})
          } else if(x.modulos_show.id == '12' && x.mostrar == '1') {
            this.pages.push({icon: 'logo-rss', ios: 'logo-rss', title: x.modulos_show.nombre, apodo: x.apodo, component: 'BlogUserPage'})
          } else if(x.modulos_show.id == '13' && x.mostrar == '1') {
            this.pages.push({icon: 'md-thumbs-up', ios: 'ios-thumbs-up', title: x.modulos_show.nombre, apodo: x.apodo, component: 'SocialUserPage'})
          } else if(x.modulos_show.id == '14' && x.mostrar == '1') {
            this.pages.push({icon: 'md-play', ios: 'ios-play', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ActivePauseUserPage'})
          } else if(x.modulos_show.id == '15' && x.mostrar == '1') {
            this.pages.push({icon: 'md-pricetag', ios: 'ios-pricetag', title: x.modulos_show.nombre, apodo: x.apodo, component: 'PromotionUserPage'})
          } else if(x.modulos_show.id == '17' && x.mostrar == '1') {
            this.pages.push({icon: 'md-pricetags', ios: 'ios-pricetags', title: x.modulos_show.nombre, apodo: x.apodo, component: 'BenefitsUserPage'})
          } else if(x.modulos_show.id == '18' && x.mostrar == '1') {
            this.pages.push({icon: 'md-body', ios: 'ios-body', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ProgressUserPage'})
          } else if(x.modulos_show.id == '19' && x.mostrar == '1') {
            this.pages.push({icon: 'md-stopwatch', ios: 'ios-stopwatch', title: x.modulos_show.nombre, apodo: x.apodo, component: 'RunningClubUserPage'})
          } else if(x.modulos_show.id == '20' && x.mostrar == '1') {
            this.pages.push({icon: 'md-people', ios: 'ios-people', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ZFitClubUserPage'})
          } else if(x.modulos_show.id == '21' && x.mostrar == '1') {
            this.pages.push({icon: 'md-planet', ios: 'ios-planet', title: x.modulos_show.nombre, apodo: x.apodo, component: 'StaffUserPage'})
          } else if(x.modulos_show.id == '22' && x.mostrar == '1') {
            this.pages.push({icon: 'md-leaf', ios: 'ios-leaf', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MotivationsUserPage'})
          } else if(x.modulos_show.id == '23' && x.mostrar == '1') {
            this.pages.push({icon: 'md-alarm', ios: 'ios-alarm', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CitesUserPage'})
          } else if(x.modulos_show.id == '24' && x.mostrar == '1') {
            this.pages.push({icon: 'md-information-circle', ios: 'ios-information-circle', title: x.modulos_show.nombre, apodo: x.apodo, component: 'InformationUserPage'})
          } else if(x.modulos_show.id == '25' && x.mostrar == '1') {
            this.pages.push({icon: 'md-bookmarks', ios: 'ios-bookmarks', title: x.modulos_show.nombre, apodo: x.apodo, component: 'TipsBibliaUserPage'})
          } else if(x.modulos_show.id == '27' && x.mostrar == '1') {
            this.pages.push({icon: 'md-albums', ios: 'ios-albums', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CategorysDocumentsUserPage'})
          } else if(x.modulos_show.id == '28' && x.mostrar == '1') {
            this.pages.push({icon: 'md-document', ios: 'ios-document', title: x.modulos_show.nombre, apodo: x.apodo, component: 'DocumentsUserPage'})
          } else if(x.modulos_show.id == '30' && x.mostrar == '1') {
            this.pages.push({icon: 'md-cube', ios: 'ios-cube', title: x.modulos_show.nombre, apodo: x.apodo, component: 'AnuncioUserPage'})
          } else if(x.modulos_show.id == '31' && x.mostrar == '1') {
            this.pages.push({icon: 'md-stopwatch', ios: 'ios-stopwatch', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CronometroUserPage'})
          }
        }
      }
      console.clear
    }).catch(error => {
      console.log(error);
    })
  }

  public getPages() {
    if(this.opcion2 == '1') {
      if(this.designCategory!='0') {
        this.rootPage = 'CategorysCardClientPage'
      } else {
        this.rootPage = 'CategorysClientPage';
      }
    } else if(this.opcion2 == '2') {
      this.rootPage = 'ProductsClientPage';
    } else if(this.opcion2 == '3') {
      this.rootPage = 'MyOrdersClientPage';      
    } else if(this.opcion2 == '4') {
      this.rootPage = 'EventsClientPage';      
    } else if(this.opcion2 == '5') {
      this.rootPage = 'YoutubeClientPage';      
    } else if(this.opcion2 == '6') {
      this.rootPage = 'NewsClientPage';      
    } else if(this.opcion2 == '7') {
      this.rootPage = 'TipsClientPage';
    } else if(this.opcion2 == '8') {
      this.rootPage = 'MessagesClientPage';
    } else if(this.opcion2 == '9') {
      this.rootPage = 'ProfileClientPage';
    } else if(this.opcion2 == '10') {
      this.rootPage = 'WorkoutsClientPage';
    } else if(this.opcion2 == '11') {
      this.rootPage = 'LeadearboardClientPage';
    } else if(this.opcion2 == '12') {
      this.rootPage = 'BlogClientPage';
    } else if(this.opcion2 == '13') {
      this.rootPage = 'SocialClientPage';
    } else if(this.opcion2 == '14') {
      this.rootPage = 'ActivePauseClientPage';
    } else if(this.opcion2 == '15') {
      this.rootPage = 'PromotionClientPage';
    } else if(this.opcion2 == '16') {
      this.rootPage = 'MyMembershipClientPage';
    } else if(this.opcion2 == '17') {
      this.rootPage = 'BenefitsClientPage';
    } else if(this.opcion2 == '18') {
      this.rootPage = 'MyProgressClientPage';
    } else if(this.opcion2 == '19') {
      this.rootPage = 'RunningClubClientPage';
    } else if(this.opcion2 == '20') {
      this.rootPage = 'ZFitClubClientPage';
    } else if(this.opcion2 == '21') {
      this.rootPage = 'StaffClientPage';
    } else if(this.opcion2 == '22') {
      this.rootPage = 'MotivationClientPage';
    } else if(this.opcion2 == '23') {
      this.rootPage = 'CitesClientPage';
    } else if(this.opcion2 == '24') {
      this.rootPage = 'InformationClientPage';
    } else if(this.opcion2 == '25') {
      this.rootPage = 'TipsBibliaClientPage';
    } else if(this.opcion2 == '26') {
      this.rootPage = 'MyBooksClientPage';
    } else if(this.opcion2 == '27') {
      this.rootPage = 'CategorysDocumentsClientPage';
    } else if(this.opcion2 == '28') {
      this.rootPage = 'DocumentsClientPage';
    } else if(this.opcion2 == '29') {
      this.rootPage = 'InformationTwoClientPage';
    } else {
      this.rootPage = 'LoginPage';
    }
  }

  //GET DATOS CLIENTE
  public getData() {
    this.opcion2 = localStorage.getItem("currentHome");
    this.baseIdClient = localStorage.getItem("currentId");
    if(localStorage.getItem('currentpictureCover') != 'null'){
      this.pictureCover = localStorage.getItem('currentpictureCover');
    }
    this.designCategory = localStorage.getItem('currentDesignCategory');
    if(localStorage.getItem('currentAuthentication')) {
      this.authentication = localStorage.getItem('currentAuthentication');
      if(localStorage.getItem('currentRolId') == '1') {
        this.getSingleUser(this.baseIdClient);
        this.getModulesApp();
        if(this.baseIdApp == 82) {
          this.rootPage = 'CategorysDocumentsUserPage';
        } else {
          if(this.designCategory!='0') {
            this.rootPage = 'CategorysUserPage'
          } else {
            this.rootPage = 'CategorysCardUserPage';
          }
        }
      } else if(localStorage.getItem('currentRolId') == '2') {
        this.getModulesClient();
        this.getSingleUser(this.baseIdClient);
        if(this.baseIdApp == 49) {
          this.getSingleStaff(this.baseIdApp, this.baseIdClient);
        }
        this.getPages();
      } else {
        if(this.baseIdApp == 49) {
          this.rootPage = 'LoginPage';          
        } else {
          this.getModulesClient();
          this.getPages();
        }
      }
    } else {
      localStorage.setItem('currentAuthentication', 'NoAuthentication')
      this.authentication = 'NoAuthentication'
      this.getModulesClient();
      this.getPages();
    }
    this.rol = localStorage.getItem('currentRolId');
    this.appMembresia = localStorage.getItem('currentAppMembresia');
    this.getSingleUserApp(this.baseIdApp);
  }

  openCategory() {
    this.nav.setRoot('CategoryClientPage');
  }

  openCoach() {
    this.nav.setRoot('CoachUserPage');
  }
  
  openWorkouts() {
    console.log("COMPROBAR", localStorage.getItem('currentType') == 'client')
    if(localStorage.getItem('currentType') == 'client') {
      this.nav.setRoot('WorkoutsMonthClientPage');
    } else {
      this.nav.setRoot('WorkoutsMonthUserPage');
    }
  }

}
