import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, AlertController, IonicPage } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';
import { OrdersService } from '../../../app/service/orders.service';
import { path } from '../../../app/config.module';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

@IonicPage()
@Component({
  selector: 'orders-user',
  templateUrl: 'orders-user.html'
})
export class OrdersUserPage implements OnInit {
  //Propiedades 
  public orders:any[] = [];
  public idProduct:any;
  public idUser:any = path.id;
  public selectItem:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  moneda = localStorage.getItem('currentCurrency');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public modal: ModalController,
    public productsService: ProductsService,
    public alertCtrl: AlertController,
    public ordersService: OrdersService
  ) {
    this.idProduct = this.navParams.get('parameter');
    this.selectItem = 'pagados';
    if(this.idProduct) {
      this.getAllByProducts(this.idProduct);
    } else {
      this.getAll(this.idUser, 3);
    }
  }

  ngOnInit() {
  }

  //VER DETALLES DE LA ORDEN
  openForm(parameter:any) {
    let chooseModal = this.modal.create('DetailOrdersUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      if(data != 'Close') {
        if(data == 'Aceptado') {
          this.selectItem = 'enviados';
          this.getAll(this.idUser, 2);
        }
      }      
    });
    chooseModal.present();
  }

  public getAllByProducts(id:any) {
    this.ordersService.getOrdersProducts(id)
    .then(response => {
        this.orders = response;
    }).catch(error => {
        console.clear
    })
  }

  public getAll(id:any, state:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.ordersService.getOrdersByUserState(id, state)
    .then(response => {
      this.orders = response;
      this.orders.reverse();
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  public cancel(id:string){
    let order = {
      state: '0',
      id: id
    }
    let confirm = this.alertCtrl.create({
      title: '¿Deseas cancelar el pedido?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.selectItem = 'cancelados';
            this.ordersService.update(order)
            .then(response => {
              this.getAll(this.idUser, 0);
              console.clear
            }).catch(error => {
              console.clear
            });
          }
        }
      ]
    });
    confirm.present();
  }

  //ACEPTAR ORDEN
  public accept(id:string){
    let order = {
      state: '2',
      id: id
    }
    let confirm = this.alertCtrl.create({
      title: 'Desea envíar el pedido?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.selectItem = 'enviados';
            this.ordersService.update(order)
            .then(response => {
              this.getAll(this.idUser, 2);
              console.clear
            }).catch(error => {
              console.clear
            });
          }
        }
      ]
    });
    confirm.present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.idProduct) {
        this.getAllByProducts(this.idProduct);
      } else {
        if(this.selectItem == 'pagados') {
          this.getAll(this.idUser, 3);
        } else if(this.selectItem == 'pendientes') {
          this.getAll(this.idUser, 4);
        } else if(this.selectItem == 'enviados') {
          this.getAll(this.idUser, 2);
        } else if(this.selectItem == 'entregados') {
          this.getAll(this.idUser, 1);
        } else if(this.selectItem == 'cancelados') {
          this.getAll(this.idUser, 0);
        }
      }
      refresher.complete();
    }, 2000);
  }

}