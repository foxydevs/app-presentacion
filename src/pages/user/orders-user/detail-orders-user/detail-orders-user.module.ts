import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailOrdersUserPage } from './detail-orders-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailOrdersUserPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailOrdersUserPage),
    PipeModule
  ],
  exports: [
    DetailOrdersUserPage
  ]
})
export class DetailOrdersUserPageModule {}