import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { UsersService } from '../../../app/service/users.service';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { EncriptationService } from '../../../app/service/encriptation.service';
import { AlertController } from '../../../../node_modules/ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'profile-user',
  templateUrl: 'profile-user.html'
})
export class ProfileUserPage {
  //PROPIEDADES
  public profilePicture:any = localStorage.getItem('currentPicture');
  public user:any;
  public email:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public userService: UsersService,
    public modalController: ModalController,
    public barcodeScanner: BarcodeScanner,
    public secondService: EncriptationService,
    public alertCtrl: AlertController
  ) {
  }

  ionViewWillEnter() {
    this.getSingle();
  }

  //GET SINGLE
  getSingle() {
    this.profilePicture = localStorage.getItem("currentPicture");
    this.user = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.email = localStorage.getItem('currentEmail');
    this.navColor = localStorage.getItem('currentColor');
    this.btnColor = localStorage.getItem('currentColorButton');
  }

  //ABRIR MODAL
  public openModal(state:any, id?:any) {
    let parameter = {
      state: state,
      id: id
    }
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      this.getSingle();
    });
    chooseModal.present();
  }

  //MI PERFIL
  public updateProfile() {
    let parameter = '1'
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      this.getSingle();
    });
    chooseModal.present();
  }

  //CAMBIAR CONTRASEÑA
  public changePassword() {
    let parameter = '2'
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.present();
  }

  //UBICACION
  public location() {
    let parameter = '3'
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.present();
  }

  //ACCESOS
  public access() {
    let parameter = '4'
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.present();
  }

  //VER + CONFIGURACIONES
  public moreConfigurations() {
    let parameter = '6'
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.present();
  }

  //PERSONALIZAR
  public personalization() {
    let parameter = '7'
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      this.getSingle();
    });
    chooseModal.present();
  }

  //LECTOR CODEBAR
  public readCodebar() {
    this.barcodeScanner.scan().then(barcodeData => {
      var aMyUTF8Output = this.secondService.base64DecToArr(barcodeData.text);
      var sMyOutput = this.secondService.UTF8ArrToStr(aMyUTF8Output);
      this.openModal(5, +sMyOutput)
      /*this.userService.getSingle(+sMyOutput)
      .then(response => {
        alert('El usuario es: ' + response.firstname + ' '+ response.lastname)
      }).catch(error => {
        alert(error)
      });*/
    }).catch(err => {
      console.log('Error', err)
      //this.confirmation('Información!', 'El formato no esta soportado.');
    });
  }

  //CONFIRMACIÓN
  confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

}