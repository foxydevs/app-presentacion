import { Component } from '@angular/core';
import { NavController, ViewController, LoadingController, IonicPage } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AccessService } from '../../../../../app/service/access.service';
import { Events } from '../../../../../../node_modules/ionic-angular/util/events';

@IonicPage()
@Component({
  selector: 'modal-modules-user',
  templateUrl: 'modal-modules-user.html'
})
export class ModalModulesUserPage {
  public parameter:any;
  btnDisabled:boolean = false;
  public data = {
    mostrar: 0,
    orden: 0,
    apodo: '',
    membresia: 0,
    id: 0
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(public navCtrl: NavController,
  public mainService: AccessService,
  public loading: LoadingController,
  public navParams: NavParams,
  public toast: ToastController,
  public alertCtrl: AlertController,
  public events: Events,
  public view: ViewController) {
    this.parameter = this.navParams.get('parameter');
    this.getSingle(this.parameter);
  }

  //CARGAR CLIENTE
  public getSingle(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(id)
    .then(res => {
      console.log(res)
      this.data.id = res.id;
      this.data.mostrar = res.mostrar;
      this.data.membresia = res.membresia;
      this.data.orden = res.orden;
      this.data.apodo = res.apodo;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    })
  }

  //ACTUALIZAR
  saveChanges() {
    this.btnDisabled = true;
    this.mainService.update(this.data)
    .then(response => {
      this.confirmation('Acceso Actualizado', 'El acceso fue actualizado exitosamente.');
      this.view.dismiss(response);
      this.events.publish('user:updates');
      this.btnDisabled = false;
    }).catch(error => {
      this.btnDisabled = false;
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJE DE ERROR
  public message(message:string) {
    this.toast.create({
      message: message,
      duration: 1000
    }).present();
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

}
