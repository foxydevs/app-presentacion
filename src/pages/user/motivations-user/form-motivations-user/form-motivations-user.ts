import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { MotivationService } from '../../../../app/service/motivation.service';
import { path } from '../../../../app/config.module';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { UsersService } from '../../../../app/service/users.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'form-motivations-user',
  templateUrl: 'form-motivations-user.html',
})
export class FormMotivationsUserPage {
  //PROPIEDADES
  public parameter:any;
  public title:any;
  public disabledBtn:any;
  public basePath:string = path.path;
  selectItem:any = 'actualizar';
  public users:any[] = [];
  public comments:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data = {
    title: '',
    description: '',
    state: 2,
    user: path.id,
    picture: localStorage.getItem('currentPicture'),
    id: ''
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public loading: LoadingController,
    public mainService: MotivationService,
    public secondService: UsersService,
  ) {
    this.parameter = this.navParams.get('parameter');
    console.log(this.parameter)
    this.getAllSecond();
    if(this.parameter) {
      this.title = "Edición Motivación";
      this.getSingle(this.parameter);   
    } else {
      this.title = "Agregar Motivación";
    }
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    this.data.picture = $('img[alt="Avatar"]').attr('src');
    if(this.data.title) {
      if(this.data.description) {
        this.disabledBtn = true;
        if(this.parameter) {
          console.log(this.data);
          this.update(this.data);
        } else {
          console.log(this.data);
          this.create(this.data);
        }
      } else {
        this.message('La descripción es requerida.');
      }
    } else {
      this.message('El título es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Motivación Agregada', 'La motivación fue agregada exitosamente.');
      this.parameter = response.id;
      this.data.id = response.id;
      this.disabledBtn = false;
      console.log(response);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Motivación Actualizada', 'La motivación fue actualizada exitosamente.');
      this.navCtrl.pop();
      this.disabledBtn = false;
      console.log(response);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar la motivación?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      this.getAllComments(response.id);
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    this.secondService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //IMAGEN DE CATEGORIA
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'motivations'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //CARGAR COMENTARIOS POR PRODUCTOS
  public getAllComments(id:any) {
    this.comments = [];
    this.mainService.getComments(id)
    .then(res => {
      for(let x of res) {
        let comment = {
          comment: x.comment,
          fecha: x.created_at,
          user: this.returnNameUser(x.user),
          picture: this.returnPicture(x.user)
        }
        this.comments.push(comment);
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear();
    });
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //Devolver el Nombre del Usuario
   public returnPicture(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].picture;
      }
    }
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }
}
