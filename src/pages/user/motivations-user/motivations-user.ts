import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { path } from '../../../app/config.module';
import { MotivationService } from '../../../app/service/motivation.service';

@IonicPage()
@Component({
  selector: 'motivations-user',
  templateUrl: 'motivations-user.html',
})
export class MotivationsUserPage {
  //PROPIEDADES
  public table:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: MotivationService
  ) {
  }

  openForm(parameter?:any) {
    this.navCtrl.push('FormMotivationsUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(path.id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll();
  }

}
