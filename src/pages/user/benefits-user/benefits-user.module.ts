import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BenefitsUserPage } from './benefits-user';
 
@NgModule({
  declarations: [
    BenefitsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(BenefitsUserPage),
  ],
  exports: [
    BenefitsUserPage
  ]
})
export class BenefitsUserPageModule {}