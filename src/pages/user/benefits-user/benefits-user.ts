import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { BenefitsService } from '../../../app/service/benefits.service';
import { CommerceService } from '../../../app/service/commerce.service';

@IonicPage()
@Component({
  selector: 'benefits-user',
  templateUrl: 'benefits-user.html',
})
export class BenefitsUserPage {
  //PROPIEDADES
  public table:any[] = [];
  public table2:any[] = [];
  public idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  selectItem:any = 'beneficios';

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: BenefitsService,
    public secondService: CommerceService,
  ) {
    this.idUser = localStorage.getItem('currentId');
  }

  openForm(parameter?:any) {
    this.navCtrl.push('FormBenefitsUserPage', { parameter });
  }

  openForm2(parameter?:any) {
    this.navCtrl.push('FormCommerceUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR LOS RETOS
  public getAllSecond(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAllUser(id)
    .then(response => {
      this.table2 = []
      this.table2 = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    if(this.selectItem == 'beneficios') {
      this.getAll(this.idUser);        
    } else {
      this.getAllSecond(this.idUser)
    }
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.selectItem == 'beneficios') {
        this.getAll(this.idUser);        
      } else {
        this.getAllSecond(this.idUser)
      }
      refresher.complete();
    }, 2000);
  }

}
