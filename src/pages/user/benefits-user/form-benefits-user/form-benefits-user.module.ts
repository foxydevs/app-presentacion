import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormBenefitsUserPage } from './form-benefits-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormBenefitsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormBenefitsUserPage),
    PipeModule
  ],
  exports: [
    FormBenefitsUserPage
  ]
})
export class FormBenefitsUserPageModule {}