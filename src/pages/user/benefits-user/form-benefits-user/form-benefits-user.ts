import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { BenefitsService } from '../../../../app/service/benefits.service';
import { CommerceService } from '../../../../app/service/commerce.service';
import { InAppBrowser } from '@ionic-native/in-app-browser';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'form-benefits-user',
  templateUrl: 'form-benefits-user.html',
})
export class FormBenefitsUserPage {
  //PROPIEDADES
  public parameter:any;
  public table:any[] = [];
  public title:any;
  public disabledBtn:boolean;
  public basePath:string = path.path;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data = {
    title: '',
    description: '',
    comercio: 1,
    user: path.id,
    picture: localStorage.getItem('currentPicture'),
    id: '',
    link1: '',
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public mainService: BenefitsService,
    public secondService: CommerceService,
    public iab: InAppBrowser
  ) {
    this.parameter = this.navParams.get('parameter')
    this.getAll(path.id);
    if(this.parameter) {
      this.title = "Edición Beneficio";
      this.getSingle(this.parameter);  
    } else {
      this.title = "Agregar Beneficio";
    }
  }

  //CARGAR BENEFICIOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    this.data.picture = $('img[alt="Avatar"]').attr('src');
    if(this.data.title) {
      if(this.data.description) {
        this.disabledBtn = true;
        if(this.parameter) {
          console.log(this.data)
          this.update(this.data)
        } else {
          console.log(this.data)
          this.create(this.data)
        }
      } else {
        this.message('La descripción es requerida.');
      }
    } else {
      this.message('El título es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    let load = this.loading.create({
      content: 'Agregando...'
    });
    load.present();
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Beneficio Agregado', 'El beneficio fue agregado exitosamente.');
      this.data.id = response.id;
      this.parameter = response.id;
      load.dismiss();
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    let load = this.loading.create({
      content: 'Actualizando...'
    });
    load.present();
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Beneficio Actualizado', 'El beneficio fue actualizado exitosamente.');
      this.navCtrl.pop();
      console.log(response);
      load.dismiss();
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el beneficio?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //IMAGEN DE CATEGORIA
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'benefits'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#E67E22');
  }
}
