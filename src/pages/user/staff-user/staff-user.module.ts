import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StaffUserPage } from './staff-user';
 
@NgModule({
  declarations: [
    StaffUserPage,
  ],
  imports: [
    IonicPageModule.forChild(StaffUserPage),
  ],
  exports: [
    StaffUserPage
  ]
})
export class StaffUserPageModule {}