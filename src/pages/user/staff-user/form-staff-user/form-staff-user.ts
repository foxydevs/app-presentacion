import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, AlertController, ViewController, IonicPage } from 'ionic-angular';
import { NavParams } from '../../../../../node_modules/ionic-angular/navigation/nav-params';
import { StaffService } from '../../../../app/service/staff.service';

@IonicPage()
@Component({
  selector: 'form-staff-user',
  templateUrl: 'form-staff-user.html'
})
export class FormStaffUserPage {
  //PROPIEDADES
  public parameter:any;
  public disabledBtn:boolean = false;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data:any = {
    puesto: '',
    description: '',
    id: '',
    nombre: '',
    picture: '',
  }

  //CONSTRUCTOR
  constructor(public navCtrl: NavController,
  public loading: LoadingController,
  public alertCtrl: AlertController,
  public navsParams: NavParams,
  public modal: ModalController,
  public view: ViewController,
  public mainService: StaffService) {
    this.parameter = this.navsParams.get('parameter');
    this.data.id = this.parameter;
    this.getSingle(this.parameter)
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data.puesto = response.puesto;
      this.data.description = response.description;
      this.data.picture = response.picture;
      this.data.nombre = response.users.firstname + ' ' + response.users.lastname;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }
  
  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  //GUARDAR CAMBIOS
  public saveChanges() {
    this.disabledBtn = true;
    this.update(this.data)
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.view.dismiss();
      console.log(response);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

}