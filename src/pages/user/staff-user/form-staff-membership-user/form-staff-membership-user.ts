import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, AlertController, ViewController, IonicPage } from 'ionic-angular';
import { NavParams } from '../../../../../node_modules/ionic-angular/navigation/nav-params';
import { UsersService } from '../../../../app/service/users.service';

@IonicPage()
@Component({
  selector: 'form-staff-membership-user',
  templateUrl: 'form-staff-membership-user.html'
})
export class FormStaffMembershipUserPage {
  //PROPIEDADES
  parameter:any;
  quantities:any = [];
  disabledBtn:boolean = false;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  data:any = {
    id: '',
    nombre: '',
    picture: '',
    opcion15: '',
    opcionRespaldo15: '',
    opcion14: 1,
    inicioMembresia: '',
    finMembresia: '',
    tipoNivel: '',
  }

  //CONSTRUCTOR
  constructor(public navCtrl: NavController,
  public loading: LoadingController,
  public alertCtrl: AlertController,
  public navsParams: NavParams,
  public modal: ModalController,
  public view: ViewController,
  public mainService: UsersService) {
    this.parameter = this.navsParams.get('parameter');
    this.data.id = this.parameter;
    this.getQuantity();
    setTimeout(() => {
      this.getSingle(this.parameter)      
    }, 500);
    this.getDate();
  }

  //CARGAR NUMEROS
  public getQuantity() {
    for(let x = 1; x <= 100; x++) {
      this.quantities.push(x);
    } 
  }

  getDate() {
    let fechahoy = new Date();
    this.data.inicioMembresia = fechahoy.getFullYear() + '-' + (fechahoy.getMonth() + 1) + '-' + fechahoy.getDate();
    //this.data.hora = fechahoy.getHours() + ':' + fechahoy.getMinutes() + ':' + fechahoy.getSeconds();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data.opcion14 = response.opcion14;
      this.data.picture = response.picture;
      this.data.finMembresia = response.finMembresia;
      this.data.tipoNivel = response.tipoNivel;
      this.data.nombre = response.firstname + ' ' + response.lastname;
      if(response.opcion15) {
        this.data.opcion15 = response.opcion15;      
      } else {
        this.data.opcion15 = this.createUUID(response.id);
      }
      /*if(id.length == 1) {
        this.data.opcionRespaldo15 = this.addZero(response.id, 10)
      } else if(id.length == 2) {
        this.data.opcionRespaldo15 = this.addZero(response.id, 9)
      } else if(id.length == 3) {
        this.data.opcionRespaldo15 = this.addZero(response.id, 8)
      } else if(id.length == 4) {
        this.data.opcionRespaldo15 = this.addZero(response.id, 7)
      } else if(id.length == 5) {
        this.data.opcionRespaldo15 = this.addZero(response.id, 6)
      }*/
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CREAR CÓDIGO
  createCodeUser() {
    this.data.opcion15 = this.createUUID(this.data.id);
  }

  //ENVIAR UUID
  public createUUID(id:any){
    var dt = id;
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
  }
  
  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  //GUARDAR CAMBIOS
  public saveChanges() {
    this.disabledBtn = true;
    this.update(this.data)
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.view.dismiss();
      console.log(response);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  addZero(string: any,largo:number) {
    console.log(string);
    console.log(largo);
    let ceros = '';
    let cantidad = largo - string.toString().split('').length;
    console.log(ceros);
    console.log(cantidad);
 
    if (cantidad >= 1)
    	{
    		for(let i=0;i<cantidad;i++)
    		{
    		ceros += '0';
    	}
    	return (ceros + string);
    }
    else
    return string;
  }

}