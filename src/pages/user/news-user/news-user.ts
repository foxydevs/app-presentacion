import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, IonicPage } from 'ionic-angular';
import { NewsService } from '../../../app/service/news.service';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { path } from '../../../app/config.module';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'news-user',
  templateUrl: 'news-user.html'
})
export class NewsUserPage {
  //PROPIEDADES
  public news:any[] = [];
  public search:any;
  public idUser:any;
  idApp:any = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public newsService: NewsService,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public modalController: ModalController
  ) {
    this.idUser = localStorage.getItem('currentId');
  }

  //CARGAR PRODUCTOS
  public getAll() {
    this.newsService.getAll()
    .then(response => {
      this.news = [];
      this.news = response;
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll();
  }

  openForm(parameter:any) {
    let chooseModal = this.modalController.create('FormNewsUserPage', { parameter });
    chooseModal.onDidDismiss((data)=> {
      this.getAll();
      console.log(data)
    })
    chooseModal.present();
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

}
