import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsUserPage } from './news-user';
 
@NgModule({
  declarations: [
    NewsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsUserPage),
  ],
  exports: [
    NewsUserPage
  ]
})
export class NewsUserPageModule {}