import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { SocialService } from '../../../app/service/social.service';

@IonicPage()
@Component({
  selector: 'social-user',
  templateUrl: 'social-user.html',
})
export class SocialUserPage {
  //PROPIEDADES
  public table:any[] = [];
  public idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: SocialService,
  ) {
    this.idUser = localStorage.getItem('currentId');
  }

  openForm(parameter?:any) {
    this.navCtrl.push('FormSocialUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idUser);
      refresher.complete();
    }, 2000);
  }

}
