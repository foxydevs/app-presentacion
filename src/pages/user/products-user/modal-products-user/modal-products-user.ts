import { Component } from '@angular/core';
import { NavController, IonicPage, ViewController} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ProductsService } from '../../../../app/service/products.service';
import { CategorysService } from '../../../../app/service/categorys.service';
import { PresentationService } from '../../../../app/service/presentation.service';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'modal-products-user',
  templateUrl: 'modal-products-user.html',
})
export class ModalProductsUserPage {
  //PROPIEDADES
  categories:any = [];
  Table:any = [];
  comments:any[] = [];
  parameter:any;
  title:any;
  disabledBtn:any;
  baseUserId = path.id;
  basePath:string = path.path;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  appMembresia = localStorage.getItem('currentAppMembresia');
  moneda = localStorage.getItem('currentCurrency');
  data = {
    name: '',
    id: 0,
    description : '',
    price: 0,
    quantity: 0,
    tiempo: 0,
    periodo: 0,
    membresia: 0,
    nivel: 1,
    cost : 0,
    user_created: this.baseUserId,
    presentacion: '',
    presentaciones_varias: [],
    presentacionNombre: '',
    category: '',
    digital: 0,
    ver: 1,
    opcion1: 1,
    libro: '',
    duracion: '0',
    idPic1: '',
    idPic2: '',
    idPic3: '',
    picture1: localStorage.getItem('currentPicture'),
    picture2: localStorage.getItem('currentPicture'),
    picture3: localStorage.getItem('currentPicture'),
    libros: localStorage.getItem('currentPicture'),
    picture: localStorage.getItem('currentPicture')
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public iab: InAppBrowser,
    public loading: LoadingController,
    public mainService: ProductsService,
    public secondService: CategorysService,
    public thirdService: PresentationService,
    public view: ViewController,
    public modalController: ModalController
  ) {
    this.parameter = this.navParams.get('parameter');
    console.log(this.parameter)
    this.getAllSecond();
    if(this.parameter.state == '1') {
      this.title = 'Agregar Producto';
      if(this.parameter.category) {
        this.data.category = this.parameter.category;
      }
    } else if(this.parameter.state == '2') {
      this.title = 'Edición Producto';
      this.getSingle(this.parameter.id);
      setTimeout(() => {
        this.getAllComments(this.parameter.id)        
      }, 100);
    }
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.name) {
      if(this.data.description) {
        if(this.parameter.state == '1') {
          this.create(this.data);
        } else if(this.parameter.state == '2') {
          this.update(this.data);      
        }
      } else {
        this.message('La descripción es requerida.');
      }
    } else {
      this.message('El nombre es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Producto Agregado', 'El producto fue agregado exitosamente.');
      this.data.id = response.id;
      this.parameter.state = '2'
      this.title = "Edición Producto";
      this.getSingle(response.id);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Producto Actualizado', 'El producto fue actualizado exitosamente.');
      this.view.dismiss(response);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el producto?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.mainService.delete(id)
            .then(response => {
              this.view.dismiss(response);
            }).catch(error => {
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data.id = response.id;
      this.data.name = response.name;
      this.data.description = response.description;
      this.data.price = response.price.toFixed(2);
      this.data.quantity = response.quantity;
      this.data.cost = response.cost.toFixed(2);
      this.data.category = response.category;
      this.data.picture = response.picture;
      this.data.tiempo = response.tiempo;
      this.data.periodo = response.periodo;
      this.data.membresia = response.membresia;
      this.data.nivel = response.nivel;
      this.data.digital = response.digital;
      this.data.libro = response.libro;
      this.data.ver = response.ver;
      this.data.presentacion = response.presentacion;
      this.data.presentaciones_varias = response.presentaciones_varias;
      this.data.duracion = response.duracion;
      this.data.opcion1 = response.opcion1;
      if(response.libro) {
        this.data.libros = 'https://developers.zamzar.com/assets/app/img/convert/pdf.png'
      }
      if(response.presentaciones) {
        this.data.presentacionNombre = response.presentaciones.name;
      }
      if(response.pictures) {
        if(response.pictures[0]) {
          this.data.picture1 = response.pictures[0].picture;
          this.data.idPic1 = response.pictures[0].id;
        } 
        if(response.pictures[1]) {
          this.data.picture2 = response.pictures[1].picture;
          this.data.idPic2 = response.pictures[1].id;
        }
        if(response.pictures[2]) {
          this.data.picture3 = response.pictures[2].picture;
          this.data.idPic3 = response.pictures[2].id;
        }
      }
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //GET ALL CATEGORIAS
  public getAllSecond(){
    this.secondService.getAllUser(this.baseUserId)
    .then(response => {
      this.categories = [];
      this.categories = response;
    }).catch(error => {
      console.clear
    });
  }

  //SUBIR IMAGENES
  uploadImage(archivo:any, id:any) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}product/upload/${this.data.id}`;

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.picture)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //SUBIR DOCUMENTO
  uploadDocument(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == 'application/pdf') {
      if(size<(10*(1024*1024))) {
        $('#imgAvatar4').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'documents'
          },
          function(respuesta) {
            $('#imgAvatar5').attr("src", respuesta.url)
            $('#imgAvatar4').attr("src", 'https://developers.zamzar.com/assets/app/img/convert/pdf.png')
            $("#"+id).val('')
          }
        );
      } else {
        this.message('El documento es demasiado grande.')
      }
    } else {
      this.message('El tipo de documento no es válido.')
    }
  }

  //SUBIR IMAGEN PRINCIPAL
  uploadImages(archivo, id, img, idPicture) {
    this.getInformationImage();
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}product/upload/multiple/${this.data.id}`;
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {        
      if(size<(2*(1024*1024))) {
        $('#'+img).attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif');          
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            user: this.baseUserId,
            idPic: idPicture
          },
          function(respuesta) {
            console.log(respuesta)
            $('#'+img).attr("src", respuesta.picture);
            $("#"+id).val('');  
          }
        );
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.')
    }
  }

  //CARGAR PRODUCTO
  public getInformationImage() {
    this.mainService.getSingle(this.data.id)
    .then(response => {
      if(response.pictures) {
        if(response.pictures[0]) {
          this.data.picture1 = response.pictures[0].picture;
          this.data.idPic1 = response.pictures[0].id;
        } 
        if(response.pictures[1]) {
          this.data.picture2 = response.pictures[1].picture;
          this.data.idPic2 = response.pictures[1].id;
        }
        if(response.pictures[2]) {
          this.data.picture3 = response.pictures[2].picture;
          this.data.idPic3 = response.pictures[2].id;
        }
      }      
    }).catch(error => {
      console.log(error)
    })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  //CARGAR COMENTARIOS
  public getAllComments(id:any) {
    this.comments = [];
    this.mainService.getAllCommentsByProduct(id)    
    .then(response => {
      for(let x of response) {
        let comment = {
          comment: x.comment,
          fecha: moment(x.created_at).format('LL'),
          user: x.users.firstname + ' ' + x.users.lastname,
          picture: x.users.picture
        }
        this.comments.push(comment);
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear;
    })
  }

  //OPEN MODAL PRODUCTS
  public openModal(product:any, presentacion?:any) {
    let parameter = {
      product: product,
      presentacion: presentacion,
    }
    let chooseModal = this.modalController.create('ModalPresentacionUserPage', { parameter });
    chooseModal.onDidDismiss((data) => {
      this.getSingle(this.parameter.id);      
    });
    chooseModal.present();
  }

  //ELIMINAR
  public delete2(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar la presentación?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.thirdService.deleteType(id)
            .then(response => {
              this.getSingle(this.parameter.id);
            }).catch(error => {
            })
          }
        }
      ]
    });
    confirm.present();
  }

}
