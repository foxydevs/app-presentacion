import { Component } from '@angular/core';
import { NavController, IonicPage, NavParams} from 'ionic-angular';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { PresentationService } from '../../../../app/service/presentation.service';
import { path } from '../../../../app/config.module';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { ProductsService } from '../../../../app/service/products.service';

@IonicPage()
@Component({
  selector: 'modal-presentacion-user',
  templateUrl: 'modal-presentacion-user.html',
})
export class ModalPresentacionUserPage {
  //PROPIEDADES
  public Table:any[] = [];
  public idUser:any = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  selectItem:any = 'beneficios';
  title:any;
  presentaciones:boolean = true;
  formulario:boolean = false;
  disabledBtn:boolean;
  parameter:any;
  data = {
    presentacion: '',
    product: '',
    cost: '',
    quantity: '',
    description: '',
    calificacion: 0,
    price: '',
    app: path.id,
    tipo: 0,
    state: 0,
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: PresentationService,
    public secondService: ProductsService,
    public view: ViewController,
    public navParams: NavParams
  ) {
    this.title = 'Presentaciones'
    this.parameter = this.navParams.get('parameter');
    if(this.parameter) {
      if(this.parameter.presentacion) {
        this.getSingle(this.parameter.presentacion);
      } else if(this.parameter.product) {
        this.data.product = this.parameter.product;
      }
    }
  }

  select(id:any, nombre:any) {
    this.presentaciones = false;
    this.formulario = true;
    this.data.presentacion = id;
    this.data.description = nombre;
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    this.presentaciones = false;
    this.formulario = true;
    this.mainService.getSingleType(parameter)
    .then(response => {
      this.data = response;
    }).catch(error => {
      console.log(error)
    });
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAll()
    .then(response => {
      this.Table = []
      this.Table = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll();
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //SAVE CHANGES
  public saveChanges() {
    if(this.parameter.presentacion) {
      this.update(this.data);
    } else if(this.parameter.product) {
      this.create(this.data);
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.disabledBtn = true;
    this.mainService.createType(formValue)
    .then(response => {
      this.disabledBtn = false;
      this.view.dismiss('Close');
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //AGREGAR
  update(formValue:any) {
    this.disabledBtn = true;
    this.mainService.updateType(formValue)
    .then(response => {
      this.disabledBtn = false;
      this.view.dismiss('Close');
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

}
