import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogUserPage } from './blog-user';
 
@NgModule({
  declarations: [
    BlogUserPage,
  ],
  imports: [
    IonicPageModule.forChild(BlogUserPage),
  ],
  exports: [
    BlogUserPage
  ]
})
export class BlogUserPageModule {}