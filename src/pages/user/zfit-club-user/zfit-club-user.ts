import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, IonicPage } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';
import { EventsTypeService } from '../../../app/service/events-type.service';

@IonicPage()
@Component({
  selector: 'zfit-club-user',
  templateUrl: 'zfit-club-user.html'
})
export class ZFitClubUserPage {
  //PROPIEDADES
  public table:any[] = [];
  public table2:any[] = [];
  public idUser:any;
  selectItem:any = 'principal';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
     public loading: LoadingController,
     public eventsService: EventsService,
     public secondService: EventsTypeService,
     public alertCtrl :AlertController
  ) {
    this.idUser = localStorage.getItem("currentId");
  }

  //GET ALL
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.eventsService.getAllType(this.idUser, 3).then(response => {
      this.table = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear();
    })
  }

  //GET ALL
  public getAllSecond() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAll()
    .then(response => {
      this.table2 = response;
      load.dismiss();
    }).catch(error => {
      console.clear();
    })
  }

  //OPEN PAGE
  public openPage(parameter:any) {
    this.navCtrl.push('DetailZFitClubUserPage', { parameter });
  }

  //OPEN FORM
  public openForm(parameter?:any) {
    this.navCtrl.push('FormEventTypeUserPage', { parameter });
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.selectItem == 'principal') {
        this.getAll();
      } else {
        this.getAllSecond();
      }
      refresher.complete();
    }, 2000);
  }

  //ENTER
  ionViewWillEnter(){
    if(this.selectItem == 'principal') {
      this.getAll();
    } else {
      this.getAllSecond();
    }
  }

}
