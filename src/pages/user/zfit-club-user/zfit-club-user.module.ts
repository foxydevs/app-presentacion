import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ZFitClubUserPage } from './zfit-club-user';
 
@NgModule({
  declarations: [
    ZFitClubUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ZFitClubUserPage),
  ],
  exports: [
    ZFitClubUserPage
  ]
})
export class ZFitClubUserPageModule {}