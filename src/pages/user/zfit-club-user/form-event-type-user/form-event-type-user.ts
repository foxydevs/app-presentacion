import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { EventsTypeService } from '../../../../app/service/events-type.service';

@IonicPage()
@Component({
  selector: 'form-event-type-user',
  templateUrl: 'form-event-type-user.html',
})
export class FormEventTypeUserPage {
  //PROPIEDADES
  public parameter:any;
  public title:any;
  public disabledBtn:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data = {
    tipo: 0,
    description: '',
    state: 1,
    //user: path.id,
    id: ''
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public mainService: EventsTypeService
  ) {
    this.parameter = this.navParams.get('parameter')
    if(this.parameter) {
      this.title = "Edición Tipo de Reto";
      this.getSingle(this.parameter);  
    } else {
      this.title = "Agregar Tipo de Reto";
    }
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.description) {
      this.disabledBtn = true;
      if(this.parameter) {
        console.log(this.data)
        this.update(this.data)
      } else {
        console.log(this.data)
        this.create(this.data)
      }
    } else {
      this.message('La descripción es requerida.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Tipo de Reto Agregado', 'El tipo de reto fue agregado exitosamente.');
      this.data.id = response.id;
      this.parameter = response.id;
      this.navCtrl.pop();
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Tipo de Reto Actualizado', 'El tipo de reto fue actualizado exitosamente.');
      this.navCtrl.pop();
      console.log(response);
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el tipo de reto?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }
}
