import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, AlertController, IonicPage } from 'ionic-angular';
import { EventsService } from '../../../../app/service/events.service';
import { UsersService } from '../../../../app/service/users.service';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { path } from '../../../../app/config.module';

//JQUERY
declare var $:any;
declare var google;

@IonicPage()
@Component({
  selector: 'form-running-club-user',
  templateUrl: 'form-running-club-user.html'
})
export class FormRunningClubUserPage{
  //PROPIEDADES
  public map: any;
  public users:any[] = [];
  public data = {
    address: '',
    description : '',
    date: '',
    time: '',
    latitude: '',
    longitude: '',
    user_created: +localStorage.getItem('currentId'),
    user_owner: +localStorage.getItem('currentId'),
    place: '',
    tipo: 2,
    place_id:'0',
    id: '',
  }
  public events = {
    picture : localStorage.getItem('currentPicture'),
    assistants: '',
    interested: '',
  }
  public title:any;
  public marker:any;
  public parameter:any;
  public btnDisabled:boolean = false;
  public basePath:string = path.path
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public mainService: EventsService,
    public secondService: UsersService,
    public loading: LoadingController,
    public geolocation: Geolocation,
    public view: ViewController,
    public alertCtrl: AlertController
  ) {
    this.parameter = this.navParams.get('parameter');
    if(this.parameter) {
      this.title = "Edición de Evento";
      this.getSingle(this.parameter);
    } else {
      this.getPosition();
      this.title = "Nuevo Evento";
    }
  }

  //Cargar los productos
  public loadAllUsers(){
    this.secondService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR EVENTO
  public getSingle(parameter:any) {
    this.mainService.getSingle(this.parameter)
    .then(res => {     
      this.data.latitude = res.latitude;
      this.data.longitude = res.longitude;
      this.loadMapUpdate(this.data.latitude, this.data.longitude);
      this.data.place = res.place;
      this.data.address = res.address;
      this.data.description = res.description;
      this.data.date = res.date;
      this.data.time = res.time;
      this.data.place_id = res.place_id;
      this.events.assistants = res.assistants.length;
      this.events.interested = res.interested.length;
      this.data.id = res.id;
      this.events.picture = res.picture;
    }).catch(error => {
      console.log(error)
    });
  }

  getPosition():any{
    this.geolocation.getCurrentPosition().then(resp => {
      this.loadMap(resp)
     }).catch(error => {
      console.log('Error getting location', error);
    });
  }

  //CARGAR MAPA
  public loadMap(position: Geoposition) {
  let latitude = position.coords.latitude;
  let longitude = position.coords.longitude;
  this.data.latitude = latitude.toString();
  this.data.longitude = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.data.latitude = evt.latLng.lat();
      this.data.longitude = evt.latLng.lng();
    });
  }

  //CARGAR MAPA ACTUALIZAR
  public loadMapUpdate(lat:any, lon:any) {
  let latitude = lat;
  let longitude = lon;
  this.data.latitude = latitude.toString();
  this.data.longitude = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.data.latitude = evt.latLng.lat();
      this.data.longitude = evt.latLng.lng();
    });
  }

  toggleBounce() {
    this.marker.getPosition()
    console.log(this.marker.getPosition())
    if (this.marker.getAnimation()) {
      this.marker.setAnimation(null);
      console.log(this.marker)
    } else {
      this.marker.setAnimation(google.maps.Animation.BOUNCE);
      console.log(this.marker)
    }
  }

  //GUARDAR CAMBIOS
  public saveChanges() {
    if(this.data.place) {
      if(this.data.address) {
        if(this.data.description) {
          if(this.data.description.length >= 10) {
            if(this.data.date) {
              if(this.data.time) {
                this.btnDisabled = true;
                if(this.parameter) {
                  this.update(this.data);
                } else {
                  this.create(this.data);
                }
              } else {
                this.message('La hora es requerida.');
              }
            } else {
              this.message('La fecha es requerida.');
            }
          } else {
            this.message('La descripcion debe de llevar al menos 10 caracteres.');
          }
        } else {
          this.message('La descripción es requerida.');
        }
      } else {
        this.message('La dirección es requerida.');
      }
    } else {
      this.message('El lugar es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    let load = this.loading.create({
      content: 'Agregando...'
    });
    load.present();
    this.mainService.create(formValue)
    .then(response => {
      load.dismiss();
      this.confirmation('Evento de Running Club Agregado', 'El evento fue agregado exitosamente.');
      this.data.id = response.id;
      this.parameter = response.id;
      this.btnDisabled = false;
    }).catch(error => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    let load = this.loading.create({
      content: 'Actualizando...'
    });
    load.present();
    this.mainService.update(formValue)
    .then(response => {
      load.dismiss();
      this.confirmation('Evento de Running Club Actualizado', 'El evento fue actualizado exitosamente.');
      this.navCtrl.pop();
      console.log(response);
      this.btnDisabled = false;
    }).catch(error => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //Subir Imagenes de Producto
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}event/upload/${this.data.id}`;
    
    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.picture)
            $("#"+id).val('')
          }
        );
      } else {
        this.message('La imagen es demasiado grande.');
      }
    } else {
      this.message('El tipo de imagen no es válido.');
    }
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el evento?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  public confirmation(title: any, message?:any) {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

}
