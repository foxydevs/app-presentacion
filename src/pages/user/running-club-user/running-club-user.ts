import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, IonicPage } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';

@IonicPage()
@Component({
  selector: 'running-club-user',
  templateUrl: 'running-club-user.html'
})
export class RunningClubUserPage {
  //PROPIEDADES
  public events:any[] = [];
  public idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
     public loading: LoadingController,
     public eventsService: EventsService,
     public alertCtrl :AlertController
  ) {
    this.idUser = localStorage.getItem("currentId");
  }

  //GET
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.eventsService.getAllType(this.idUser, 2).then(response => {
      this.events = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear();
    })
  }

  //ABRIR FORMULARIO
  openForm(parameter?:any) {
    this.navCtrl.push('FormRunningClubUserPage', { parameter })
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //ENTER
  ionViewWillEnter(){
    this.getAll();
  }

}
