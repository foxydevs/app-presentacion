import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, AlertController, IonicPage, Slides, NavParams, ActionSheetController } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { path } from '../../../../app/config.module';

@IonicPage()
@Component({
  selector: 'subcategory-user',
  templateUrl: 'subcategory-user.html'
})
export class SubCategorysUserPage {
  @ViewChild(Slides) slides: Slides;
  //Propiedades
  public categorys:any[] = [];
  public idUser:any;
  public pictureCategories:any;
  public parameter:any;
  public baseId = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public categorysService: CategorysService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public usersService: UsersService,
    public modalController: ModalController,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.idUser = localStorage.getItem("currentId");
    this.parameter = this.navParams.get('parameter');
    if(localStorage.getItem('currentState') == '21') {
      this.openModalCreate();
    }
    this.loadSingleUser();
    this.getSingle(this.parameter);
  }

  public ngOnInit() {
    this.slides.effect = 'cube';
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    return currentIndex;
  }

  public loadSingleUser(){
    if((this.baseId+'')!='null'){
      this.usersService.getSingle(this.baseId)
      .then(response => {
        this.pictureCategories = response.pic2
        //console.clear();
      }).catch(error => {
        //console.clear();
      })
    }
  }

  //Cargar los productos
  public getSingle(id:any){
    this.categorysService.getSingle(id)
    .then(response => {
      this.categorys = [];
      this.categorys = response.subcategorys;
    }).catch(error => {
      console.clear
    })
  }

  //Ver Productos de la Categoria
  public seeProducts(parameter:any) {
    this.loading.create({
        content: "Cargando",
        duration: 750
    }).present();
    this.navCtrl.push('ProductsUserPage', { parameter });
  }

  //OPEN MODAL
  public openModal(data:any) {
    let parameter = {
      parameter: data,
      category: this.parameter
    }
    console.log(parameter)
    let chooseModal = this.modalController.create('CategoryFormUserPage', { parameter });
    chooseModal.onDidDismiss((data) => {
      if(data!='Close') {
        this.getSingle(this.parameter);
        setTimeout(() => {
          this.goToSlide(+this.slideChanged())      
        }, 1000);
      }
    });
    chooseModal.present();
  }

  //Eliminar Productos
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: 'Eliminar Categoría',
      message: '¿Deseas eliminar la categoría?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.categorysService.delete(id)
            .then(response => {
                this.loading.create({
                  content: "Eliminando Categoría...",
                  duration: 2000
                }).present();
                this.getSingle(this.parameter);
                setTimeout(() => {
                  this.goToSlide(+this.slideChanged())      
                }, 1000);
                console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.goToSlide(+this.slideChanged());
    this.slides.effect = 'cube';
    setTimeout(() => {
      this.goToSlide(this.slideChanged());
    }, 1000);
  }

  goToSlide(id:number) {
    this.slides.slideTo(id, 1000);
  }

  //ABIR MODAL AGREGAR
  public openModalCreate() {
    let parameter = {
      state: '2',
      id: null
    }
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadSingleUser();
      this.getSingle(this.parameter);
      refresher.complete();
    }, 2000);
  }

  presentActionSheet(parameter:any) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Elegir opción',
      buttons: [
        {
          text: 'Actualizar',
          icon: 'create',
          handler: () => {
            this.openModal(parameter);
          }
        },
        {
          text: 'Eliminar',
          icon: 'trash',
          handler: () => {
            this.delete(parameter);
          }
        },
        {
          text: 'Productos',
          icon: 'pricetag',
          handler: () => {
            this.navCtrl.push('ProductsUserPage', { parameter });
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          icon: 'close',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

}
