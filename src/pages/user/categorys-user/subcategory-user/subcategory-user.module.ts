import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubCategorysUserPage } from './subcategory-user';
 
@NgModule({
  declarations: [
    SubCategorysUserPage,
  ],
  imports: [
    IonicPageModule.forChild(SubCategorysUserPage),
  ],
  exports: [
    SubCategorysUserPage
  ]
})
export class SubCategorysUserPageModule {}