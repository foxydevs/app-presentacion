import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategorysCardUserPage } from './category-card-user';
 
@NgModule({
  declarations: [
    CategorysCardUserPage,
  ],
  imports: [
    IonicPageModule.forChild(CategorysCardUserPage),
  ],
  exports: [
    CategorysCardUserPage
  ]
})
export class CategorysCardUserPageModule {}