import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, AlertController, IonicPage, Slides, ActionSheetController } from 'ionic-angular';
import { CategorysService } from '../../../app/service/categorys.service';
import { UsersService } from '../../../app/service/users.service';
import { path } from "./../../../app/config.module";
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

@IonicPage()
@Component({
  selector: 'categorys-user',
  templateUrl: 'categorys-user.html'
})
export class CategorysUserPage {
  @ViewChild(Slides) slides: Slides;
  //Propiedades
  public categorys:any[] = [];
  public idUser:any;
  public pictureCategories:any;
  public baseId = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  subcategory = localStorage.getItem('currentOpcion21');

  constructor(
    public navCtrl: NavController,
    public categorysService: CategorysService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public usersService: UsersService,
    public modalController: ModalController,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.idUser = localStorage.getItem("currentId");
    if(localStorage.getItem('currentState') == '21') {
      this.openModalCreate();
    }
    this.loadSingleUser();
    this.getAll();
  }

  public ngOnInit() {
    this.slides.effect = 'cube';
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    return currentIndex;
  }

  public loadSingleUser(){
    if((this.baseId+'')!='null'){
      this.usersService.getSingle(this.baseId)
      .then(res => {
        this.pictureCategories = res.pic2
        //console.clear();
      }).catch(error => {
        //console.clear();
      })
    }
  }

  //Cargar los productos
  public getAll(){
    this.categorysService.getAllUser(this.idUser)
    .then(res => {
      this.categorys = [];
      this.categorys = res;
    }).catch(error => {
      console.clear
    })
  }

  //Ver Productos de la Categoria
  public seeProducts(parameter:any, subcategorys?:any, obj?:any) {
    console.log(subcategorys)
    console.log(subcategorys.length > 0)
    if(this.subcategory == '0') {
      //this.navCtrl.push('ProductsUserPage', { parameter });
      this.presentActionSheet3(parameter, obj)
    } else {
      if(subcategorys.length > 0) {
        //this.navCtrl.push('SubCategorysUserPage', { parameter });
        this.presentActionSheet2(parameter, obj);
      } else {
        this.presentActionSheet(parameter, obj);
      }      
    }
  }

  presentActionSheet(parameter:any, parameter2?:any) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Elegir opción',
      buttons: [
        {
          text: 'Actualizar',
          icon: 'create',
          handler: () => {
            this.openModal(parameter);
          }
        },
        {
          text: 'Eliminar',
          icon: 'trash',
          handler: () => {
            this.delete(parameter);
          }
        },
        {
          text: 'Anterior',
          icon: 'arrow-dropleft-circle',
          handler: () => {
            this.removeOrder(parameter2)
          }
        },
        {
          text: 'Siguiente',
          icon: 'arrow-dropright-circle',
          handler: () => {
            this.addOrder(parameter2);
          }
        },
        {
          text: 'Subcategoría',
          icon: 'apps',
          handler: () => {
            this.navCtrl.push('SubCategorysUserPage', { parameter });
          }
        }, {
          text: 'Productos',
          icon: 'pricetag',
          handler: () => {
            this.navCtrl.push('ProductsUserPage', { parameter });
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          icon: 'close',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  presentActionSheet2(parameter:any, parameter2?:any) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Elegir opción',
      buttons: [
        {
          text: 'Actualizar',
          icon: 'create',
          handler: () => {
            this.openModal(parameter);
          }
        },
        {
          text: 'Eliminar',
          icon: 'trash',
          handler: () => {
            this.delete(parameter);
          }
        },
        {
          text: 'Anterior',
          icon: 'arrow-dropleft-circle',
          handler: () => {
            this.removeOrder(parameter2)
          }
        },
        {
          text: 'Siguiente',
          icon: 'arrow-dropright-circle',
          handler: () => {
            this.addOrder(parameter2);
          }
        },
        {
          text: 'Subcategoría',
          icon: 'apps',
          handler: () => {
            this.navCtrl.push('SubCategorysUserPage', { parameter });
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          icon: 'close',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  presentActionSheet3(parameter:any, parameter2?:any) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Elegir opción',
      buttons: [
        {
          text: 'Actualizar',
          icon: 'create',
          handler: () => {
            this.openModal(parameter);
          }
        },
        {
          text: 'Eliminar',
          icon: 'trash',
          handler: () => {
            this.delete(parameter);
          }
        },
        {
          text: 'Anterior',
          icon: 'arrow-dropleft-circle',
          handler: () => {
            this.removeOrder(parameter2)
          }
        },
        {
          text: 'Siguiente',
          icon: 'arrow-dropright-circle',
          handler: () => {
            this.addOrder(parameter2);
          }
        },
        {
          text: 'Productos',
          icon: 'pricetag',
          handler: () => {
            this.navCtrl.push('ProductsUserPage', { parameter });
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          icon: 'close',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  //OPEN MODAL
  public openModal(data:any) {
    let parameter = {
      parameter: data
    }
    let chooseModal = this.modalController.create('CategoryFormUserPage', { parameter });
    chooseModal.onDidDismiss((data) => {
      if(data!='Close') {
        this.getAll();
        setTimeout(() => {
          this.goToSlide(+this.slideChanged())      
        }, 1000);
      }
    });
    chooseModal.present();
  }

  //Eliminar Productos
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: 'Eliminar Categoría',
      message: '¿Deseas eliminar la categoría?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.categorysService.delete(id)
            .then(res => {
                this.loading.create({
                  content: "Eliminando Categoría...",
                  duration: 2000
                }).present();
                this.getAll();
                setTimeout(() => {
                  this.goToSlide(+this.slideChanged())      
                }, 1000);
                console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.goToSlide(+this.slideChanged());
    this.slides.effect = 'cube';
    setTimeout(() => {
      this.goToSlide(this.slideChanged());
    }, 1000);
  }

  goToSlide(id:number) {
    this.slides.slideTo(id, 1000);
  }

  //ABIR MODAL AGREGAR
  public openModalCreate() {
    let parameter = {
      state: '2',
      id: null
    }
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadSingleUser();
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //SUMAR ORDEN
  addOrder(p:any) {
    console.log(p)
    for (var x in this.categorys) {
      if (this.categorys[x] == p) {
        this.categorys[x].orden = +this.categorys[x].orden + 1;
        this.update(this.categorys[x]);
        console.log(this.categorys[x].orden)
      }
    }
  }

  //SUMAR ORDEN
  removeOrder(p:any) {
    console.log(p)
    for (var x in this.categorys) {
      if (this.categorys[x] == p) {
        if(this.categorys[x].orden > 1) {
          this.categorys[x].orden = +this.categorys[x].orden - 1;
          this.update(this.categorys[x]);
          console.log(this.categorys[x].orden)
        }
      }
    }
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.categorysService.update(formValue)
    .then(res => {
      console.log(res)
      this.getAll();
    }).catch(error => {
      console.clear
    });
  }

}
