import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormTipsBibliaUserPage } from './form-tips-biblia-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormTipsBibliaUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormTipsBibliaUserPage),
    PipeModule
  ],
  exports: [
    FormTipsBibliaUserPage
  ]
})
export class FormTipsBibliaUserPageModule {}