import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TipsBibliaUserPage } from './tips-biblia-user';
 
@NgModule({
  declarations: [
    TipsBibliaUserPage,
  ],
  imports: [
    IonicPageModule.forChild(TipsBibliaUserPage),
  ],
  exports: [
    TipsBibliaUserPage
  ]
})
export class TipsBibliaUserPageModule {}