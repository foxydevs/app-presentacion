import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { CategorysService } from '../../../../app/service/categorys.service';
import { DocumentsService } from '../../../../app/service/documents.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'form-documents-user',
  templateUrl: 'form-documents-user.html',
})
export class FormDocumentUserPage {
  //PROPIEDADES
  public parameter:any;
  public title:any;
  public disabledBtn:boolean;
  public basePath:string = path.path;
  selectItem:any = 'actualizar';
  public categories:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data = {
    name: '',
    description: '', 
    pictures: 'https://graficasizquierdo.es/wp-content/uploads/2017/04/pdf.png',
    id: '',
    picture: '',
    user_created: path.id,
    category: '',
    archivo: '',
    price: 0,
    quantity: 0,
    cost: 0
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public mainService: DocumentsService,
    public secondService: CategorysService,
  ) {
    this.parameter = this.navParams.get('parameter');
    console.log(this.parameter)
    this.getAll();
    if(this.parameter) {
      this.title = "Edición Documento";
      this.getSingle(this.parameter); 
    } else {
      this.title = "Agregar Documento";
    }
  }

  //Cargar los productos
  public getAll(){
    this.secondService.getAllUser(path.id)
    .then(response => {
      this.categories = response;
    }).catch(error => {
      console.clear
    })
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    this.data.archivo = $('img[alt="Avatar1"]').attr('src');
    console.log(this.data)
    if(this.parameter) {
      this.update(this.data)
    } else {
      this.create(this.data)
    }
  }

  //AGREGAR
  create(formValue:any) {
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.mainService.create(formValue)
    .then(response => {
      load.dismiss();
      this.confirmation('Documento Agregado', 'El documento fue agregado exitosamente.');
      this.data.id = response.id;
      this.parameter = response.id;
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.mainService.update(formValue)
    .then(response => {
      load.dismiss();
      this.confirmation('Documento Actualizado', 'El documento fue actualizado exitosamente.');
      this.navCtrl.pop();
      console.log(response);
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      console.log(this.data)
      this.data.pictures = 'https://graficasizquierdo.es/wp-content/uploads/2017/04/pdf.png';
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //SUBIR DOCUMENTO
  uploadDocument(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == 'application/pdf') {
      if(size<(10*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'goshem'
          },
          function(respuesta) {
              console.log(respuesta)
            $('#imgAvatar1').attr("src", respuesta.url)
            $('#imgAvatar').attr("src", 'https://graficasizquierdo.es/wp-content/uploads/2017/04/pdf.png')
            $("#"+id).val('')
          }
        );
      } else {
        this.message('El documento es demasiado grande.')
      }
    } else {
      this.message('El tipo de documento no es válido.')
    }
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }
}
