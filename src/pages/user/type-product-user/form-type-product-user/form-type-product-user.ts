import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Clipboard } from '../../../../../node_modules/@ionic-native/clipboard';
import { PresentationService } from '../../../../app/service/presentation.service';

@IonicPage()
@Component({
  selector: 'form-type-product-user',
  templateUrl: 'form-type-product-user.html',
})
export class FormTypeProductUserPage {
  //PROPIEDADES
  public parameter:any;
  public title:any;
  public disabledBtn:any;
  public basePath:string = path.path;
  public baseUserId = path.id;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data = {
    name: '',
    description: '',
    video: '',
    user_created: path.id,
    app: path.id,
    picture: localStorage.getItem('currentPicture'),
    id: ''
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public iab: InAppBrowser,
    public loading: LoadingController,
    public mainService: PresentationService,
    public clipboard: Clipboard
  ) {
    this.parameter = this.navParams.get('parameter')
    if(this.parameter) {
      this.title = "Edición Tipo";
      this.getSingle(this.parameter);     
    } else {
      this.title = "Agregar Tipo";
    }
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.name) {
      if(this.data.description) {
        if(this.parameter) {
          console.log(this.data)
          this.update(this.data)
        } else {
          console.log(this.data)
          this.create(this.data)
        }
      } else {
        this.message('La descripción es requerida.');
      }
    } else {
      this.message('El título es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Tipo Agregado', 'El tipo fue agregado exitosamente.');
      this.data.id = response.id;
      this.parameter = response.id;
      this.title = "Edición Tipo";
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Tipo Actualizado', 'El tipo fue actualizado exitosamente.');
      this.navCtrl.pop();
      console.log(response);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el blog?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //OBTENER DATOS
  getComments(parameter:any) {
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
    }).catch(error => {
      console.log(error)
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }
}
