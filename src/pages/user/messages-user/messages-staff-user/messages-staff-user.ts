import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, IonicPage } from 'ionic-angular';
import { MessagesService } from '../../../../app/service/messages.service';
import { UsersService } from '../../../../app/service/users.service';

@IonicPage()
@Component({
  selector: 'messages-staff-user',
  templateUrl: 'messages-staff-user.html'
})
export class MessagesStaffUserPage {
  //Propiedades
  public idUser:any;
  public messagesTemp2:any[] = [];  
  public messages:any[] = [];
  public users:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public messagesService: MessagesService,
    public usersService: UsersService
  ) {
    this.idUser = localStorage.getItem("currentId");
  }

  //OBTENER MENSAJES
  public getMessages(id:any) {
    let messagesTemp:any[] = [];
    this.messagesService.getUserReceipt(id)
    .then(response => {
      for(let x of response) {
        //console.log(x.user_send)
        let user = {
          user: this.returnNameUser(x.user_send),
          picture: this.returnPicture(x.user_send),
          id: x.user_send,
          id_message: x.id,
          tipo: x.tipo
        }
        messagesTemp.push(user);
      }
      this.messagesTemp2 = this.removeDuplicates(messagesTemp, 'id');      
      console.clear;
    }).catch(error => {
      console.clear;
    })
    this.messagesService.getUserSend(id)
    .then(response => {
      for(let x of response) {
        //console.log(x.user_receipt)
        if(x.tipo == "1") {
          let user = {
            user: this.returnNameUser(x.user_receipt),
            picture: this.returnPicture(x.user_receipt),
            id: x.user_receipt,
            id_message: x.id,
            tipo: x.tipo
          }
          messagesTemp.push(user); 
        }           
      }
      this.messagesTemp2 = this.removeDuplicates(messagesTemp, 'id');    
      console.clear;
    }).catch(error => {
      console.clear;
    })
  }

  //ORDENAR MENSAJES
  public getMessagesOrdered() {
    setTimeout(() => {
      this.getMessages(this.idUser);      
    }, 2000);
    setTimeout(() => {
      this.compare();      
      this.messages = this.messagesTemp2;
    }, 3000);    
  }

  public compare() {
    this.messagesTemp2.sort(function(a, b) {
      return a.id_message-b.id_message; /* Modificar si se desea otra propiedad */
    });
    this.messagesTemp2.reverse();
    localStorage.setItem('currentMessagesStaff', JSON.stringify(this.messagesTemp2));
  }

  //RETORNAR NOMBRE COMPLETO
  public returnNameUser(idUser:any):any {
    for(var i = 0; i < this.users.length; i++) {
      if(this.users[i].id == idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //CARGAR USUARIOS
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear
    })
  }

  //RETORNAR IMAGEN DE PERFIL
  public returnPicture(idUser:any):any {
    for(var i = 0; i < this.users.length; i++) {
      if(this.users[i].id == idUser) {
        return this.users[i].picture;
      }
    }
  }

  public messagesClientUser(parameter:any) {
    this.navCtrl.push('MessagesClientUserPage', { parameter });
  }

  public removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject  = {};

    for(var i in originalArray) {
       lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for(i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
    newArray.reverse();
    return newArray;
  }

  ionViewWillEnter() {
    this.loadAllUsers();
    this.messages = JSON.parse(localStorage.getItem('currentMessagesStaff'));
    this.getMessagesOrdered();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getMessagesOrdered();
      refresher.complete();
    }, 2000);
  }

}
