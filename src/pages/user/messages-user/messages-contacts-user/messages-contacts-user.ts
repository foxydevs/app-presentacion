import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, IonicPage } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'messages-contacts-user',
  templateUrl: 'messages-contacts-user.html'
})
export class MessagesContactsUserPage {
  //PROPIEDADES
  public idUser:any;
  public users:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public search:any;

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public usersService: UsersService
  ) {
    this.idUser = localStorage.getItem("currentId");
    this.getAll();
  }

  //CARGAR USUARIOS
  public getAll(){
    this.usersService.getClients(this.idUser)
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear
    })
  }

  public sendMessage(param:any) {
    let parameter = {
      parameter: param,
      message: 'New'
    }
    console.log(parameter)
    this.navCtrl.push('SendMessagesClientUserPage', { parameter });
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-item").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  ionViewWillEnter() {
    this.getAll();
  }
}
