import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { ChallengeService } from '../../../app/service/challenge.service';
import { MotivationService } from '../../../app/service/motivation.service';

@IonicPage()
@Component({
  selector: 'leadearboard-user',
  templateUrl: 'leadearboard-user.html',
})
export class LeadearboardUserPage {
  //PROPIEDADES
  selectItem:any = 'retos';
  public challenges:any[] = [];
  public motivations:any[] = [];
  public members:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public modal: ModalController,
    public loading: LoadingController,
    public mainService: ChallengeService,
    public secondService: MotivationService,
  ) {
  }

  openFormChallenge(parameter?:any) {
    this.selectItem = 'retos';
    this.navCtrl.push('FormChallengeUserPage', { parameter });
  }

  openFormMotivation(parameter?:any) {
    this.selectItem = 'motivacion';
    this.navCtrl.push('FormMotivationUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAll()
    .then(response => {
      this.challenges = []
      this.challenges = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR LOS RETOS
  public getAllSecond() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAll()
    .then(response => {
      this.motivations = []
      this.motivations = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //GET SCORE
  getAllThird() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllScore()
    .then(response => {
      this.members = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  ionViewWillEnter() {
    if(this.selectItem == 'retos') {
      this.getAll();
    } else if(this.selectItem == 'motivacion') {
      this.getAllSecond();
    }
  }

}
