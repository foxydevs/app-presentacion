import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormMotivationUserPage } from './form-motivation-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormMotivationUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormMotivationUserPage),
    PipeModule
  ],
  exports: [
    FormMotivationUserPage
  ]
})
export class FormMotivationUserPageModule {}