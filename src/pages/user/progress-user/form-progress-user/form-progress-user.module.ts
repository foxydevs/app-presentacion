import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormProgressUserPage } from './form-progress-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormProgressUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormProgressUserPage),
    PipeModule
  ],
  exports: [
    FormProgressUserPage
  ]
})
export class FormProgressUserPageModule {}