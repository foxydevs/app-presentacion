import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, IonicPage } from 'ionic-angular';
import { TipsService } from '../../../app/service/tips.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'tips-user',
  templateUrl: 'tips-user.html',
})
export class TipsUserPage {
  //PROPIEDADES
  public table:any[] = [];
  public search:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  idUser = localStorage.getItem("currentId");
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public mainService: TipsService,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams
  ) {
  }

  //CARGAR PRODUCTOS
  public loadAll() {
    let id = localStorage.getItem('currentId');
    this.mainService.getAllUser(id)
    .then(response => {
      this.table = [];
      this.table = response;
    }).catch(error => {
      console.clear
    })
  } 

  openForm(parameter?:any) {
    this.navCtrl.push('FormTipsUserPage', { parameter })
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-item").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }


  public detailTip(parameter:any) {
    this.loading.create({
      content: 'Cargando...',
      duration: 1000
    }).present();
    this.navCtrl.push('SeeTipsUserPage', { parameter })   
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadAll();
      refresher.complete();
    }, 2000);
  }

  ionViewWillEnter() {
    this.loadAll();
  }

}
