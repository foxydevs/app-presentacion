import { Component } from '@angular/core';
import { IonicPage, ViewController, Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'coach-modal-user',
  templateUrl: 'coach-modal-user.html'
})
export class CoachModalUserPage {
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    private view: ViewController,
    private platform: Platform,
    private iab: InAppBrowser
  ){
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }

  openBrowserPDF(urlObject:any, title:any) {
    //this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    if (this.platform.is('android')) {
      this.iab.create('http://docs.google.com/gview?url=' + urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#003D6E');      
    } else {
      this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    }
  }

  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#E67E22');
  }
}
