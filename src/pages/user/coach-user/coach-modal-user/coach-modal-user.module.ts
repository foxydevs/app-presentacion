import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CoachModalUserPage } from './coach-modal-user';
 
@NgModule({
  declarations: [
    CoachModalUserPage,
  ],
  imports: [
    IonicPageModule.forChild(CoachModalUserPage),
  ],
  exports: [
    CoachModalUserPage
  ]
})
export class CoachModalUserPageModule {}