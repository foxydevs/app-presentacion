import { Component } from '@angular/core';
import { IonicPage, ModalController, Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'coach-user',
  templateUrl: 'coach-user.html'
})
export class CoachUserPage {
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    private modalController: ModalController,
    
  ){
  }

  public openModal() {
    let chooseModal = this.modalController.create('CoachModalUserPage');
    chooseModal.present();
  }

    


}
