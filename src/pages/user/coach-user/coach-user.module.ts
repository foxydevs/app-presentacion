import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CoachUserPage } from './coach-user';
 
@NgModule({
  declarations: [
    CoachUserPage,
  ],
  imports: [
    IonicPageModule.forChild(CoachUserPage),
  ],
  exports: [
    CoachUserPage
  ]
})
export class CoachUserPageModule {}