import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, IonicPage } from 'ionic-angular';
import * as moment from 'moment';
import { CitesService } from '../../../../app/service/cites.service';
import { AlertController } from '../../../../../node_modules/ionic-angular/components/alert/alert-controller';
import { UsersService } from '../../../../app/service/users.service';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { path } from '../../../../app/config.module';
import { SelectSearchableComponent } from '../../../../../node_modules/ionic-select-searchable';
import { ProductsService } from '../../../../app/service/products.service';

@IonicPage()
@Component({
  selector: 'form-cites-user',
  templateUrl: 'form-cites-user.html',
})
export class FormCitesUserPage {
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  selectItem:any = 'actualizar';
  parameter:any;
  disabledBtn:boolean;
  users:any[] = [];
  products:any[] = [];
  title:any;
  data = {
    id: '',
    fechaInicio: '',
    fechaFin: '',
    aprobacion: 0,
    comentario: '',
    state: '0',
    titulo: '',
    descripcion: '',
    user: path.id,
    client: '',
    end: '',
    start: '',
    product: ''
  }
  data2:any = [];
  dateStart: any;
  dateEnd: any;
  hourStart:any;
  hourEnd:any;

  event = { startTime: new Date().toISOString(), endTime: new Date().toISOString(), allDay: false };
  minDate = new Date().toISOString();
 
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    public loading: LoadingController,
    public mainService: CitesService,
    public secondService: UsersService,
    public thirdService: ProductsService,
    public toast: ToastController,
    public alertCtrl: AlertController) {
    this.parameter = this.navParams.get('parameter');
    this.getAll();
    this.getAllThird();
    if(this.parameter) {
      this.title = 'Edición Cita'
      this.getSingle(this.parameter)
    } else {
      let fechaahora = new Date();
      this.data.fechaInicio = moment(fechaahora).format();
      this.data.fechaFin = moment(fechaahora).format();
      this.title = 'Nueva Cita'  
    }
    //let preselectedDate = moment(this.navParams.get('selectedDay')).format();
    //this.event.startTime = preselectedDate;
    //this.event.endTime = preselectedDate;
  }
 
  cancel() {
    this.viewCtrl.dismiss();
  }
 
  saveChanges() {
    //this.viewCtrl.dismiss(this.event);
    if(this.data.aprobacion == 1) {
      this.data.aprobacion = 1;
    } else {
      this.data.aprobacion = 0;
    }
    this.disabledBtn = true;
    if(this.parameter) {
      //this.title = 'Edición Cita'
      this.update(this.data)
    } else {
      //this.title = 'Edición Cita'
      this.create(this.data)   
    }
  }

  /*//COMPROBAR FECHA
  comprobar(dateInicio:any, dateFin:any) {
    //FECHA INICIO COMPROBACION
    let fechaInicio = (dateInicio.replace('T','%20').replace('Z', '')).split(".");
    //FECHA FIN COMPROBACION
    let fechaFin = (dateFin.replace('T','%20').replace('Z', '')).split(".");
    let data = {
      fechaInicio: fechaInicio[0],
      fechaFin: fechaFin[0],
    }
    this.mainService.getByCiteUser(this.data.user, data)
    .then(response => {
      console.log(response)
      if(response.length > 0) {
        this.confirmation('Hora No Disponible', 'Debe cambiar la hora de la cita.');
        this.data.fechaInicio = response[0].fechaFin;
      }
    }).catch(error => {
      console.clear
    });
  }*/

  //AGREGAR
  create(formValue:any) {
    console.log(formValue)
    let fechainicio = (formValue.fechaInicio.replace('T',' ').replace('Z', '')).split(".");
    formValue.fechaInicio = fechainicio[0];
    let fechafin = (formValue.fechaFin.replace('T',' ').replace('Z', '')).split(".");
    formValue.fechaFin = fechafin[0];
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Cita Agregada', 'La cita fue agregada exitosamente.');
      this.data.id = response.id;
      this.parameter = response.id;
      this.title = 'Edición Cita'
      this.getSingle(this.parameter);
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    let fechainicio = (formValue.fechaInicio.replace('T',' ').replace('Z', '')).split("-")
    formValue.fechaInicio = fechainicio[0] + '-' + fechainicio[1] + '-' + fechainicio[2];
    let fechafin = (formValue.fechaFin.replace('T',' ').replace('Z', '')).split("-")
    formValue.fechaFin = fechafin[0] + '-' + fechafin[1] + '-' + fechafin[2];
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Cita Actualizada', 'La cita fue actualizada exitosamente.');
      this.disabledBtn = false;
      this.getSingle(this.parameter);
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      this.data.fechaFin = moment(response.fechaFin).format();
      this.data.fechaInicio = moment(response.fechaInicio).format();
      this.data2 = response;
      this.dateStart = moment(response.fechaInicio).format('LL');
      this.dateEnd = moment(response.fechaFin).format('LL');
      this.hourStart = moment(response.fechaInicio).format('HH:mm');
      this.hourEnd = moment(response.fechaFin).format('HH:mm');
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }


  //CARGAR USUARIOS
  public getAll(){
    this.secondService.getClients(path.id)
    .then(response => {
      this.users = [];
      this.users = response;
      console.log(response)
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR USUARIOS
  public getAllThird(){
    this.thirdService.getAllUser(path.id)
    .then(response => {
      this.products = [];
      this.products = response;
      console.log(response)
    }).catch(error => {
      console.clear
    })
  }
  
  portChange(event: {
    component: SelectSearchableComponent,
    value: any 
  }) {
    console.log('port:', event.value);
    this.data.client = event.value.id
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }
 
}