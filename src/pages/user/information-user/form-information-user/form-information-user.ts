import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { UsersService } from '../../../../app/service/users.service';
import { ViewController } from '../../../../../node_modules/ionic-angular/navigation/view-controller';
import { InformationService } from '../../../../app/service/information.service';

@IonicPage()
@Component({
  selector: 'form-information-user',
  templateUrl: 'form-information-user.html',
})
export class FormInformationUserPage {
  //PROPIEDADES
  parameter:any;
  users:any[] = [];
  title:any;
  disabledBtn:boolean;
  basePath:string = path.path;
  selectItem:any = 'actualizar';
   data = {
    title: '',
    link: '',
    icono: '',
    color: '',
    app: localStorage.getItem('currentId'),
    id: '',
    abierto: 1,
    state: 0,
    type: 0
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public view: ViewController,
    public mainService: InformationService,
    public secondService: UsersService,
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getAllSecond();
    if(this.parameter) {
      this.title = "Edición Información";
      this.getSingle(this.parameter);  
    } else {
      this.title = "Agregar Información";
    }
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.title) {
      this.disabledBtn = true;
      if(this.parameter) {
        console.log(this.data)
        this.update(this.data)
      } else {
        console.log(this.data)
        this.create(this.data)
      }
    } else {
      this.message('El título es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Información Agregada', 'La Información fue agregada exitosamente.');
      this.parameter = response.id;
      this.data.id = response.id;
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.confirmation('Información Actualizada', 'La Información fue actualizada exitosamente.');
      this.navCtrl.pop();
      console.log(response);
      this.disabledBtn = false;
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar la información?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            });
            load.present();
            this.mainService.delete(id)
            .then(response => {
              load.dismiss();
              this.navCtrl.pop();
              console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    this.secondService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }
}
