import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormInformationUserPage } from './form-information-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormInformationUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormInformationUserPage),
    PipeModule
  ],
  exports: [
    FormInformationUserPage
  ]
})
export class FormInformationUserPageModule {}