import { Component } from '@angular/core';
import { NavController, ModalController, IonicPage } from 'ionic-angular';
import { UsersService } from '../../../app/service/users.service';
import { path } from '../../../app/config.module';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { InformationService } from '../../../app/service/information.service';
import { ScheduleService } from '../../../app/service/schedule.service';

//JQUERY
declare var google;

@IonicPage()
@Component({
  selector: 'information-user',
  templateUrl: 'information-user.html'
})
export class InformationUserPage {
  //PROPIEDADES
  public data:any = [];
  public schedules:any = [];
  public information:any = [];
  public profilePicture:any;
  public portadaPicture:any;
  public map: any;
  public idUser:any = path.id;
  public last_latitud:any;
  public last_longitud:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public userService: UsersService,
    public iab: InAppBrowser,
    public modal: ModalController,
    public mainService: InformationService,
    public secondService: ScheduleService,
  ) {
    this.userService.getSingle(this.idUser)
    .then(response => {
    this.profilePicture = response.picture;
    this.portadaPicture = response.pic3;
    this.last_latitud = response.last_latitud;
    this.last_longitud = response.last_longitud;
    this.data = response;
      this.loadMapUpdate(this.last_latitud, this.last_longitud);
    }).catch(error => {
        console.clear()
    })
  }

  //CARGAR USUARIOS
  public getAll(){
    this.mainService.getAllUser(this.idUser)
    .then(response => {
      this.information = [];
      this.information = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    this.secondService.getAllUser(this.idUser)
    .then(response => {
      this.schedules = [];
      this.schedules = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR MAPA ACTUALIZAR
  public loadMapUpdate(lat:any, lon:any) {
  let latitude = lat;
  let longitude = lon;
  this.last_latitud = latitude.toString();
  this.last_longitud = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.last_latitud = evt.latLng.lat();
      this.last_longitud = evt.latLng.lng();
    });
  }

  //ABRIR HORARIO
  openModalSchedule(parameter:any) {
    let chooseModal = this.modal.create('FormScheduleUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      this.getAllSecond(); 
    });
    chooseModal.present();
  }

  //ABRIR INFORMACION
  openModalInformation(parameter:any) {
    let chooseModal = this.modal.create('FormInformationUserPage', { parameter });
    chooseModal.onDidDismiss(data => {
      this.getAll();
    });
    chooseModal.present();
  }

   //CARGAR PAGINA
   openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
  }

  ionViewWillEnter() {
    this.getAll();
    this.getAllSecond();
  }

}