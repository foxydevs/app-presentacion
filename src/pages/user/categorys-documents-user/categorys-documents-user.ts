import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, IonicPage } from 'ionic-angular';
import { CategorysService } from '../../../app/service/categorys.service';
import { UsersService } from '../../../app/service/users.service';
import { path } from "./../../../app/config.module";
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

@IonicPage()
@Component({
  selector: 'categorys-documents-user',
  templateUrl: 'categorys-documents-user.html'
})
export class CategorysDocumentsUserPage {
  //Propiedades
  public categorys:any[] = [];
  public idUser:any;
  public pictureCategories:any;
  public baseId = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public categorysService: CategorysService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public usersService: UsersService,
    public modal: ModalController,
  ) {
    this.idUser = localStorage.getItem("currentId");
    if(localStorage.getItem('currentState') == '21') {
      this.openModalCreate();
    }
    this.loadSingleUser();
    this.loadAll();
  }

  public loadSingleUser(){
    if((this.baseId+'')!='null'){
      this.usersService.getSingle(this.baseId)
      .then(response => {
        this.pictureCategories = response.pic2
        //console.clear();
      }).catch(error => {
        //console.clear();
      })
    }
  }

  //Cargar los productos
  public loadAll(){
    this.categorysService.getAllUser(this.idUser)
    .then(response => {
      this.categorys = []
      this.categorys = response;
    }).catch(error => {
      console.clear
    })
  }

  //Ver Productos de la Categoria
  public seeProducts(parameter:any) {
    this.loading.create({
        content: "Cargando",
        duration: 750
    }).present();
    this.navCtrl.push('DocumentsUserPage', { parameter });
  }

  //Ver Formulario Agregar
  public viewForm() {
    let parameter:string = 'new';
    this.navCtrl.push('CategoryFormUserPage', { parameter });
  }

  public updateForm(parameter:any) {
    this.navCtrl.push('CategoryFormUserPage', { parameter });
  }

  //Eliminar Productos
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar la categoría?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.categorysService.delete(id)
            .then(response => {
                this.loading.create({
                  content: "Eliminando Categoría...",
                  duration: 2000
                }).present();
                this.loadAll();
                console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  //ABIR MODAL AGREGAR
  public openModalCreate() {
    let parameter = {
      state: '2',
      id: null
    }
    let chooseModal = this.modal.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadSingleUser();
      this.loadAll();
      refresher.complete();
    }, 2000);
  }

}
