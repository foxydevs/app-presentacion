import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { ActivePauseService } from '../../../app/service/activepause.service';

@IonicPage()
@Component({
  selector: 'active-pause-user',
  templateUrl: 'active-pause-user.html',
})
export class ActivePauseUserPage {
  //PROPIEDADES
  public table:any[] = [];
  public idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: ActivePauseService,
  ) {
    this.idUser = localStorage.getItem('currentId');
  }

  openForm(parameter?:any) {
    this.navCtrl.push('FormActivePauseUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll(this.idUser);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idUser);
      refresher.complete();
    }, 2000);
  }

}
