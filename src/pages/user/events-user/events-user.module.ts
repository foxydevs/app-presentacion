import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventsUserPage } from './events-user';
 
@NgModule({
  declarations: [
    EventsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(EventsUserPage),
  ],
  exports: [
    EventsUserPage
  ]
})
export class EventsUserPageModule {}