import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormEventsUserPage } from './form-events-user';
import { PipeModule } from '../../../../pipes/pipes.module';
import { SocialSharing } from '@ionic-native/social-sharing';
 
@NgModule({
  declarations: [
    FormEventsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormEventsUserPage),
    PipeModule
  ],
  exports: [
    FormEventsUserPage
  ],
  providers: [
    SocialSharing
  ]
})
export class FormEventsUserPageModule {}