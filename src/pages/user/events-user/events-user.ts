import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, IonicPage, ModalController } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';

@IonicPage()
@Component({
  selector: 'events-user',
  templateUrl: 'events-user.html'
})
export class EventsUserPage {
  //PROPIEDADES
  public events:any[] = [];
  public idUser:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
     public loading: LoadingController,
     public eventsService: EventsService,
     public alertCtrl: AlertController,
     public modalController: ModalController
  ) {
    this.idUser = localStorage.getItem("currentId");
  }

  //GET
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.eventsService.getAllType(this.idUser, 1).then(response => {
      this.events = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear();
    })
  }

  //ABRIR FORMULARIO
  openForm(parameter?:any) {
    this.navCtrl.push('FormEventsUserPage', { parameter })
  }

  //OPEN MODAL
  public openModal(parameter?:any) {
    let chooseModal = this.modalController.create('FormEventsUserPage', { parameter });
    chooseModal.onDidDismiss((data) => {
      this.getAll();  
    });
    chooseModal.present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //ENTER
  ionViewWillEnter(){
    this.getAll();
  }

}
