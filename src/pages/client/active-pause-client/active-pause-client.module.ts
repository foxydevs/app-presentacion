import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivePauseClientPage } from './active-pause-client';
 
@NgModule({
  declarations: [
    ActivePauseClientPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivePauseClientPage),
  ],
  exports: [
    ActivePauseClientPage
  ]
})
export class ActivePauseClientPageModule {}