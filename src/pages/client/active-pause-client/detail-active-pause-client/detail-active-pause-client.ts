import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { UsersService } from '../../../../app/service/users.service';
import { ActivePauseService } from '../../../../app/service/activepause.service';

@IonicPage()
@Component({
  selector: 'detail-active-pause-client',
  templateUrl: 'detail-active-pause-client.html',
})
export class DetailActivePauseClientPage {
  //PROPIEDADES
  public parameter:any;
  public users:any[] = [];
  public comments:any[] = [];
  public data:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public mainService: ActivePauseService,
    public secondService: UsersService,
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getSingle(this.parameter);
    this.getAllSecond();
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
      this.getAllComments(response.id);
    }).catch(error => {
      console.log(error)
    });
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    this.secondService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR COMENTARIOS POR PRODUCTOS
  public getAllComments(id:any) {
    this.comments = [];
    this.mainService.getCommentsByWorkouts(id)
    .then(res => {
      for(let x of res) {
        let comment = {
          comment: x.comment,
          fecha: x.created_at,
          user: this.returnNameUser(x.user),
          picture: this.returnPicture(x.user)
        }
        this.comments.push(comment);
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear();
    });
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //Devolver el Nombre del Usuario
   public returnPicture(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].picture;
      }
    }
  }

  
  public addComment(parameter:any) {
    //this.navCtrl.push(CommentsWorkoutsClientPage, { parameter })
  }
}
