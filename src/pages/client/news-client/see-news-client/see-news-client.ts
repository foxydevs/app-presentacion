import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { UsersService } from '../../../../app/service/users.service';
import { Geolocation } from '@ionic-native/geolocation';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { NewsService } from '../../../../app/service/news.service';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { IonicPage } from '../../../../../node_modules/ionic-angular/navigation/ionic-page';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'see-news-client',
  templateUrl: 'see-news-client.html'
})
export class SeeNewsClientPage implements OnInit {
  public users:any[] = [];
  public comments:any[] = [];
  public parameter:any;
  public trustedVideoUrl: SafeResourceUrl;
  public news = {
    title: '',
    description: '',
    link: '',
    video: '',
    picture: '',
    id: '',
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public usersService: UsersService,
    public loading: LoadingController,
    public geolocation: Geolocation,
    public newsService: NewsService,
    public alertCtrl: AlertController,
    public iab: InAppBrowser,
    //public streamingMedia: StreamingMedia,
    //public youtube: YoutubeVideoPlayer,
    public domSanitizer: DomSanitizer
  ) {
    this.loadAllUsers();
    this.parameter = this.navParams.get('parameter');
  }

  ngOnInit() {
  }

  //CARGAR USUARIOS
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR LA NOTICIA
  public loadSingleNew(idEvent:any) {
    this.newsService.getSingle(idEvent)
    .then(response => {
      this.news.description = response.description;
      this.news.title = response.title;
      this.news.link = response.link;
      this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(response.video);
      this.news.picture = response.picture;
      this.news.id = response.id;
    }).catch(error => {
      console.clear;
    })
  }

  //CARGAR COMENTARIOS POR NOTICIAS
  public loadCommentsByNew(id:any) {
    this.comments = [];
    this.newsService.getAllCommentsByNew(id)
    .then(res => {
      for(let x of res) {
        let comment = {
          comment: x.comment,
          fecha: x.created_at,
          user: this.returnNameUser(x.user),
          picture: this.returnPicture(x.user)
        }
        this.comments.push(comment);
      }
      this.comments.reverse();
    }).catch(error => {
      console.clear();
    });
  }

  //RETORNAR NOMBRE DE USUARIO
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //RETORNAR IMAGEN USUARIO
  public returnPicture(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].picture;
      }
    }
  }

  public openPage(parameter:any) {
    this.navCtrl.push('SeeNewsClientCommentPage', { parameter });
  }

  /*public startVideo() {
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Finished Video') },
      errorCallback: (e) => { console.log('Error: ', e) },
      orientation: 'portrait'
    };
 
    this.streamingMedia.playVideo(this.news.link, options);
  }*/

  public youtubeVideo() {
    //this.youtube.openVideo(this.news.link);
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#E67E22');
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.loadSingleNew(this.parameter);
      this.loadCommentsByNew(this.parameter)
    }, 800);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadCommentsByNew(this.parameter);
      refresher.complete();
    }, 2000);
  }

}