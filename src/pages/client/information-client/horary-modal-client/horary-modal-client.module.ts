import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { HoraryModalClientPage } from './horary-modal-client';
 
@NgModule({
  declarations: [
    HoraryModalClientPage,
  ],
  imports: [
    IonicPageModule.forChild(HoraryModalClientPage),
    PipeModule
  ],
  exports: [
    HoraryModalClientPage
  ]
})
export class HoraryModalClientPageModule {}