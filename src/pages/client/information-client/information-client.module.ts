import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformationClientPage } from './information-client';
import { PipeModule } from '../../../pipes/pipes.module';
import { CallNumber } from '../../../../node_modules/@ionic-native/call-number';
import { AndroidPermissions } from '../../../../node_modules/@ionic-native/android-permissions';
import { SliderService } from '../../../app/service/sliders.service';
 
@NgModule({
  declarations: [
    InformationClientPage,
  ],
  imports: [
    IonicPageModule.forChild(InformationClientPage),
    PipeModule,
  ],
  exports: [
    InformationClientPage
  ],
  providers: [
    CallNumber,
    AndroidPermissions,
    SliderService
  ]
})
export class InformationClientPageModule {}