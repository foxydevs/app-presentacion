import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendMessagesUserClientPage } from './messages-user-send-client';
import { PipeModule } from '../../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SendMessagesUserClientPage,
  ],
  imports: [
    IonicPageModule.forChild(SendMessagesUserClientPage),
    PipeModule
  ],
  exports: [
    SendMessagesUserClientPage
  ]
})
export class SendMessagesUserClientPageModule {}