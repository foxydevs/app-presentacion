import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, Platform, IonicPage } from 'ionic-angular';
import { MessagesService } from '../../../app/service/messages.service';
import { UsersService } from '../../../app/service/users.service';
import { path } from '../../../app/config.module';
import { MembershipService } from '../../../app/service/membership.service';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'messages-client',
  templateUrl: 'messages-client.html'
})
export class MessagesClientPage {
  //PROPIEDADES
  public idClient:any;
  idUser:any = path.id;
  public messages:any[] = [];
  public messagesTemp2:any[] = [];
  public users:any[] = [];
  public staff:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');
  selectItem:any = 'mensajes';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public messagesService: MessagesService,
    public usersService: UsersService,
    public secondService: MembershipService,
    public plt: Platform,
    public iab: InAppBrowser
  ) {
    this.idClient = localStorage.getItem("currentId");
    this.loadAllUsers();
    this.secondService.calculateMembership();
    //MEMBRESIA
    if(this.plt.is('android')) {
      if(+this.membresiaClient < +this.nivelMembresia) {
        this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
      }
    }
  }

  //OBTENER MENSAJES
  public getMessages(id:any) {
    let messagesTemp:any[] = [];
    this.messagesService.getUserReceipt(id)
    .then(response => {
      console.log(response)
      for(let x of response) {
        //console.log(x.user_send)
        let user = {
          user: this.returnNameUser(x.user_send),
          picture: this.returnPicture(x.user_send),
          id: x.user_send,
          id_message: x.id
        }
        messagesTemp.push(user);
      }
      this.messagesTemp2 = this.removeDuplicates(messagesTemp, 'id');
      console.clear;
    }).catch(error => {
      console.clear;
    })
    this.messagesService.getUserSend(id)
    .then(response => {
      console.log(response)
      for(let x of response) {
        //console.log(x.user_receipt)
        let user = {
          user: this.returnNameUser(x.user_receipt),
          picture: this.returnPicture(x.user_receipt),
          id: x.user_receipt,
          id_message: x.id
        }
        messagesTemp.push(user);
      }
      this.messagesTemp2 = this.removeDuplicates(messagesTemp, 'id');    
      console.clear;
    }).catch(error => {
      console.clear;
    })
  }

  public sendMessage(parameter?:any) {
    this.navCtrl.push('SendMessagesClientPage', { parameter });
  }

  //RETORNAR EL NOMBRE DE USUARIO
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id == idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //CARGAR USUARIOS
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear;
    })
  }

  //RETORNAR IMAGEN DE PERFIL
  public returnPicture(idUser:any):any {
    for(var i = 0; i < this.users.length; i++) {
      if(this.users[i].id == idUser) {
        return this.users[i].picture;
      }
    }
  }

  public messagesClientUser(parameter:any) {
    this.navCtrl.push('MessagesUserClientPage', { parameter });
  }

  //REMOVER DUPLICADOS
  public removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject  = {};

    for(var i in originalArray) {
       lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for(i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
     return newArray;
  }

  //ORDENAR MENSAJES
  public getMessagesOrdered() {
    setTimeout(() => {
      this.getMessages(this.idClient);      
    }, 1000);
    setTimeout(() => {
      this.compare();
    }, 1500);
    setTimeout(() => {
      console.log(this.messagesTemp2)
      this.messages = [];
      this.messages = this.messagesTemp2;
      console.log(this.messages)

    }, 4000);
  }

  public compare() {
    this.messagesTemp2.sort(function(a, b) {
      return a.id_message-b.id_message; /* Modificar si se desea otra propiedad */
    });
    this.messagesTemp2.reverse();
    localStorage.setItem('currentMessages', JSON.stringify(this.messagesTemp2));
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.messages = JSON.parse(localStorage.getItem('currentMessages'));
    this.getMessagesOrdered();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getMessagesOrdered();
      refresher.complete();
    }, 2000);
  }

  //CARGAR PAGINA
  openBrowser() {
    if(localStorage.getItem('currentWhatsapp')) {
      this.iab.create(localStorage.getItem('currentWhatsapp'), '_blank', 'presentationstyle=pagesheet,toolbarcolor=#E67E22');
    }
  }

}
