import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { ProductsService } from '../../../app/service/products.service';
import { OrdersService } from '../../../app/service/orders.service';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { path } from '../../../app/config.module';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { DescuentoService } from '../../../app/service/descuentos.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'orders-client',
  templateUrl: 'orders-client.html'
})
export class OrdersClientPage implements OnInit {
  //Propiedades
  public order = {
    comment: '',
    quantity: 1,
    unit_price: 0,
    unit_price2: '',
    user: '',
    cliente: '',
    product: '',
    membresia: 0,
    tiempo: 0,
    fechaInicio: '',
    fechaFin: '',
    periodo: 0,
    nivel: 0,
    applicacion: path.id,
    id: '',
    picture: localStorage.getItem('currentPicture11'),
    paymentType: '1',
    image: '',
    deposito: '',
    libro: ''
  }
  public data = {
    id: 0,
    cantidad: 0,
    codigo: '',
    cliente: localStorage.getItem('currentId'),
    usado: 0
  }
  public product:any;
  public quantities:any[] = [];
  public url:any;
  public idUser:string = path.id;
  public idClient = localStorage.getItem('currentId');
  public btnDisabled:boolean = false;
  public btnDisabled2:boolean = false;
  public basePath:string = path.path;
  public parameter = {
    ern: ''
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  moneda = localStorage.getItem('currentCurrency');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    public productsService: ProductsService,
    public iab: InAppBrowser,
    public alertCtrl: AlertController,
    public mainService: OrdersService,
    public secondService: DescuentoService,
    public modal: ModalController
  ) {    
    this.order.product = this.navParams.get('parameter');
    this.order.user = localStorage.getItem("currentId");
    this.getQuantity();
    this.getSingle(this.order.product);
  }

  ngOnInit() {
    $('#imgAvatar2').hide();
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  public getSingle(idProduct:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.productsService.getSingle(idProduct)
    .then(response => {
      this.order.unit_price = response.price;
      this.order.unit_price2 = this.moneda + + parseFloat(response.price).toFixed(2);
      this.product = response.name;
      this.order.id = response.id;
      this.order.picture = response.picture;
      this.order.image = response.picture;
      this.order.membresia = response.membresia;
      this.order.tiempo = response.tiempo;
      this.order.periodo = response.periodo;
      this.order.nivel = response.nivel;
      this.order.libro = response.libro;
      if(response.quantity == 0) {
        this.confirmation('Producto Agotado', 'Este producto esta agotado, comunícate con el staff para solicitar una preventa.');
        this.btnDisabled = true;
      }
      if(this.order.membresia == 0) {
        this.order.paymentType = '1';
      } else {
        this.order.paymentType = '2';
      }
      load.dismiss();
    }).catch(error => {
      load.dismiss();
      console.clear
    })
  }

  //GET SINGLE
  search() {
      let load = this.loading.create({
        content: 'Cargando...'
      });
      load.present();
      this.secondService.search(path.id, this.data.codigo, 'codigo')
      .then(response => {
        console.log(response)
        if(response) {
          if(response[0].usado == '1') {
            this.confirmation('Código Usado', 'El código ya ha sido utilizado');
          } else {
            this.btnDisabled2 = true;
            this.data = response[0];
            this.order.unit_price =  this.order.unit_price - (+this.order.unit_price * (this.data.cantidad/100));;
            this.order.unit_price2 = this.moneda + + parseFloat(this.order.unit_price.toString()).toFixed(2);
            this.confirmation('Código Encontrado', 'El código de descuento ha sido aplicado exitosamente su descuento es de '+ response[0].cantidad.toString() + '%');
          }
          load.dismiss();
        }        
      }).catch(error => {
        load.dismiss();
      })
  }

  //CARGAR NUMEROS
  public getQuantity() {
    for(let x = 1; x <= 1000; x++) {
      this.quantities.push(x);
    } 
  }

  //GUARDAR CAMBIOS
  public saveChanges() {
    //this.addDays(this.order.deposito)
    this.order.picture = $('img[alt="Avatar"]').attr('src');
    if(this.order.picture == 'https://bpresentacion.s3.us-west-2.amazonaws.com/products/M7Xjg9Ih4fCEhK5SQ2y2r3Go70pv9YJ2jgx7CcQt.png'
    || this.order.picture == this.order.image) {
      this.order.picture = '';
      this.create();
    } else {
      this.create();
    }
  }

  addDays(dias:any) {
    var fecha = new Date(),
    dia = fecha.getDate(),
    mes = fecha.getMonth() + 1,
    anio = fecha.getFullYear(),
    addTime = dias * 86400; //Tiempo en segundos
    fecha.setSeconds(addTime); //Añado el tiempo
    this.order.fechaInicio = anio + "-" + mes + "-" + dia;
    this.order.fechaFin = fecha.getFullYear().toString() + "-" + (fecha.getMonth() + 1).toString() + "-" + fecha.getDate().toString();

    console.log("FECHA ACTUAL: " + this.order.fechaInicio)
    console.log("FECHA FINAL: " + this.order.fechaFin)
  }

  convert(event: any) {
    console.log('old:', this.product.unit_price);
    this.product.unit_price = event.target.value.replace(/[^\d\.]/g ,'');
    console.log('new:', this.product.unit_price);
  }

  //UPDATE
  public updateSecond() {
    this.data.usado = 1;
    this.data.cliente = localStorage.getItem('currentId');
    if(this.data.id != 0) {
      this.secondService.update(this.data)
      .then((res) => {
        console.log(res)
  
      }).catch((error) => {
        console.log(error)
      })
    }    
  }

  //PAGAR CON TARJETA DE CREDITO
  public payMembresia(fechaInicio:any, fechaFin:any) {
    this.btnDisabled = true;
    let ern = this.generate(4);
    let orden = {
      cantidad: this.order.quantity,
      descripcion: this.product,
      precio: this.order.unit_price,
      id: this.order.id,
      url: "http://me.gtechnology.gt",
      applicacion: this.idUser,
      ern: ern
    }
    let data = {
      comment: this.order.comment,
      finMembresia: fechaFin,
      inicioMembresia: fechaInicio,
      tipoNivel: this.order.nivel,
      nivelMembresia: 0,
      membresia: this.order.product,
      unit_price: this.order.unit_price,
      quantity: this.order.quantity,
      product: this.order.product,
      picture: this.order.picture,
      libro: this.order.libro,
      user: this.idUser,
      state: 3,
      client: this.idClient,
      ern: ern
    }
    this.parameter.ern = orden.ern;
    this.mainService.pagar(orden)
    .then(response => {
      console.log(response)
      this.loading.create({
        content: "Realizando Pedido...",
        duration: 2000
      }).present();
      this.mainService.create(data)
      .then(response2 => {
        let url = response.token;
        let parameter = {
          ern: this.parameter.ern,
          tokenID: ''
        }
        //CARGAR ULTIMO LINK
        const browser = this.iab.create(url, '_blank', 'location=yes,clearsessioncache=yes,clearcache=yes');
        browser.on('loadstart').subscribe((e) => {
          this.url = e.url;
          //URL TOKEN NUEVO
          let tokenUrlComparation = e.url;
          //URL TOKEN A COMPARAR
          let token = this.url.replace('http://toktok.foxylabs.xyz/home/comprobante/','');
          let ernReplace = '/' + ern;
          let tokenFinally = token.replace(ernReplace, '')
          parameter.tokenID = tokenFinally;
          let urlFinally = 'http://toktok.foxylabs.xyz/home/comprobante/' + tokenFinally + ernReplace
          if(tokenUrlComparation == urlFinally) {
            if(tokenFinally.length >= 30) {
              this.navCtrl.setRoot('MyOrdersClientPage')
              browser.close()
              this.loading.create({
                content: 'Procesando Compra...',
                duration: 3000
              }).present();
              //DESCUENTO
              this.updateSecond();
              let chooseModal = this.modal.create('DetailOrdersClientPage', { parameter });
              chooseModal.present();
            } else {
              this.loading.create({
                content: 'Saliendo de la Compra...',
                duration: 2000
              }).present();
              this.navCtrl.setRoot('CategorysClientPage')
            }  
          }
        });
        //CARGAR SALIDA
        browser.on('exit').subscribe((data) =>  {
          //this.update(orderNoPay);
          this.navCtrl.setRoot('MyOrdersClientPage')
        });
      }).catch(error => {
        this.btnDisabled = false;
      })
    }).catch(error => {
      this.btnDisabled = false;
    })
  }

  //PAGAR CON TARJETA DE CREDITO
  public payCard() {
    this.btnDisabled = true;
    let ern = this.generate(4);
    let orden = {
      cantidad: this.order.quantity,
      descripcion: this.product,
      precio: this.order.unit_price,
      id: this.order.id,
      url: "http://me.gtechnology.gt",
      ern: ern,
      applicacion: this.idUser
    }
    let data = {
      comment: this.order.comment,
      unit_price: this.order.unit_price,
      quantity: this.order.quantity,
      product: this.order.product,
      picture: this.order.picture,
      libro: this.order.libro,
      state: 3,
      user: this.idUser,
      client: this.idClient,
      ern: ern
    }
    this.parameter.ern = orden.ern;
    this.mainService.pagar(orden)
    .then(response => {
      console.log(response)
      this.loading.create({
        content: "Realizando Pedido...",
        duration: 2000
      }).present();
      this.mainService.create(data)
      .then(response2 => {
        let url = response.token;
        let parameter = {
          ern: this.parameter.ern,
          tokenID: ''
        }
        //CARGAR ULTIMO LINK
        const browser = this.iab.create(url, '_blank', 'location=yes,clearsessioncache=yes,clearcache=yes');
        browser.on('loadstart').subscribe((e) => {
          this.url = e.url;
          //URL TOKEN NUEVO
          let tokenUrlComparation = e.url;
          //URL TOKEN A COMPARAR
          let token = this.url.replace('http://toktok.foxylabs.xyz/home/comprobante/','');
          let ernReplace = '/' + ern;
          let tokenFinally = token.replace(ernReplace, '')
          parameter.tokenID = tokenFinally;
          let urlFinally = 'http://toktok.foxylabs.xyz/home/comprobante/' + tokenFinally + ernReplace
          if(tokenUrlComparation == urlFinally) {
            if(tokenFinally.length >= 30) {
              this.navCtrl.setRoot('MyOrdersClientPage')
              browser.close()
              this.loading.create({
                content: 'Procesando Compra...',
                duration: 3000
              }).present();
              //DESCUENTO
              this.updateSecond();
              let chooseModal = this.modal.create('DetailOrdersClientPage', { parameter });
              chooseModal.present();
            } else {
              this.loading.create({
                content: 'Saliendo de la Compra...',
                duration: 2000
              }).present();
              this.navCtrl.setRoot('CategorysClientPage')
            }  
          }
        });
        //CARGAR SALIDA
        browser.on('exit').subscribe((data) =>  {
          //this.update(orderNoPay);
          this.navCtrl.setRoot('MyOrdersClientPage')
        });
      }).catch(error => {
        this.btnDisabled = false;
      })
    }).catch(error => {
      this.btnDisabled = false;
    })
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.navCtrl.setRoot('MyOrdersClientPage')
    }).catch(error => {
      console.clear
    });
  }

  //PAGAR EFECTIVO O DEPOSITO
  public payEfectivoDeposito(estado:any, deposito:any) {
    this.btnDisabled = true;
    let ern = this.generate(4);
    let data = {
      comment: this.order.comment,
      unit_price: this.order.unit_price,
      quantity: this.order.quantity,
      product: this.order.product,
      picture: this.order.picture,
      libro: this.order.libro,
      user: this.idUser,
      applicacion: this.idUser,
      state: estado,
      client: this.idClient,
      deposito: deposito,
      ern: ern
    }
    this.mainService.create(data)
    .then(response => {
      console.log(response)
      this.loading.create({
        content: "Realizando Pedido...",
        duration: 2000
      }).present();
      //DESCUENTO
      this.updateSecond();
      this.navCtrl.setRoot('MyOrdersClientPage');
    }).catch(error => {
      console.log(error)
    })
  }

  //INSERTAR ORDENES
  public create(){
    if(this.order.quantity >= 1) {
      if(this.order.paymentType == '2') {
        if(this.order.membresia == 1) {
          if(this.order.periodo == 1) {
            //SUMAR DIAS
            this.addDays(this.order.tiempo);
            this.payMembresia(this.order.fechaInicio, this.order.fechaFin);
          } else if (this.order.periodo == 2) {
            //SUMAR MESES
            let dias = this.order.tiempo * 30;
            this.addDays(dias);
            this.payMembresia(this.order.fechaInicio, this.order.fechaFin);
          } else if (this.order.periodo == 3) {
            //SUMAR AÑOS
            let dias = this.order.tiempo * 365;
            this.addDays(dias);
            this.payMembresia(this.order.fechaInicio, this.order.fechaFin);
          }
        } else {
          this.payCard();
        }
      } if(this.order.paymentType == '1') {
        this.payEfectivoDeposito(4, null);        
      } if(this.order.paymentType == '3') {
        this.payEfectivoDeposito(5, this.order.deposito);
      }
    } else {
      this.toast.create({
        message: 'La cantidad es requerida.',
        duration: 1000
      }).present();
    }
  }

  public generate(longitude)
  {
    let i:number
    var caracteres = "123456789+-*abcdefghijkmnpqrtuvwxyz123456789+-*ABCDEFGHIJKLMNPQRTUVWXYZ12346789+-*";
    var password = "";
    for (i=0; i<longitude; i++) password += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
    return '02' + password;
  }

  //IMAGEN DE ORDEN
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif');
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'orders'
          },
          function(respuesta) {
            if(type == 'application/pdf') {
              $('#imgAvatar').attr("src", respuesta.url);
              $('#imgAvatar2').show();
              $('#imgAvatar').hide();              
              $('#imgAvatar2').attr("src", 'https://developers.zamzar.com/assets/app/img/convert/pdf.png');
              $("#"+id).val('');
            } else {
              $('#imgAvatar').attr("src", respuesta.url);
              $("#"+id).val('');
            }
          }
        );
      } else {
        this.message('El archivo es demasiado grande.')
      }
    } else {
      this.message('El tipo de archivo no es válido.')
    }
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

}
