import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { MyOrderQPPClientPage } from './my-order-qpp';
import { AddressService } from '../../../../app/service/address.service';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
 
@NgModule({
  declarations: [
    MyOrderQPPClientPage,
  ],
  imports: [
    IonicPageModule.forChild(MyOrderQPPClientPage),
    PipeModule
  ],
  exports: [
    MyOrderQPPClientPage
  ], 
  providers: [
    AddressService,
    NativeGeocoder
  ]
})
export class MyOrderQPPClientPageModule {}