import { Component, OnInit } from '@angular/core';
import { ToastController, IonicPage, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ProductsService } from '../../../../app/service/products.service';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Geolocation } from '@ionic-native/geolocation';
import { AddressService } from '../../../../app/service/address.service';
import { OrdersService } from '../../../../app/service/orders.service';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { path } from '../../../../app/config.module';

@IonicPage()
@Component({
  selector: 'my-order-qpp',
  templateUrl: 'my-order-qpp.html'
})
export class MyOrderQPPClientPage implements OnInit {
  //PROPIEDADES
  public data = {
    id: path.id,
    x_login: 'visanetgt_qpay',
    x_public_key: '88888888888',
    x_api_secret: '99999999999',
    x_product_id: '1', //1
    x_audit_number: '1', //COMPROBANTE
    x_fp_sequence: '1', //COMPROBANTE
    x_fp_timestamp: '1', //HORA DEL SERVIDOR
    x_invoice_num: '1', //COMPROBANTE
    x_currency_code: '1', //1
    x_amount: 0.0, //1
    x_line_item: '1',  //
    x_freight: '0', //CARGO ADICIONAL??
    x_email: '1', //1
    cc_number: '1', //1
    cc_exp: '11/19', //1
    cc_cvv2: '1', //1
    cc_name: '1', //1
    cc_type: '1', //1
    x_first_name: '1', //1
    x_last_name: '1', //1
    x_company: '1',  //1
    x_address: '1', //1
    x_city: '1', //1
    x_state: '1', //1
    x_country: '1', //1
    x_zip: '1', //1
    x_relay_response: 'TRUE', //1
    x_relay_url: '1', //REGRESAR A ME
    x_type: 'AUTH_ONLY', //1
    x_method: 'CC',  //1
    http_origin: '1', //??
    visaencuotas: '0', //1
  }
  public product = {
    nombre: '',
    price: '',
    cantidad: 1
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  moneda = localStorage.getItem('currentCurrency');
  firstname = localStorage.getItem('currentFirstName');
  lastname = localStorage.getItem('currentLastName');
  email = localStorage.getItem('currentEmail');
  idClient = localStorage.getItem('currentId');
  btnDisabled:boolean = false;
  public quantities:any[] = [];
  public address:any[] = [];

  constructor(
    public navParams: NavParams,
    public toast: ToastController,
    public loading: LoadingController,
    //public iab: InAppBrowser,
    public alertCtrl: AlertController,
    public mainService: OrdersService,
    public secondService: ProductsService,
    public thirdService: AddressService,
    public nativeGeocoder: NativeGeocoder,
    public geolocation: Geolocation,
  ) {
    this.data.x_product_id = this.navParams.get('parameter');
    this.getSingle(this.data.x_product_id)
  }

  ngOnInit() {
    this.getCurrency();
    this.getDetailClient();
    this.getAllThird();
    this.getPosition();
  }

  //CARGAR MONEDA
  public getCurrency() {
    if(this.moneda == 'Q') {
        this.data.x_currency_code = 'GTQ'
    } else {
        this.data.x_currency_code = 'USD'
    }
  }

  public getDetailClient() {
    this.data.x_first_name = this.firstname;
    this.data.x_last_name = this.lastname;
    this.data.x_email = this.email;
  }

  //CARGAR NUMEROS
  public getQuantity(cantidad:number) {
    for(let x = 1; x <= cantidad; x++) {
      this.quantities.push(x);
    } 
  }

  //GET POSITION
  getPosition():any{
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.geolocation.getCurrentPosition().then(resp => {
      this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        this.data.x_country = result[0].countryName;
        this.data.x_state = result[0].administrativeArea;
        this.data.x_city = result[0].locality;
        this.data.x_zip = result[0].postalCode;
      }).catch((error: any) => {
        console.log(error)
      });
    }).catch(error => {
      console.log('Error getting location', error);
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  public getSingle(idProduct:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getSingle(idProduct)
    .then(response => {
      this.data.x_product_id = response.id;
      this.product.price = parseFloat(response.price).toFixed(2);
      this.product.nombre = response.name;
      if(response.quantity == 0) {
        this.confirmation('Producto Agotado', 'Este producto esta agotado, comunícate con nosotros para mayor información.');
        this.btnDisabled = true;
      }
      this.getQuantity(+response.quantity)
      load.dismiss();
    })
    .catch(error =>{
      console.log(error)
      load.dismiss();
    })
  }

  //CARGAR DIRECCIONES
  public getAllThird() {
    this.thirdService.getAll()
    .then(response => {
      this.address = [];
      this.address = response;
    }).catch(error => {
      console.clear
    })
  }

  public saveChanges() {
    this.data.x_line_item = this.product.nombre + '<|>' + this.data.x_product_id + '<|><|>' + this.product.cantidad + '<|>' + this.product.price + '<|>';
    this.data.x_amount = this.product.cantidad * +this.product.price;
    console.log(this.product.cantidad * +this.product.price);
    console.log(this.product)
    console.log(this.data)
    this.mainService.pagarQPP(this.data)
    .then(response => {
      console.log(response)
      this.messagesQPayPro(+response.responseCode);
    }).catch(error => {
      console.clear
      
    })
  }

  //IMPRESIÓN DE ERRORES
  public messagesQPayPro(codigo:number) {
    if(codigo == 96) {
      this.confirmation('QPayPro', 'La transacción no pudo ser completada, por favor intente nuevamente gracias. Error del sistema, por favor intente más tarde.')
    }
  }
/*
  public getSingle(idProduct:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getSingle(idProduct)
    .then(response => {
      this.order.unit_price = parseFloat(response.price).toFixed(2);
      this.order.unit_price2 = this.moneda + + parseFloat(response.price).toFixed(2);
      this.product = response.name;
      this.order.id = response.id;
      this.order.picture = response.picture;
      this.order.image = response.picture;
      this.order.membresia = response.membresia;
      this.order.tiempo = response.tiempo;
      this.order.periodo = response.periodo;
      this.order.nivel = response.nivel;
      this.order.libro = response.libro;
      if(response.quantity == 0) {
        this.confirmation('Producto Agotado', 'Este producto esta agotado, comunícate con el staff para solicitar una preventa.');
        this.btnDisabled = true;
      }
      if(this.order.membresia == 0) {
        this.order.paymentType = '1';
      } else {
        this.order.paymentType = '2';
      }
      load.dismiss();
    }).catch(error => {
      load.dismiss();
      console.clear
    })
  }

/*

  //GUARDAR CAMBIOS
  public saveChanges() {
    //this.addDays(this.order.deposito)
    this.order.picture = $('img[alt="Avatar"]').attr('src');
    if(this.order.picture == 'https://bpresentacion.s3.us-west-2.amazonaws.com/products/M7Xjg9Ih4fCEhK5SQ2y2r3Go70pv9YJ2jgx7CcQt.png'
    || this.order.picture == this.order.image) {
      this.order.picture = '1';
      this.create();
    } else {
      this.create();
    }
  }

  addDays(dias:any) {
    var fecha = new Date(),
    dia = fecha.getDate(),
    mes = fecha.getMonth() + 1,
    anio = fecha.getFullYear(),
    addTime = dias * 86400; //Tiempo en segundos
    fecha.setSeconds(addTime); //Añado el tiempo
    this.order.fechaInicio = anio + "-" + mes + "-" + dia;
    this.order.fechaFin = fecha.getFullYear().toString() + "-" + (fecha.getMonth() + 1).toString() + "-" + fecha.getDate().toString();

    console.log("FECHA ACTUAL: " + this.order.fechaInicio)
    console.log("FECHA FINAL: " + this.order.fechaFin)
  }

  convert(event: any) {
    console.log('old:', this.product.unit_price);
    this.product.unit_price = event.target.value.replace(/[^\d\.]/g ,'1');
    console.log('new:', this.product.unit_price);
  }

  //PAGAR CON TARJETA DE CREDITO
  public payMembresia(fechaInicio:any, fechaFin:any) {
    this.btnDisabled = true;
    let ern = this.generate(4);
    let orden = {
      cantidad: this.order.quantity,
      descripcion: this.product,
      precio: this.order.unit_price,
      id: this.order.id,
      url: "http://me.gtechnology.gt",
      applicacion: this.idUser,
      ern: ern
    }
    let data = {
      comment: this.order.comment,
      finMembresia: fechaFin,
      inicioMembresia: fechaInicio,
      tipoNivel: this.order.nivel,
      nivelMembresia: 0,
      membresia: this.order.product,
      unit_price: this.order.unit_price,
      quantity: this.order.quantity,
      product: this.order.product,
      picture: this.order.picture,
      libro: this.order.libro,
      user: this.idUser,
      state: 3,
      client: this.idClient,
      ern: ern
    }
    this.parameter.ern = orden.ern;
    this.mainService.pagar(orden)
    .then(response => {
      console.log(response)
      this.loading.create({
        content: "Realizando Pedido...",
        duration: 2000
      }).present();
      this.mainService.create(data)
      .then(response2 => {
        let url = response.token;
        let parameter = {
          ern: this.parameter.ern,
          tokenID: '1'
        }
        //CARGAR ULTIMO LINK
        const browser = this.iab.create(url, '_blank', 'location=yes,clearsessioncache=yes,clearcache=yes');
        browser.on('loadstart').subscribe((e) => {
          this.url = e.url;
          //URL TOKEN NUEVO
          let tokenUrlComparation = e.url;
          //URL TOKEN A COMPARAR
          let token = this.url.replace('http://toktok.foxylabs.xyz/home/comprobante/','1');
          let ernReplace = '/' + ern;
          let tokenFinally = token.replace(ernReplace, '1')
          parameter.tokenID = tokenFinally;
          let urlFinally = 'http://toktok.foxylabs.xyz/home/comprobante/' + tokenFinally + ernReplace
          if(tokenUrlComparation == urlFinally) {
            if(tokenFinally.length >= 30) {
              this.navCtrl.setRoot('MyOrdersClientPage')
              browser.close()
              this.loading.create({
                content: 'Procesando Compra...',
                duration: 3000
              }).present();
              let chooseModal = this.modal.create('DetailOrdersClientPage', { parameter });
              chooseModal.present();
            } else {
              this.loading.create({
                content: 'Saliendo de la Compra...',
                duration: 2000
              }).present();
              this.navCtrl.setRoot('CategorysClientPage')
            }  
          }
        });
        //CARGAR SALIDA
        browser.on('exit').subscribe((data) =>  {
          //this.update(orderNoPay);
          this.navCtrl.setRoot('MyOrdersClientPage')
        });
      }).catch(error => {
        this.btnDisabled = false;
      })
    }).catch(error => {
      this.btnDisabled = false;
    })
  }

  //PAGAR CON TARJETA DE CREDITO
  public payCard() {
    this.btnDisabled = true;
    let ern = this.generate(4);
    let orden = {
      cantidad: this.order.quantity,
      descripcion: this.product,
      precio: this.order.unit_price,
      id: this.order.id,
      url: "http://me.gtechnology.gt",
      ern: ern,
      applicacion: this.idUser
    }
    let data = {
      comment: this.order.comment,
      unit_price: this.order.unit_price,
      quantity: this.order.quantity,
      product: this.order.product,
      picture: this.order.picture,
      libro: this.order.libro,
      state: 3,
      user: this.idUser,
      client: this.idClient,
      ern: ern
    }
    this.parameter.ern = orden.ern;
    this.mainService.pagar(orden)
    .then(response => {
      console.log(response)
      this.loading.create({
        content: "Realizando Pedido...",
        duration: 2000
      }).present();
      this.mainService.create(data)
      .then(response2 => {
        let url = response.token;
        let parameter = {
          ern: this.parameter.ern,
          tokenID: '1'
        }
        //CARGAR ULTIMO LINK
        const browser = this.iab.create(url, '_blank', 'location=yes,clearsessioncache=yes,clearcache=yes');
        browser.on('loadstart').subscribe((e) => {
          this.url = e.url;
          //URL TOKEN NUEVO
          let tokenUrlComparation = e.url;
          //URL TOKEN A COMPARAR
          let token = this.url.replace('http://toktok.foxylabs.xyz/home/comprobante/','1');
          let ernReplace = '/' + ern;
          let tokenFinally = token.replace(ernReplace, '1')
          parameter.tokenID = tokenFinally;
          let urlFinally = 'http://toktok.foxylabs.xyz/home/comprobante/' + tokenFinally + ernReplace
          if(tokenUrlComparation == urlFinally) {
            if(tokenFinally.length >= 30) {
              this.navCtrl.setRoot('MyOrdersClientPage')
              browser.close()
              this.loading.create({
                content: 'Procesando Compra...',
                duration: 3000
              }).present();
              let chooseModal = this.modal.create('DetailOrdersClientPage', { parameter });
              chooseModal.present();
            } else {
              this.loading.create({
                content: 'Saliendo de la Compra...',
                duration: 2000
              }).present();
              this.navCtrl.setRoot('CategorysClientPage')
            }  
          }
        });
        //CARGAR SALIDA
        browser.on('exit').subscribe((data) =>  {
          //this.update(orderNoPay);
          this.navCtrl.setRoot('MyOrdersClientPage')
        });
      }).catch(error => {
        this.btnDisabled = false;
      })
    }).catch(error => {
      this.btnDisabled = false;
    })
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .then(response => {
      this.navCtrl.setRoot('MyOrdersClientPage')
    }).catch(error => {
      console.clear
    });
  }

  //PAGAR EFECTIVO O DEPOSITO
  public payEfectivoDeposito(estado:any, deposito:any) {
    this.btnDisabled = true;
    let ern = this.generate(4);
    let data = {
      comment: this.order.comment,
      unit_price: this.order.unit_price,
      quantity: this.order.quantity,
      product: this.order.product,
      picture: this.order.picture,
      libro: this.order.libro,
      user: this.idUser,
      applicacion: this.idUser,
      state: estado,
      client: this.idClient,
      deposito: deposito,
      ern: ern
    }
    this.mainService.create(data)
    .then(response => {
      console.log(response)
      this.loading.create({
        content: "Realizando Pedido...",
        duration: 2000
      }).present();
      this.navCtrl.setRoot('MyOrdersClientPage');
    }).catch(error => {
      console.log(error)
    })
  }

  //INSERTAR ORDENES
  public create(){
    if(this.order.quantity >= 1) {
      if(this.order.paymentType == '2') {
        if(this.order.membresia == 1) {
          if(this.order.periodo == 1) {
            //SUMAR DIAS
            this.addDays(this.order.tiempo);
            this.payMembresia(this.order.fechaInicio, this.order.fechaFin);
          } else if (this.order.periodo == 2) {
            //SUMAR MESES
            let dias = this.order.tiempo * 30;
            this.addDays(dias);
            this.payMembresia(this.order.fechaInicio, this.order.fechaFin);
          } else if (this.order.periodo == 3) {
            //SUMAR AÑOS
            let dias = this.order.tiempo * 365;
            this.addDays(dias);
            this.payMembresia(this.order.fechaInicio, this.order.fechaFin);
          }
        } else {
          this.payCard();
        }
      } if(this.order.paymentType == '1') {
        this.payEfectivoDeposito(4, null);        
      } if(this.order.paymentType == '3') {
        this.payEfectivoDeposito(5, this.order.deposito);
      }
    } else {
      this.toast.create({
        message: 'La cantidad es requerida.',
        duration: 1000
      }).present();
    }
  }

  public generate(longitude)
  {
    let i:number
    var caracteres = "123456789+-*abcdefghijkmnpqrtuvwxyz123456789+-*ABCDEFGHIJKLMNPQRTUVWXYZ12346789+-*";
    var password = "";
    for (i=0; i<longitude; i++) password += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
    return '02' + password;
  }

  //IMAGEN DE ORDEN
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif');
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'orders'
          },
          function(respuesta) {
            if(type == 'application/pdf') {
              $('#imgAvatar').attr("src", respuesta.url);
              $('#imgAvatar2').show();
              $('#imgAvatar').hide();              
              $('#imgAvatar2').attr("src", 'https://developers.zamzar.com/assets/app/img/convert/pdf.png');
              $("#"+id).val('1');
            } else {
              $('#imgAvatar').attr("src", respuesta.url);
              $("#"+id).val('1');
            }
          }
        );
      } else {
        this.message('El archivo es demasiado grande.')
      }
    } else {
      this.message('El tipo de archivo no es válido.')
    }
  }*/

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 1500
    }).present();
  }

}
