import { Component } from '@angular/core';
import { NavController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ChallengeService } from '../../../../app/service/challenge.service';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { UsersService } from '../../../../app/service/users.service';
import * as $ from 'jquery'

@IonicPage()
@Component({
  selector: 'detail-challenges-client',
  templateUrl: 'detail-challenges-client.html',
})
export class DetailChallengeUserPage {
  //PROPIEDADES
  public parameter:any;
  selectItem:any = 'detalle';
  authentication:any = localStorage.getItem('currentAuthentication');
  idClient:any = localStorage.getItem('currentId')
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  members:any[] = [];
  public data = {
    title: '',
    description: '',
    user: path.id,
    picture: 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatar/8Bjpl7wuGY88gjCBWR3nsBELyEgqEbXwPkyvWiCz.png',
    id: ''
  }
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public loading: LoadingController,
    public mainService: ChallengeService,
    public secondService: UsersService,
  ) {
    this.parameter = this.navParams.get('parameter');
    $(document).ready(function(){
      $(".slide").click(function(){
        
        var target = $(this).parent().children(".slideContent");
        $(target).slideToggle();
      });
    });
  }

  //ABRIR COMENTARIOS
  openForm() {
    if(this.authentication == 'NoAuthentication') {
      this.message('Usted debe iniciar sesión.')
    } else {
      let parameter = this.data.id;
      this.navCtrl.push('CommentsChallengeClientPage', { parameter });
    }    
  }

  //ABRIR COMENTARIOS
  openFormComment(id:any) {
    if(this.authentication == 'NoAuthentication') {
      this.message('Usted debe iniciar sesión.')
    } else {
      let parameter = {
        id: this.data.id,
        comment: id
      }
      this.navCtrl.push('CommentsChallengeClientPage', { parameter });
    }    
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar tu comentario?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let load = this.loading.create({
              content: "Eliminando..."
            })
            load.present();
            this.mainService.deleteComment(id)
            .then(response => {
              load.dismiss();
              this.getSingle(this.parameter);
              console.clear();
            }).catch(error => {
              console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //GET SCORE
  getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getScore(+this.data.id)
    .then(response => {
      this.members = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.getSingle(this.parameter);
    }, 1000);
    setTimeout(() => {
    console.log("afterinit");
    var acc = document.getElementsByClassName("accordion");
    console.log(acc);
    var i;
    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            console.log('CLICK');          
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
    }, 2000);
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getSingle(this.parameter);
      refresher.complete();
    }, 2000);
  }

}
