import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailChallengeUserPage } from './detail-challenges-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailChallengeUserPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailChallengeUserPage),
    PipeModule
  ],
  exports: [
    DetailChallengeUserPage
  ]
})
export class DetailChallengeUserPageModule {}