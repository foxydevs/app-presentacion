import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, NavController, IonicPage } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UsersService } from '../../app/service/users.service';
import { Events } from 'ionic-angular/util/events';
import { AccessService } from '../../app/service/access.service';
import { path } from '../../app/config.module';

@IonicPage()
@Component({
  selector: 'page-client',
  templateUrl: 'client.html'
})
export class ClientPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  pages: Array<{icon:string, ios:string, title: string, apodo?: string, component: any, membresia: any}>;
  picture:any;
  email:any;
  firstName:any;
  baseUser:any;
  baseId:any;
  baseIdUser:number = path.id;
  pictureCover:any;
  opcion2:any;
  appMembresia:any;
  authentication = localStorage.getItem('currentAuthentication');

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public loading: LoadingController,
    public events: Events,
    public usersService: UsersService,
    public accessService: AccessService,
    public navCtrl: NavController) {
    //CARGAR USUARIO    
    this.getData();
    this.getPages();
    this.loadModules();
    if(localStorage.getItem('currentpictureCover')!='null'){
      this.pictureCover = localStorage.getItem('currentpictureCover');
    }
    events.subscribe('user:update', res => {
      this.loadModules();
      this.authentication = localStorage.getItem('currentAuthentication');
      this.appMembresia = localStorage.getItem('currentAppMembresia');
      this.picture = localStorage.getItem("currentPicture");
      this.email = localStorage.getItem("currentEmail");
      this.firstName = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    });
  }

  public loadSingleUser(id:any){
    this.usersService.getSingle(id)
    .then(response => {
      this.baseUser = response;
      this.picture = response.picture;
      localStorage.setItem("currentPicture",response.picture);
    }).catch(error => {
    })
  }

  public loadSingleUserApp(id:any){
    this.usersService.getSingle(id)
    .then(response => {
      if(response.opcion3 == true) {
        localStorage.setItem('currentAppMembresia', '1');
      } else {
        localStorage.setItem('currentAppMembresia', '0');
      }
      this.appMembresia = localStorage.getItem('currentAppMembresia');
    }).catch(error => {
    })
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    localStorage.setItem('currentNivelMembresia', page.membresia);
    this.nav.setRoot(page.component);
  }

  logOut() {
    localStorage.clear();
    this.loading.create({
      content: "Cerrando Sesión...",
      duration: 1000
    }).present();
    this.events.publish('user:update');
    localStorage.setItem('currentAuthentication', 'NoAuthentication');
    this.authentication = localStorage.getItem('currentAuthentication');
  }
  openAdoptions() {
    this.nav.setRoot('AdoptionClientPage');
  }
  openInformation() {
    this.nav.setRoot('InformationClientPage');
  }
  
  openWorkouts() {
    this.nav.setRoot('WorkoutsMonthUserPage');
  }

  openLogIn() {
    this.nav.setRoot('LoginPage');
  }

  public loadModules(){
    this.accessService.getAccess(this.baseIdUser)
    .then(response => {      
      if(response.permitidos.length > 0){
        this.pages = [];
        for(let x of response.permitidos) {
          if(x.modulos_show.id == '16' && x.mostrar == '1') {
            if(this.appMembresia == '1') {
              this.pages.push({icon: 'md-star', ios: 'ios-star', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MyMembershipClientPage', membresia: x.membresia})
            } 
          } else if(x.modulos_show.id == '1' && x.mostrar == '1') {
            if(this.baseIdUser == 82) {
              this.pages.push({icon: 'md-apps', ios: 'ios-apps', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CategorysDocumentsClientPage', membresia: x.membresia})            
            } else {
              this.pages.push({icon: 'md-apps', ios: 'ios-apps', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CategorysClientPage', membresia: x.membresia})
            }
          } else if(x.modulos_show.id == '2' && x.mostrar == '1') {
            if(this.baseIdUser == 82) {
              this.pages.push({icon: 'md-document', ios: 'ios-document', title: x.modulos_show.nombre, apodo: x.apodo, component: 'DocumentsClientPage', membresia: x.membresia})
            } else {
              this.pages.push({icon: 'md-albums', ios: 'ios-albums', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ProductsClientPage', membresia: x.membresia})              
            }
          } else if(x.modulos_show.id == '3' && x.mostrar == '1') {
            this.pages.push({icon: 'md-cart', ios: 'ios-cart', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MyOrdersClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '4' && x.mostrar == '1') {
            this.pages.push({icon: 'md-calendar', ios: 'ios-calendar', title: x.modulos_show.nombre, apodo: x.apodo, component: 'EventsClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '5' && x.mostrar == '1') {
            this.pages.push({icon: 'logo-youtube', ios: 'logo-youtube', title: x.modulos_show.nombre, apodo: x.apodo, component: 'YoutubeClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '6' && x.mostrar == '1') {
            this.pages.push({icon: 'md-globe', ios: 'ios-globe', title: x.modulos_show.nombre, apodo: x.apodo, component: 'NewsClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '7' && x.mostrar == '1') {
            if(this.baseIdUser == 49) {
              this.pages.push({icon: 'md-nutrition', ios: 'ios-nutrition', title: x.modulos_show.nombre, apodo: x.apodo, component: 'TipsClientPage', membresia: x.membresia})
            } else {
              this.pages.push({icon: 'md-alarm', ios: 'ios-alarm', title: x.modulos_show.nombre, apodo: x.apodo, component: 'TipsClientPage', membresia: x.membresia})
            }
          } else if(x.modulos_show.id == '8' && x.mostrar == '1') {
            this.pages.push({icon: 'md-send', ios: 'ios-send', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MessagesClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '9' && x.mostrar == '1') {
            this.pages.push({icon: 'md-person', ios: 'ios-person', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ProfileClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '10' && x.mostrar == '1') {
            if(this.baseIdUser == 88) {
              this.pages.push({icon: 'md-walk', ios: 'ios-walk', title: x.modulos_show.nombre, apodo: x.apodo, component: 'WorkoutsMonthClientPage', membresia: x.membresia})
            } else {
              this.pages.push({icon: 'md-walk', ios: 'ios-walk', title: x.modulos_show.nombre, apodo: x.apodo, component: 'WorkoutsClientPage', membresia: x.membresia})
            }
          } else if(x.modulos_show.id == '11' && x.mostrar == '1') {
            this.pages.push({icon: 'md-medal', ios: 'ios-medal', title: x.modulos_show.nombre, apodo: x.apodo, component: 'LeadearboardClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '12' && x.mostrar == '1') {
            this.pages.push({icon: 'logo-rss', ios: 'logo-rss', title: x.modulos_show.nombre, apodo: x.apodo, component: 'BlogClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '13' && x.mostrar == '1') {
            this.pages.push({icon: 'md-thumbs-up', ios: 'ios-thumbs-up', title: x.modulos_show.nombre, apodo: x.apodo, component: 'SocialClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '14' && x.mostrar == '1') {
            this.pages.push({icon: 'md-play', ios: 'ios-play', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ActivePauseClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '15' && x.mostrar == '1') {
            this.pages.push({icon: 'md-pricetag', ios: 'ios-pricetag', title: x.modulos_show.nombre, apodo: x.apodo, component: 'PromotionClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '17' && x.mostrar == '1') {
            this.pages.push({icon: 'md-pricetags', ios: 'ios-pricetags', title: x.modulos_show.nombre, apodo: x.apodo, component: 'BenefitsClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '18' && x.mostrar == '1') {
            this.pages.push({icon: 'md-body', ios: 'ios-body', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MyProgressClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '19' && x.mostrar == '1') {
            this.pages.push({icon: 'md-stopwatch', ios: 'ios-stopwatch', title: x.modulos_show.nombre, apodo: x.apodo, component: 'RunningClubClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '20' && x.mostrar == '1') {
            this.pages.push({icon: 'md-people', ios: 'ios-people', title: x.modulos_show.nombre, apodo: x.apodo, component: 'ZFitClubClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '21' && x.mostrar == '1') {
            this.pages.push({icon: 'md-planet', ios: 'ios-planet', title: x.modulos_show.nombre, apodo: x.apodo, component: 'StaffClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '22' && x.mostrar == '1') {
            this.pages.push({icon: 'md-leaf', ios: 'ios-leaf', title: x.modulos_show.nombre, apodo: x.apodo, component: 'MotivationClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '23' && x.mostrar == '1') {
            this.pages.push({icon: 'md-alarm', ios: 'ios-alarm', title: x.modulos_show.nombre, apodo: x.apodo, component: 'CitesClientPage', membresia: x.membresia})
          } else if(x.modulos_show.id == '24' && x.mostrar == '1') {
            this.pages.push({icon: 'md-information-circle', ios: 'ios-information-circle', title: x.modulos_show.nombre, apodo: x.apodo, component: 'InformationClientPage', membresia: x.membresia})
          }
        }
      }
      console.clear
    }).catch(error => {
      console.log(error);
    })
  }
  
  public getPages() {
    if(this.opcion2 == '1') {
      if(this.baseIdUser == 82) {
        this.rootPage = 'CategorysDocumentsClientPage';
      } else {
        this.rootPage = 'CategorysClientPage';
      }
    } else if(this.opcion2 == '2') {
      if(this.baseIdUser == 82) {
        this.rootPage = 'DocumentsClientPage';
      } else {
        this.rootPage = 'ProductsClientPage';
      }
    } else if(this.opcion2 == '3') {
      this.rootPage = 'MyOrdersClientPage';      
    } else if(this.opcion2 == '4') {
      this.rootPage = 'EventsClientPage';      
    } else if(this.opcion2 == '5') {
      this.rootPage = 'YoutubeClientPage';      
    } else if(this.opcion2 == '6') {
      this.rootPage = 'NewsClientPage';      
    } else if(this.opcion2 == '7') {
      this.rootPage = 'TipsClientPage';
    } else if(this.opcion2 == '8') {
      this.rootPage = 'MessagesClientPage';
    } else if(this.opcion2 == '9') {
      this.rootPage = 'ProfileClientPage';
    } else if(this.opcion2 == '10') {
      this.rootPage = 'WorkoutsClientPage';
    } else if(this.opcion2 == '11') {
      this.rootPage = 'LeadearboardClientPage';
    } else if(this.opcion2 == '12') {
      this.rootPage = 'BlogClientPage';
    } else if(this.opcion2 == '13') {
      this.rootPage = 'SocialClientPage';
    } else if(this.opcion2 == '14') {
      this.rootPage = 'ActivePauseClientPage';
    } else if(this.opcion2 == '15') {
      this.rootPage = 'PromotionClientPage';
    } else if(this.opcion2 == '16') {
      this.rootPage = 'MyMembershipClientPage';
    } else if(this.opcion2 == '17') {
      this.rootPage = 'BenefitsClientPage';
    } else if(this.opcion2 == '18') {
      this.rootPage = 'MyProgressClientPage';
    } else if(this.opcion2 == '19') {
      this.rootPage = 'RunningClubClientPage';
    } else if(this.opcion2 == '20') {
      this.rootPage = 'ZFitClubClientPage';
    } else if(this.opcion2 == '21') {
      this.rootPage = 'StaffClientPage';
    } else if(this.opcion2 == '22') {
      this.rootPage = 'MotivationClientPage';
    } else if(this.opcion2 == '23') {
      this.rootPage = 'CitesClientPage';
    } else if(this.opcion2 == '24') {
      this.rootPage = 'InformationClientPage';
    } else {
      if(this.baseIdUser == 82) {
        this.rootPage = 'CategorysDocumentsClientPage';
      } else {
        this.rootPage = 'CategorysClientPage';
      }   
    }
  }

  //GET DATOS CLIENTE
  public getData() {
    this.appMembresia = localStorage.getItem('currentAppMembresia');
    this.baseId = localStorage.getItem('currentId');
    this.picture = localStorage.getItem("currentPicture");
    this.email = localStorage.getItem("currentEmail");
    this.opcion2 = localStorage.getItem("currentHome");
    this.firstName = localStorage.getItem("currentFirstName") + ' ' + localStorage.getItem("currentLastName");
    this.loadSingleUser(this.baseId);
    this.loadSingleUserApp(this.baseIdUser);
  }

}
