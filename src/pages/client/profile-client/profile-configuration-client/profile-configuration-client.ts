import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage, AlertController, ViewController } from 'ionic-angular';
import { path } from "../../../../app/config.module";
import { UsersService } from '../../../../app/service/users.service';
import { Events } from 'ionic-angular/util/events';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AddressService } from '../../../../app/service/address.service';
import { StaffService } from '../../../../app/service/staff.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'profile-configuration-client',
  templateUrl: 'profile-configuration-client.html'
})
export class ProfileConfigurationClientPage {
  //PROPIEDADES
  basePath:string = path.path;
  btnDisabled:boolean;
  btnDisabled2:boolean;
  btnDisabled3:boolean;
  btnDisabled4:boolean;
  membershipClient:boolean;
  parameter:any;
  title:string;
  data:any;
  table:any = [];
  staff:any = [];
  nacionality: Array<{name:string}>;
  idClient = +localStorage.getItem('currentId');
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  clientID = localStorage.getItem('currentId');
  profile = {
    picture: '',
    username: '',
    email: '',
    firstname: '',
    lastname: '',
    work: '',
    description: '',
    age: '',
    birthday: '',
    phone: '',
    referencia: '',
    inicioMembresia: '',
    finMembresia: '',
    tipoNivel: '',
    opcion18: '',
    referencias: [],
    id: 0
  }
  commerce = {
    inicioMembresia: '',
    finMembresia: '',
    tipoNivel: '',
    id: '',
    opcion14: 0
  }
  public changePassword = {
    old_pass: '',
	  new_pass : '',
	  new_pass_rep: '',
    id: +localStorage.getItem('currentId')
  }
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public events: Events,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public view: ViewController,
    public mainService: UsersService,
    public secondService: AddressService,
    public staffService: StaffService,
    public modal: ModalController
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getNacionality();
    if(this.parameter == '1') {
      this.title = 'Actualizar Perfil';
      this.getSingle();
    } else if(this.parameter == '2') {
      this.title = 'Cambiar Contraseña';      
    } else if(this.parameter == '4') {
      this.title = 'Validar Membresía';
      this.getAllStaff();
      this.getSingle();
    } else if(this.parameter == '5') {
      this.title = 'Mis Direcciones';
      this.getAll();
    }
  }

  //CARGAR USUARIOS
  public getAllStaff(){
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.staffService.getAllUser(path.id)
    .then(response => {
      this.staff = [];
      this.staff = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR USUARIO
  public getSingle() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(this.idClient)
    .then(response => {
      this.profile = response;
      if(response.tipoNivel > 0) {
        this.membershipClient = true;
      }
      if(response.referencias) {
        this.commerce.id = response.referencias.id;
        this.commerce.inicioMembresia = response.referencias.inicioMembresia;
        this.commerce.finMembresia = response.referencias.finMembresia;
        this.commerce.opcion14 = response.referencias.opcion14;
        this.commerce.tipoNivel = response.referencias.tipoNivel;
      }
      if(response.opcion18) {
        this.search();
      }
      load.dismiss();
    }).catch(error => {
      load.dismiss();
      console.log(error)
    })
  }


  //CARGAR USUARIO
  public getSingle2(id:any) {
    this.mainService.getSingle(id)
    .then(response => {
      this.data = response;
      
      console.log(response)
    }).catch(error => {
      console.log(error)
    })
  }

  //BUSCAR CÓDIGO
  public search() {
    if(this.profile.opcion18.length == 36) {
      console.log(this.profile.opcion18)
      this.mainService.findCommerce(this.profile.opcion18)
      .then(response => {
        console.log(response)
        if(response) {
          this.btnDisabled2 = true;
          this.getSingle2(response[0].id);
        } else {
          this.confirmation('Código No Encontrado', 'El código que ingreso no pertenece a ninguna empresa, verifique que lo haya escrito correctamente.');
          this.btnDisabled2 = false;
        }
      }).catch(error => {
        this.confirmation('Código No Encontrado', 'El código que ingreso no pertenece a ninguna empresa, verifique que lo haya escrito correctamente.');
        this.btnDisabled2 = false;
      })
    }    
  }

  //CAMBIAR FOTO DE PERFIL
  uploadImage(archivo, id) {
    let events = this.events;
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}users/upload/${this.idClient}`;
    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
          $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta)
          {
            $('#imgAvatar').attr("src",respuesta.picture)
            localStorage.setItem('currentPicture', respuesta.picture);
            events.publish('user:update');
            $("#"+id).val('')
        });
      } else {
        this.message('La imagen es demasiado grande.')
      }
    } else {
      this.message('El tipo de imagen no es válido.');
    }
  }

  //ACTUALIZAR PERFIL
  public updateProfile() {
    this.btnDisabled = true;
    this.profile.picture = localStorage.getItem('currentPicture');
    this.mainService.update(this.profile)
    .then(response => {
      localStorage.setItem('currentFirstName', this.profile.firstname);
      localStorage.setItem('currentLastName', this.profile.lastname);
      localStorage.setItem('currentEmail', this.profile.email);
      this.events.publish('user:update');
      this.closeModal();
      this.confirmation('Perfil Actualizado', 'Tu perfil ha sido actualizado exitosamente.');
    }).catch(error => {
      this.btnDisabled = false;
    });
  }

  //ACTUALIZAR PERFIL
  public updateProfile2() {
    this.btnDisabled2 = true;
    this.mainService.update(this.profile)
    .then(response => {
      localStorage.setItem('currentFirstName', this.profile.firstname);
      localStorage.setItem('currentLastName', this.profile.lastname);
      localStorage.setItem('currentEmail', this.profile.email);
      this.events.publish('user:updates');
      this.confirmation('Perfil Actualizado', 'Tu perfil ha sido actualizado exitosamente.');
    }).catch(error => {
      this.btnDisabled2 = false;
    });
  }

  //ACTUALIZAR PERFIL
  public updateProfile3() {
    this.btnDisabled = true;
    this.mainService.update(this.profile)
    .then(response => {
      localStorage.setItem('currentFirstName', this.profile.firstname);
      localStorage.setItem('currentLastName', this.profile.lastname);
      localStorage.setItem('currentEmail', this.profile.email);
      this.events.publish('user:updates');
      this.confirmation('Perfil Actualizado', 'Tu perfil ha sido actualizado exitosamente.');
    }).catch(error => {
      this.btnDisabled = false;
    });
  }

  //CAMBIAR CONTRASEÑA
  changePasswordProfile() {
    if(this.changePassword.old_pass == this.changePassword.new_pass) {
      this.message('No se puede usar la misma contraseña.');
    } else {
      if(this.changePassword.new_pass.length >= 8) {
        if(this.changePassword.new_pass == this.changePassword.new_pass_rep) {
          this.btnDisabled2 = true;
          this.mainService.changePassword(this.changePassword)
          .then(response => {
            this.closeModal();
            this.confirmation('Contraseña Actualizada', 'La contraseña ha sido actualizada exitosamente.');
          }).catch(error => {
            this.btnDisabled2 = false;
            this.message('Contraseña inválida.')
          })
        } else {
          this.message('Las contraseñas no coinciden.');
        }
      } else {
        this.message('La contraseña debe contener al menos 8 caracteres.');        
      }
    }
  }

  //NACIONALIDADES
  public getNacionality() {
    this.nacionality = [{name: 'No Aplica'},
    {name: 'Antigua y Barbuda'},
    {name: 'Argentina'},
    {name: 'Bahamas'},
    {name: 'Barbados'},
    {name: 'Belice'},
    {name: 'Bolivia'},
    {name: 'Brasil'},
    {name: 'Canadá'},
    {name: 'Barbados'},
    {name: 'Chile'},
    {name: 'Colombia'},
    {name: 'Costa Rica'},
    {name: 'Cuba'},
    {name: 'Dominica'},
    {name: 'Ecuador'},
    {name: 'El Salvador'},
    {name: 'Estados Unidos'},
    {name: 'Granada'},
    {name: 'Guatemala'},
    {name: 'Guyana'},
    {name: 'Haití'},
    {name: 'Honduras'},
    {name: 'Jamaica'},
    {name: 'México'},
    {name: 'Nicaragua'},
    {name: 'Panamá'},
    {name: 'Paraguay'},
    {name: 'Perú'},
    {name: 'República Dominicana'},
    {name: 'San Cristóbal y Nieves'},
    {name: 'San Vicente y las Granadinas'},
    {name: 'Santa Lucía'},
    {name: 'Surinam'},
    {name: 'Trinidad y Tobago'},
    {name: 'Uruguay'},
    {name: 'Venezuela'}]
  }

  //CANJEAR MEMBRESÍA
  validateMembership() {
    let clientMembership = {
      id: this.profile.id,
      inicioMembresia: this.commerce.inicioMembresia,
      finMembresia: this.commerce.finMembresia,
      tipoNivel: this.commerce.tipoNivel,
      opcion13: 1
    }
    if(+this.commerce.opcion14 >= 1) {
      var commerce = {
        id: this.commerce.id,
        opcion14: +this.commerce.opcion14 - 1
      }
    }
    this.btnDisabled3 = true;
    if(commerce.opcion14 == 0) {
      this.message('El usuario ya no cuenta con membresías disponibles.');      
    } else {
      this.mainService.update(clientMembership)
      .then(response => {
        this.mainService.update(commerce)
        .then(response => {
          this.closeModal();
          this.confirmation('Membresía Actualizada', 'Tu membresía ha sido actualizada exitosamente, por favor revisar la validación en el módulo de membresías.');
        }).catch(error => {
          this.message('Ha ocurrido un error, intente más tarde.');
        });
      }).catch(error => {
        this.message('Ha ocurrido un error, intente más tarde.');
      });
    }
  }

  //CANJEAR MEMBRESÍA
  validateMembershipCommerce() {
    let clientMembership = {
      id: this.profile.id,
      inicioMembresia: this.data.inicioMembresia,
      finMembresia: this.data.finMembresia,
      tipoNivel: this.data.tipoNivel,
      opcion13: 2
    }
    if(+this.data.opcion14 >= 1) {
      var commerce = {
        id: this.data.id,
        opcion14: +this.data.opcion14 - 1
      }
    }
    this.btnDisabled4 = true;
    if(commerce.opcion14 == 0) {
      this.message('El usuario ya no cuenta con membresías disponibles.');      
    } else {
      this.mainService.update(clientMembership)
      .then(response => {
        this.mainService.update(commerce)
        .then(response => {
          this.closeModal();
          this.confirmation('Membresía Actualizada', 'Tu membresía ha sido actualizada exitosamente, por favor revisar la validación en el módulo de membresías..');
        }).catch(error => {
          this.message('Ha ocurrido un error, intente más tarde.');
        });
      }).catch(error => {
        this.message('Ha ocurrido un error, intente más tarde.');
      });
    }
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAll()
    .then(response => {
      this.table = [];
      this.table = response;
      load.dismiss();
    }).catch(error => {
      load.dismiss();
      console.clear
    })
  }

  //OPEN MODAL
  openModal(parameter?:any) {
    let chooseModal = this.modal.create('ModalAddressUserPage', { parameter });
    chooseModal.onDidDismiss((data) => {
      if(data!='Close') {
        this.getAll();
      }
    })
    chooseModal.present();
  }

  //CONFIRMACIÓN
  confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //MENSAJES
  message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 1000
    }).present();
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss('Close');
  }
}
