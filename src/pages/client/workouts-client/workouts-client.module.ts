import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkoutsClientPage } from './workouts-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    WorkoutsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkoutsClientPage),
    PipeModule
  ],
  exports: [
    WorkoutsClientPage
  ]
})
export class WorkoutsClientPageModule {}