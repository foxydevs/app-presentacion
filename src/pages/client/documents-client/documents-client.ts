import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { InAppBrowser } from '../../../../node_modules/@ionic-native/in-app-browser';
import { path } from '../../../app/config.module';
import { Platform } from 'ionic-angular/platform/platform';
import { IonicPage } from '../../../../node_modules/ionic-angular/navigation/ionic-page';
import { DocumentsService } from '../../../app/service/documents.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'documents-client',
  templateUrl: 'documents-client.html'
})
export class DocumentsClientPage {
  //PROPIEDADES
  public products:any[] = [];
  public parameter:any;
  public search:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public mainService: DocumentsService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams,
    public iab: InAppBrowser,
    public platform: Platform,
    public toast: ToastController
  ) {
    this.parameter = this.navParams.get('parameter');
  }

  //CARGAR PRODUCTOS POR CATEGORIA
  public loadAllForCategories(id:any){
    this.mainService.getAllUser(path.id)
    .then(response => {
      this.products = [];
      for(let x of response) {
        if(x.category == id) {
          this.products.push(x);
        }
      }
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR PRODUCTOS
  public loadAll() {
    this.mainService.getAllUser(path.id)
    .then(response => {
      this.products = [];
      this.products = response;
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    if(this.parameter) {
      this.loadAllForCategories(this.parameter);
    } else {
      this.loadAll();
    }
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any, title:any) {
    //this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    if (this.platform.is('android')) {
      this.iab.create('http://docs.google.com/gview?url=' + urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#003D6E');      
    } else {
      this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    }
  }

  //DOCUMENTO NO DISPONIBLE
  documentEmpty() {
    this.message('El documento no se encuentra disponible.');
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList button").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.parameter) {
        this.loadAllForCategories(this.parameter);
      } else {
        this.loadAll();
      }
      refresher.complete();
    }, 2000);
  }

}
