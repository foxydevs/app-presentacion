import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CitesClientPage } from './cites-client';
import { NgCalendarModule } from 'ionic2-calendar';
 
@NgModule({
  declarations: [
    CitesClientPage,
  ],
  imports: [
    IonicPageModule.forChild(CitesClientPage),
    NgCalendarModule,
  ],
  exports: [
    CitesClientPage
  ]
})
export class CitesClientPageModule {}