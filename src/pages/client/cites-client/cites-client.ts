import { Component } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';
import * as moment from 'moment';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { CitesService } from '../../../app/service/cites.service';
import { path } from '../../../app/config.module';
import { IonicPage } from '../../../../node_modules/ionic-angular/navigation/ionic-page';

@IonicPage()
@Component({
  selector: 'cites-client',
  templateUrl: 'cites-client.html'
})
export class CitesClientPage {
  eventSource = [];
  citas = [];
  viewTitle: string;
  selectedDay = new Date();
  selectItem:any = 'citas';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  idUserClient = localStorage.getItem('currentId')
  calendar = {
    mode: 'month',
    currentDate: new Date()
  };
  
  constructor(public navCtrl: NavController, 
    public modalCtrl: ModalController, 
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public mainService: CitesService) {
      this.getAll(path.id);
    }
 
  addEvent() {
    let modal = this.modalCtrl.create('FormCitesClientPage', {selectedDay: this.selectedDay});
    modal.present();
    modal.onDidDismiss(data => {
      this.getAll(path.id)
    });
  }

  update(event) {
    let modal = this.modalCtrl.create('FormCitesClientPage', {parameter: event.id});
    modal.present();
    modal.onDidDismiss(data => {
      this.getAll(path.id);
      this.getAllSecond();
    }); 
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(id)
    .then(response => {
      this.eventSource = [];
      for(let x of response) {
        let data = {
          startTime: new Date(x.fechaInicio),
          endTime: new Date(x.fechaFin),
          title: x.titulo,
          id: x.id,
        }
        this.eventSource.push(data);
      }
      console.log(this.eventSource)
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR LOS RETOS
  public getAllSecond() {
    this.mainService.getByCiteClient(this.idUserClient)
    .then(response => {
      this.citas = [];
      response.forEach(x => {
        let data = {
          aprobacion: x.aprobacion,
          titulo: x.titulo,
          fecha: moment(x.fechaInicio).format('LL'),
          horaInicio: moment(x.fechaInicio).format('HH:mm'),
          horaFin: moment(x.fechaFin).format('HH:mm'),
          id: x.id
        }
        this.citas.push(data)
      });
    }).catch(error => {
      console.clear
    })
  }
 
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
 
  onEventSelected(event) {
    let start = moment(event.startTime).format('LL');
    let hourStart = moment(event.startTime).format('HH:mm');
    let hourEnd = moment(event.endTime).format('HH:mm');
    
    let alert = this.alertCtrl.create({
      title: '' + event.title,
      subTitle: '<ion-icon ios="ios-calendar" md="md-calendar"></ion-icon>Fecha: ' + start + 
      '<br><ion-icon ios="ios-time" md="md-time"></ion-icon>Hora: ' + hourStart +  ' - '+ hourEnd,
      buttons: ['OK']
    })
    alert.present();
  }
 
  onTimeSelected(ev) {
    this.selectedDay = ev.selectedTime;
  }
}