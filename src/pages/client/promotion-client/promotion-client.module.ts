import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromotionClientPage } from './promotion-client';
import { PipeModule } from '../../../pipes/pipes.module';
import { DescuentoService } from '../../../app/service/descuentos.service';
 
@NgModule({
  declarations: [
    PromotionClientPage,
  ],
  imports: [
    IonicPageModule.forChild(PromotionClientPage),
    PipeModule
  ],
  exports: [
    PromotionClientPage
  ],
  providers: [
    DescuentoService
  ]
})
export class PromotionClientPageModule {}