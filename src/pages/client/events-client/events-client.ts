import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, IonicPage, ToastController } from 'ionic-angular';
import { EventsService } from '../../../app/service/events.service';

import { path } from "./../../../app/config.module";
import { MembershipService } from '../../../app/service/membership.service';

@IonicPage()
@Component({
  selector: 'events-client',
  templateUrl: 'events-client.html'
})
export class EventsClientPage {
  //PROPIEDADES
  public events:any[] = [];
  public baseId:number = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  authentication:any = localStorage.getItem('currentAuthentication');
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public eventsService: EventsService,
    public alertCtrl :AlertController,
    public mainService: MembershipService,
    public toast:ToastController
  ) {
    this.getAll();
    this.mainService.calculateMembership();
  }
  
  //GET
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.eventsService.getAllType(this.baseId, 1).then(response => {
      this.events = response;
      console.log(response)
      load.dismiss();
    }).catch(error => {
      console.clear();
    })
  }

  //Ver Más del Evento
  public viewMore(parameter: any) {
    if(this.authentication == 'NoAuthentication') {
      this.message('Usted debe iniciar sesión.')
    } else {
      this.loading.create({
        content: "Cargando",
        duration: 1000
      }).present();
      this.navCtrl.push('SeeEventsClientPage', { parameter });
    }
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 1000
    }).present();
  }

}
