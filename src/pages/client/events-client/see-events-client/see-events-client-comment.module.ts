import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeeEventsClientCommentPage } from './see-events-client-comment';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SeeEventsClientCommentPage,
  ],
  imports: [
    IonicPageModule.forChild(SeeEventsClientCommentPage),
    PipeModule
  ],
  exports: [
    SeeEventsClientCommentPage
  ]
})
export class SeeEventsClientCommentPageModule {}