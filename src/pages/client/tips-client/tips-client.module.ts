import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TipsClientPage } from './tips-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    TipsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(TipsClientPage),
    PipeModule
  ],
  exports: [
    TipsClientPage
  ]
})
export class TipsClientPageModule {}