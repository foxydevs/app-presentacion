import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { TipsService } from '../../../../app/service/tips.service';

@IonicPage()
@Component({
  selector: 'see-tips-client-comment',
  templateUrl: 'see-tips-client-comment.html'
})
export class SeeTipsClientCommentPage {
  public comment = {
    comment: '',
    post : '',
    user: ''
  }
  public parameter:any;
  public btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public tipsService: TipsService,
    public viewCtrl: ViewController,
    public loading: LoadingController
  ) {
    this.btnDisabled = false;
    this.parameter = this.navParams.get('parameter');
    this.comment.post = this.parameter;
    this.comment.user = localStorage.getItem('currentId');
  }

   //GUARDAR CAMBIOS
   public saveChanges() {
    if(this.comment.comment) {
      this.btnDisabled = true;
      this.tipsService.createComment(this.comment)
      .then(response => {
        this.loading.create({
          content: "Registrando Comentario...",
          duration: 2000
        }).present();        
        this.navCtrl.pop();
        console.clear
      }).catch(error => {
        this.btnDisabled = false;
        console.clear
      })
    } else {
      this.toast.create({
        message: "Ingrese un comentario.",
        duration: 800
      }).present();
    }
  }

}
