import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { Platform } from 'ionic-angular/platform/platform';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { IonicPage } from '../../../../../node_modules/ionic-angular/navigation/ionic-page';

@IonicPage()
@Component({
  selector: 'influencers-client',
  templateUrl: 'influencers-client.html'
})
export class InfluencersClientPage {
  //PROPIEDADES
  public categorys:any[] = [];
  public parameter:any;
  navColor = localStorage.getItem('currentColor');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams,
    public iab: InAppBrowser,
    public platform: Platform,
    public toast: ToastController
  ) {
    this.parameter = this.navParams.get('parameter');
    console.log(this.parameter)
    for(let i=1; i<=this.parameter; i++) {
      let data = {
        id: i,
        nombre: 'Influencer ' +i,
        description: 'Influencer ' +i,
        picture: null
      }
      this.categorys.push(data);
    }
  }

  add() {
    let data = {
        id: this.suma(),
        nombre: 'Influencer ' +this.suma(),
        description: 'Influencer ' +this.suma(),
        picture: null
    }
    this.categorys.push(data);
  }

  suma() {
    return +this.categorys.length +1;
  }

  /*getAll() {
    this.categorys = [
      {
        id: 1,
        nombre: 'Influencer 1',
        description: 'Influencer 1',
        picture: null
      },
      {
        id: 2,
        nombre: 'Influencer 1',
        description: 'Influencer 1',
        picture: null
      },
      {
        id: 3,
        nombre: 'Influencer 1',
        description: 'Influencer 1',
        picture: null
      },
      {
        id: 4,
        nombre: 'Influencer 1',
        description: 'Influencer 1',
        picture: null
      },
    ]
  }*/

  openProducts(parameter?:any) {
    this.navCtrl.push('ProductsClientPage');
  }

}
