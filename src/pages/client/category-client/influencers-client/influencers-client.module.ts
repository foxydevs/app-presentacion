import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfluencersClientPage } from './influencers-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    InfluencersClientPage,
  ],
  imports: [
    IonicPageModule.forChild(InfluencersClientPage),
    PipeModule
  ],
  exports: [
    InfluencersClientPage
  ]
})
export class InfluencersClientPageModule {}