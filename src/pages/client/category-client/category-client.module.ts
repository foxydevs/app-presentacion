import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../pipes/pipes.module';
import { CategoryClientPage } from './category-client';
 
@NgModule({
  declarations: [
    CategoryClientPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoryClientPage),
    PipeModule
  ],
  exports: [
    CategoryClientPage
  ]
})
export class CategoryClientPageModule {}