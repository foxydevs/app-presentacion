import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { InAppBrowser } from '../../../../node_modules/@ionic-native/in-app-browser';
import { Platform } from 'ionic-angular/platform/platform';
import { IonicPage } from '../../../../node_modules/ionic-angular/navigation/ionic-page';

@IonicPage()
@Component({
  selector: 'category-client',
  templateUrl: 'category-client.html'
})
export class CategoryClientPage {
  //PROPIEDADES
  public categorys:any[] = [];
  public parameter:any;
  navColor = localStorage.getItem('currentColor');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams,
    public iab: InAppBrowser,
    public platform: Platform,
    public toast: ToastController
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getAll();
  }

  getAll() {
    this.categorys = [
      {
        id: 1,
        nombre: 'Categoría 1',
        description: 'Categoría 1',
        picture: null
      },
      {
        id: 2,
        nombre: 'Categoría 2',
        description: 'Categoría 2',
        picture: null
      },
      {
        id: 3,
        nombre: 'Categoría 3',
        description: 'Categoría 3',
        picture: null
      },
      {
        id: 4,
        nombre: 'Categoría 4',
        description: 'Categoría 4',
        picture: null
      },
    ]
  }

  openCategory(parameter?:any) {
      
    this.navCtrl.push('InfluencersClientPage', { parameter });
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Cantidad',
      inputs: [
        {
          type: 'radio',
          label: '1',
          value: '1'
        },
        {
          type: 'radio',
          label: '2',
          value: '2'
        },
        {
          type: 'radio',
          label: '3',
          value: '3'
        },
        {
          type: 'radio',
          label: '4',
          value: '4'
        },
        {
          type: 'radio',
          label: '5',
          value: '5'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: (data:any) => {
              console.log(data)
            this.openCategory(data);
            /*if (User.isValid(data.username, data.password)) {
              // logged in!
            } else {
              // invalid login
              return false;
            }*/
          }
        }
      ]
    });
    alert.present();
  }

}
