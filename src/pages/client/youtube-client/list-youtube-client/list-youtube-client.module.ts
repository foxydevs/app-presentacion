import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListYoutubeClientPage } from './list-youtube-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    ListYoutubeClientPage,
  ],
  imports: [
    IonicPageModule.forChild(ListYoutubeClientPage),
    PipeModule
  ],
  exports: [
    ListYoutubeClientPage
  ]
})
export class ListYoutubeClientPageModule {}