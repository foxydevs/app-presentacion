import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { MyCartOrderClient } from './my-cart-order-client';
import { AddressService } from '../../../../app/service/address.service';
import { DescuentoService } from '../../../../app/service/descuentos.service';
 
@NgModule({
  declarations: [
    MyCartOrderClient,
  ],
  imports: [
    IonicPageModule.forChild(MyCartOrderClient),
    PipeModule
  ],
  exports: [
    MyCartOrderClient
  ], providers: [
    AddressService,
    DescuentoService
  ]
})
export class MyCartOrderClientModule {}