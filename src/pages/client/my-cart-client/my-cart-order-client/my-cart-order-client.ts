import { Component, OnInit } from '@angular/core';
import { NavController, ViewController, LoadingController, IonicPage, AlertController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { SalesService } from '../../../../app/service/sales.service';
import { AddressService } from '../../../../app/service/address.service';
import { DescuentoService } from '../../../../app/service/descuentos.service';


//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'my-cart-order-client',
  templateUrl: 'my-cart-order-client.html'
})
export class MyCartOrderClient implements OnInit {
  public data = {
    cliente: +localStorage.getItem('currentId'),
    usuario: path.id,
    total: 0,
    comment: '',
    nombre: '',
    unit_price: 0,
    cantidad: 0,
    tipo: 1,
    tipoPlazo: '',
    plazo: '',
    comentario: '',
    deposito: '',
    direccion: '',
    fecha: '',
    image: '',
    state: 4,
    comprobante: 1,
    opcion: 1,
    detalle: [],
    archivo: localStorage.getItem('currentPicture11'),
  }
  public data2 = {
    id: 0,
    cantidad: 0,
    codigo: '',
    cliente: localStorage.getItem('currentId'),
    usado: 0
  }
  public url:any;
  public cart:any[] = [];
  public Ventas:any[] = [];
  public table:any[] = [];
  public btnDisabled:boolean;
  public btnDisabled2:boolean;
  public basePath:string = path.path;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(public navCtrl: NavController,
  public loading:LoadingController,
  public navParams: NavParams,
  public toast: ToastController,
  public view: ViewController,
  public mainService: SalesService,
  public secondService: AddressService,
  public thirdService: DescuentoService,  
  public alertCtrl: AlertController) {
    //this.loadInvoice();
    this.cart = JSON.parse(localStorage.getItem('cart'));
    this.data.nombre = localStorage.getItem('currentFirstName') + ' ' + localStorage.getItem('currentLastName');
    this.data.detalle = this.cart;
    this.getDate();
    this.dataCart();
    //this.getAll();
    this.getAllSecond();
  }

  onChange(e:any) {
    console.log(e)
    this.dataCart()
    if(e == 1) {
      this.data.total = this.data.total + 25;
    } else {
      this.data.total = this.data.total + 35;
    }
  }

  ngOnInit() {
    $('#imgAvatar2').hide();
  }

  //OBTENER FECHA
  public getDate() {
    let date = new Date();
    let month = date.getMonth()+1;
    let month2;
    let dia= date.getDate();
    let dia2;
    if(month<10){
      month2='0'+month;
    }else{
      month2=month
    }
    if(dia<10){
      dia2='0'+dia;
    }else{
      dia2=dia
    }
    this.data.fecha= date.getFullYear()+'-'+month2+'-'+dia2;
  }

  //GET SINGLE
  search() {
      let load = this.loading.create({
        content: 'Cargando...'
      });
      load.present();
      this.thirdService.search(path.id, this.data2.codigo, 'codigo')
      .then(response => {
        console.log(response)
        if(response[0]) {
          if(response[0].usado == '1') {
            this.confirmation('Código Usado', 'El código ya ha sido utilizado');
          } else {
            this.btnDisabled2 = true;
            this.data2 = response[0];
            this.data.total = this.data.total - (+this.data.total * (this.data2.cantidad/100));
            this.confirmation('Código Encontrado', 'El código de descuento ha sido aplicado exitosamente su descuento es de '+ response[0].cantidad.toString() + '%');
          }
          load.dismiss();
        } else {
          this.confirmation('Código No Encontrado', 'El código no se encuentra disponible.');
          load.dismiss();
        }  
      }).catch(error => {
        load.dismiss();
      })
  }

  //UPDATE
  public updateSecond() {
    this.data2.usado = 1;
    this.data2.cliente = localStorage.getItem('currentId');
    if(this.data2.id != 0) {
      this.thirdService.update(this.data)
      .then((res) => {
        console.log(res)
      }).catch((error) => {
        console.log(error)
      })
    }    
  }

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss();
  }
  
  public saveChanges() {
    console.log(this.data.detalle)
    this.data.archivo = $('img[alt="Avatar"]').attr('src');
    if(this.data.tipo == 2) {
      this.data.state = 5;
    }/* else if(this.data.tipo == 3) {
      this.data.state = 3;
    }*/
    if(this.data.archivo == 'https://bpresentacion.s3.us-west-2.amazonaws.com/products/M7Xjg9Ih4fCEhK5SQ2y2r3Go70pv9YJ2jgx7CcQt.png'
    || this.data.archivo == this.data.image) {
      if(this.data.direccion) {
        this.data.archivo = '';
        this.create(this.data);
      } else {
        this.message('La dirección es requerida.', 1000);
      }
    } else {
      if(this.data.direccion) {
        console.log(this.data)
        this.create(this.data);
      } else {
        this.message('La dirección es requerida.', 1000);
      }
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.btnDisabled = true;
    this.mainService.create(formValue)
    .then(response => {
      this.confirmation('Venta Agregada', 'La venta fue agregada exitosamente.');
      localStorage.setItem('cart', JSON.stringify([]));
      //DESCUENTO
      this.updateSecond();    
      this.navCtrl.setRoot('MyOrdersClientPage');
    }).catch(error => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //CARGAR PRODUCTOS
  public getAll() {
    this.mainService.getAllTypes()
    .then(response => {
      console.log(response)
      this.Ventas = [];
      this.Ventas = response;
    }).catch(error => {
      console.clear
    })
  }

  /*//CARGAR FACTURA
  public loadInvoice(){
    this.salesService.getComprobante()
    .then(response => {
      this.data.comprobante = +response.comprobante + 1;
    }).catch(error => {
      console.clear
    })
  }*/

  //DETALLES
  public dataCart() {
    let a:number = 0;
    let b:number = 0;
    let c:number = 0;
    let d:number = 0;
    for(let x of this.cart) {
      a = x.subtotal;
      b += a;
      c = +x.cantidad;
      d += c;
    }
    this.data.total = b;
    this.data.unit_price = b;
    this.data.cantidad = d;
  }

  //CREATE
  /*create() {
    alert('En desarrollo.')
  }

 /* //GUARDAR CAMBIOS
  public create() {
    console.log(this.data)
    let ern = this.generate(4);
    this.data.ern = ern;
    let order = {
      cantidad: this.data.detalle.length,
      precio: this.data.total,
      ern:ern,
      descripcion:`Venta por el total de ${this.data.detalle.length} artículos un total de Q ${this.data.total} `,
      url: "www.foxylabs.gt"
    }
    console.log(this.data)    
    if(this.data.cantidad >= 1) {
      if(this.data.formapago == '2') {
        this.salesService.pagar(order)
        .then(response2 => {
          console.log(response2)
          this.salesService.create(this.data)
          .then(res => {
            let url = response2.token;
            let parameter = {
              ern: ern,
              tokenID: ''
            }
            const browser = this.iab.create(url, '_blank', 'location=yes,clearsessioncache=yes,clearcache=yes');
            browser.on('loadstart').subscribe((e) => {
              this.url = e.url;
              //URL TOKEN NUEVO
              let tokenUrlComparation = e.url;
              //URL TOKEN A COMPARAR
              let token = this.url.replace('http://me.gtechnology.gt/home/cliente/pedidos/','');
              let ernReplace = '/' + ern;
              let tokenFinally = token.replace(ernReplace, '')
              parameter.tokenID = tokenFinally;
              let urlFinally = 'http://me.gtechnology.gt/home/cliente/pedidos/' + tokenFinally + ernReplace
              if(tokenUrlComparation == urlFinally) {
                //alert('CONGRATULATIONS')
                if(tokenFinally.length >= 30) {
                  this.navCtrl.setRoot(OrdersPage)
                  browser.close()
                  this.loading.create({
                    content: 'Procesando Compra...',
                    duration: 3000
                  }).present();
                  //let chooseModal = this.modal.create(dataOrdersClientPage, { parameter });
                  //chooseModal.present();
                } else {
                  this.loading.create({
                    content: 'Saliendo de la Compra...',
                    duration: 2000
                  }).present();
                  this.navCtrl.setRoot(CategoriesPage)
                }  
              }
            });
          }).catch(error => {
            console.log(error)
          });
        }).catch(error => {
          console.log(error)
        });
      }
    }
  }*/

  public generate(longitude)
  {
    let i:number
    var caracteres = "123456789+-*abcdefghijkmnpqrtuvwxyz123456789+-*ABCDEFGHIJKLMNPQRTUVWXYZ12346789+-*";
    var password = "";
    for (i=0; i<longitude; i++) password += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
    return '02' + password;
  }

  //MENSAJES
  public message(messages: any, duration:any) {
    this.toast.create({
      message: messages,
      duration: duration
    }).present();
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //IMAGEN DE ORDEN
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg" || type == 'application/pdf') {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif');
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'orders'
          },
          function(respuesta) {
            if(type == 'application/pdf') {
              $('#imgAvatar').attr("src", respuesta.url);
              $('#imgAvatar2').show();
              $('#imgAvatar').hide();              
              $('#imgAvatar2').attr("src", 'https://developers.zamzar.com/assets/app/img/convert/pdf.png');
              $("#"+id).val('');
            } else {
              $('#imgAvatar').attr("src", respuesta.url);
              $("#"+id).val('');
            }
          }
        );
      } else {
        this.message('El archivo es demasiado grande.', 800)
      }
    } else {
      this.message('El tipo de archivo no es válido.', 800)
    }
  }

  //CARGAR LOS RETOS
  public getAllSecond() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAll()
    .then(response => {
      this.table = [];
      this.table = response;
      load.dismiss();
    }).catch(error => {
      load.dismiss();
      console.clear
    })
  }
 
}