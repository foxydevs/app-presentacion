import { Component, OnInit } from '@angular/core';
import { NavController, ViewController, LoadingController, IonicPage, AlertController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { path } from '../../../../app/config.module';
import { SalesService } from '../../../../app/service/sales.service';
import { AddressService } from '../../../../app/service/address.service';
import { OrdersService } from '../../../../app/service/orders.service';
import * as moment from 'moment';
import { DescuentoService } from '../../../../app/service/descuentos.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'my-cart-qpp-client',
  templateUrl: 'my-cart-qpp-client.html'
})
export class MyCartQPPClient implements OnInit {
  public data = {
    id: path.id,
    cliente: +localStorage.getItem('currentId'),
    infodeposito: localStorage.getItem('currentDeposito'),
    usuario: path.id,
    total: 0,
    nombre: '',
    unit_price: 0,
    cantidad: 0,
    tipo: 3,
    tipoPlazo: '',
    plazo: '',
    comment: '',
    deposito: '',
    direccion: '',
    fecha: '',
    image: '',
    state: 4,
    comprobante: 1,
    detalle: [],
    archivo: localStorage.getItem('currentPicture11'),
    x_product_id: '1', //1
    x_currency_code: 'GTQ', //1
    x_amount: 0.0, //1
    x_line_item: '1',  //
    x_freight: '0', //
    x_email: '', //1
    cc_number: '', //1
    cc_exp: '', //1
    cc_cvv2: '', //1
    cc_name: '', //1
    cc_type: '', //1
    x_first_name: '', //1
    x_last_name: '', //1
    x_company: '1',  //1
    x_address: '1', //1
    x_city: '1', //1
    x_state: '1', //1
    x_country: '1', //1
    x_zip: '1', //1
    http_origin: 'none', //??
    visaencuotas: '0', //NO VISACUOTAS
  }
  public data2 = {
    codigo: '',
    latitude: 0,
    cantidad: 0,
    id: 0
  }
  public url:any;
  public cart:any[] = [];
  public Ventas:any[] = [];
  public table:any[] = [];
  disabledBtn:boolean;
  btnDisabled2:boolean;
  public basePath:string = path.path;
  public userID:string = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(public navCtrl: NavController,
  public loading:LoadingController,
  public navParams: NavParams,
  public toast: ToastController,
  public view: ViewController,
  public mainService: OrdersService,
  public thirdService: SalesService,
  public secondService: AddressService,
  public fourthService: DescuentoService,
  public alertCtrl: AlertController) {
    //this.loadInvoice();
    this.cart = JSON.parse(localStorage.getItem('cart'));
    this.data.x_first_name = localStorage.getItem('currentFirstName');
    this.data.x_last_name = localStorage.getItem('currentLastName');
    this.data.x_email = localStorage.getItem('currentEmail');
    this.data.detalle = this.cart;
    this.getDate();
    this.dataCart();
    this.getAll();
    this.getAllSecond();
  }

  ngOnInit() {
    $('#imgAvatar2').hide();
  }

  //OBTENER FECHA
  public getDate() {
    let date = new Date();
    let month = date.getMonth()+1;
    let month2;
    let dia= date.getDate();
    let dia2;
    if(month<10){
      month2='0'+month;
    }else{
      month2=month
    }
    if(dia<10){
      dia2='0'+dia;
    }else{
      dia2=dia
    }
    this.data.fecha = date.getFullYear()+'-'+month2+'-'+dia2;
  }

  /*public saveChanges() {
    this.data.x_line_item = 'Varios<|>00000<|><|>' + this.data.cantidad + '<|>' + this.data.total + '<|>';
    this.data.x_amount = this.data.total;
    this.mainService.pagarQPP(this.data)
    .then(response => {
      console.log(response)
      this.messagesQPayPro(+response.responseCode);
    }).catch(error => {
      console.clear
      
    })
  }*/

  //CERRAR MODAL
  public closeModal() {
    this.view.dismiss();
  }
  
  public saveChanges() {
    if(this.userID == '144') {
      let confirm = this.alertCtrl.create({
        title: 'Información de Entregas',
        subTitle: 'Todos los pedidos recibidos antes de las 6PM serán entregados al siguiente día hábil que la ruta pase en su zona. Carretera al Salvador, San José Pinula, Z. 15, Z. 10, Z. 13, Z. 9 y Z. 16: todos los días. Mixco, San Cristobal y el Naranjo: Jueves. Para poderte llevar a domicilio el mínimo de pedido es de Q50.00.',
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.saveChanges2();
            }
          }
        ]
      });
      confirm.present();
    } else {
      this.saveChanges2();
    }
  }

  public saveChanges2() {
    this.data.cc_exp = moment(this.data.cc_exp).format('MM/YY');
    console.log(this.data.cc_exp)
    this.data.archivo = $('img[alt="Avatar"]').attr('src');
    if(this.data.tipo == 3) {        
        this.data.x_line_item = 'Varios<|>00000<|><|>' + this.data.cantidad + '<|>' + this.data.x_amount + '<|>';
        this.mainService.pagarQPP(this.data)
        .then(response => {
          console.log(response);
          console.log(this.data);
          this.messagesQPayPro(response.responseCode, response.responseText);
          this.updateDescuento();
        }).catch(error => {
          console.log(error)
          this.confirmation('QPayPro Error', error);
          console.clear
        });
    } else {
        this.data.direccion = this.data.x_address;
       if(this.data.tipo == 2) {
          this.data.state = 5;
        }
        if(this.data.archivo == 'https://bpresentacion.s3.us-west-2.amazonaws.com/products/M7Xjg9Ih4fCEhK5SQ2y2r3Go70pv9YJ2jgx7CcQt.png'
        || this.data.archivo == this.data.image) {
          if(this.data.direccion) {
            this.data.archivo = '';
            this.create(this.data);
          } else {
            this.message('La dirección es requerida.', 1000);
          }
        } else {
          if(this.data.direccion) {
            console.log(this.data)
            this.create(this.data);
          } else {
            this.message('La dirección es requerida.', 1000);
          }
        }
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.disabledBtn = true;
    this.thirdService.create(formValue)
    .then(response => {
      this.confirmation('Venta Agregada', 'La venta fue agregada exitosamente.');
      localStorage.setItem('cart', JSON.stringify([]));      
      this.navCtrl.setRoot('MyOrdersClientPage');
      this.updateDescuento();
    }).catch(error => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //CARGAR PRODUCTOS
  public getAll() {
    this.thirdService.getAllTypes()
    .then(response => {
      console.log(response)
      this.Ventas = [];
      this.Ventas = response;
    }).catch(error => {
      console.clear
    })
  }

  //IMPRESIÓN DE ERRORES
  public messagesQPayPro(responseCode:any, responseText:string) {
    if(responseCode == '00') {
      this.data.state = 3;
      this.disabledBtn = true;
      this.thirdService.create(this.data)
      .then(response => {
        this.confirmation('QPayPro - ' + responseCode, responseText);
        localStorage.setItem('cart', JSON.stringify([]));      
        this.navCtrl.setRoot('MyOrdersClientPage');
      }).catch(error => {
        this.disabledBtn = false;
        console.clear
      });
    } else {
      this.confirmation('QPayPro - ' + responseCode, responseText);
      this.disabledBtn = true;
    }
    /*switch (responseCode) {
      case '00':
        this.confirmation('QPayPro', 'La transacción se ha realizado con éxito.');
        break;
      case '01':
        this.confirmation('QPayPro', 'Denegado, por favor refiérase al banco.');
        break;
      case '02':
        this.confirmation('QPayPro', 'Denegado, por favor refiérase al banco.');
        break;
      case '03':
        this.confirmation('QPayPro', 'Comercio inválido.');
        break;
      case '04':
        this.confirmation('QPayPro', 'Denegado, llamar al banco.');
        break;
      case '05':
        this.confirmation('QPayPro', 'Denegado, transacción no aceptada.');
        break;
      case '07':
        this.confirmation('QPayPro', 'Retención de tarjeta.');
        break;
      case '12':
        this.confirmation('QPayPro', 'Emisor inválido.');
        break;
      case '13':
        this.confirmation('QPayPro', 'Fondos Insuficientes.');
        break;
      case '14':
        this.confirmation('QPayPro', 'Tarjeta Inválida.');
        break;
      case '15':
        this.confirmation('QPayPro', 'Emisor inválido.');
        break;
      case '19':
        this.confirmation('QPayPro', 'Por favor, intente nuevamente.');
        break;
      case '25':
        this.confirmation('QPayPro', 'Registro inexistente..');
        break;
      case '30':
        this.confirmation('QPayPro', 'Error de formato.');
        break;
      case '31':
        this.confirmation('QPayPro', 'Tarjeta no soportada.');
        break;
      case '35':
        this.confirmation('QPayPro', 'La venta ya ha sido anulada.');
        break;
      case '36':
        this.confirmation('QPayPro', 'La venta no existe.');
        break;
      case '41':
        this.confirmation('QPayPro', 'Tarjeta Perdida.');
        break;
      case '43':
        this.confirmation('QPayPro', 'Tarjeta Robada.');
        break;
      case '51':
        this.confirmation('QPayPro', 'Fondos insuficientes, por favor comunicarse con el banco.');
        break;
      case '54':
        this.confirmation('QPayPro', 'Su tarjeta venció, por favor comunicarse con el banco.');
        break;
      case '57':
        this.confirmation('QPayPro', 'Transacción no permitida.');
        break;
      case '58':
        this.confirmation('QPayPro', 'Transacción denegada.');
        break;
      case '61':
        this.confirmation('QPayPro', 'Transacción denegada, monto exedido.');
        break;
      case '62':
        this.confirmation('QPayPro', 'Transacción denegada, tarjeta de crédito restringida.');
        break;
      case '65':
        this.confirmation('QPayPro', 'Denegado, número de transacciones excedidas.');
        break;
      case '78':
        this.confirmation('QPayPro', 'Cuenta inválida.');
        break;
      case '85':
        this.confirmation('QPayPro', 'Transacción inválida.');
        break;
      case '89':
        this.confirmation('QPayPro', 'Terminal inválida.');
        break;
      case '91':
        this.confirmation('QPayPro', 'Emisor no disponible, inténtalo de nuevo.');
        break;
      case '96':
        this.confirmation('QPayPro', 'Mal funcionamiento del sistema, intente en unos minutos.');
        break;
      default:
        this.confirmation('QPayPro', codigo);
        break;
    }*/
  }

  //DETALLES
  public dataCart() {
    let a:number = 0;
    let b:number = 0;
    let c:number = 0;
    let d:number = 0;
    for(let x of this.cart) {
      a = x.subtotal;
      b += a;
      c = +x.cantidad;
      d += c;
    }
    this.data.x_amount = b;
    this.data.unit_price = b;
    this.data.cantidad = d;
  }

  //CREATE
  /*create() {
    alert('En desarrollo.')
  }

 /* //GUARDAR CAMBIOS
  public create() {
    console.log(this.data)
    let ern = this.generate(4);
    this.data.ern = ern;
    let order = {
      cantidad: this.data.detalle.length,
      precio: this.data.total,
      ern:ern,
      descripcion:`Venta por el total de ${this.data.detalle.length} artículos un total de Q ${this.data.total} `,
      url: "www.foxylabs.gt"
    }
    console.log(this.data)    
    if(this.data.cantidad >= 1) {
      if(this.data.formapago == '2') {
        this.salesService.pagar(order)
        .then(response2 => {
          console.log(response2)
          this.salesService.create(this.data)
          .then(res => {
            let url = response2.token;
            let parameter = {
              ern: ern,
              tokenID: ''
            }
            const browser = this.iab.create(url, '_blank', 'location=yes,clearsessioncache=yes,clearcache=yes');
            browser.on('loadstart').subscribe((e) => {
              this.url = e.url;
              //URL TOKEN NUEVO
              let tokenUrlComparation = e.url;
              //URL TOKEN A COMPARAR
              let token = this.url.replace('http://me.gtechnology.gt/home/cliente/pedidos/','');
              let ernReplace = '/' + ern;
              let tokenFinally = token.replace(ernReplace, '')
              parameter.tokenID = tokenFinally;
              let urlFinally = 'http://me.gtechnology.gt/home/cliente/pedidos/' + tokenFinally + ernReplace
              if(tokenUrlComparation == urlFinally) {
                //alert('CONGRATULATIONS')
                if(tokenFinally.length >= 30) {
                  this.navCtrl.setRoot(OrdersPage)
                  browser.close()
                  this.loading.create({
                    content: 'Procesando Compra...',
                    duration: 3000
                  }).present();
                  //let chooseModal = this.modal.create(dataOrdersClientPage, { parameter });
                  //chooseModal.present();
                } else {
                  this.loading.create({
                    content: 'Saliendo de la Compra...',
                    duration: 2000
                  }).present();
                  this.navCtrl.setRoot(CategoriesPage)
                }  
              }
            });
          }).catch(error => {
            console.log(error)
          });
        }).catch(error => {
          console.log(error)
        });
      }
    }
  }*/

  public generate(longitude)
  {
    let i:number
    var caracteres = "123456789+-*abcdefghijkmnpqrtuvwxyz123456789+-*ABCDEFGHIJKLMNPQRTUVWXYZ12346789+-*";
    var password = "";
    for (i=0; i<longitude; i++) password += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
    return '02' + password;
  }

  //MENSAJES
  public message(messages: any, duration:any) {
    this.toast.create({
      message: messages,
      duration: duration
    }).present();
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  //IMAGEN DE ORDEN
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg" || type == 'application/pdf') {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif');
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'orders'
          },
          function(respuesta) {
            if(type == 'application/pdf') {
              $('#imgAvatar').attr("src", respuesta.url);
              $('#imgAvatar2').show();
              $('#imgAvatar').hide();              
              $('#imgAvatar2').attr("src", 'https://developers.zamzar.com/assets/app/img/convert/pdf.png');
              $("#"+id).val('');
            } else {
              $('#imgAvatar').attr("src", respuesta.url);
              $("#"+id).val('');
            }
          }
        );
      } else {
        this.message('El archivo es demasiado grande.', 800)
      }
    } else {
      this.message('El tipo de archivo no es válido.', 800)
    }
  }

  //CARGAR LOS RETOS
  public getAllSecond() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAll()
    .then(response => {
      this.table = [];
      this.table = response;
      load.dismiss();
    }).catch(error => {
      load.dismiss();
      console.clear
    })
  }

  //GET SINGLE
  search() {
      let load = this.loading.create({
        content: 'Cargando...'
      });
      load.present();
      this.fourthService.search(path.id, this.data2.codigo, 'codigo')
      .then(response => {
        console.log(response)
        if(response) {
          if(response[0].cantidad > 0) {
            this.btnDisabled2 = true;
            this.data2 = response[0];
            this.data.unit_price =  this.data.unit_price - (+this.data.unit_price * (this.data2.latitude/100));;
            this.data.x_amount =  this.data.x_amount - (+this.data.x_amount * (this.data2.latitude/100));;
            this.data.total =  this.data.x_amount;
            this.confirmation('Código Encontrado', 'El código de descuento ha sido aplicado exitosamente su descuento es de '+ response[0].latitude.toString() + '%');
            this.data2.cantidad = this.data2.cantidad - 1;
          } else {
            this.confirmation('Código No Disponible', 'Ya no queda disponibilidad para canjear codigo.');
          }
          load.dismiss();
        }        
      }).catch(error => {
        load.dismiss();
      })
  }

  //UPDATE
  public updateDescuento() {
    this.fourthService.update(this.data2)
    .then((res) => {
      console.log(res)
    }).catch((error) => {
      console.log(error)
    })
  }
 
}