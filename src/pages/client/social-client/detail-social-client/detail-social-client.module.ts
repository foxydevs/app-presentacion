import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailSocialClientPage } from './detail-social-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailSocialClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailSocialClientPage),
    PipeModule
  ],
  exports: [
    DetailSocialClientPage
  ]
})
export class DetailSocialClientPageModule {}