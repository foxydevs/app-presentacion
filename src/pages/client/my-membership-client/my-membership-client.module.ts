import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyMembershipClientPage } from './my-membership-client';
import { PipeModule } from '../../../pipes/pipes.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { EncriptationService } from '../../../app/service/encriptation.service';

@NgModule({
  declarations: [
    MyMembershipClientPage,
  ],
  imports: [
    IonicPageModule.forChild(MyMembershipClientPage),
    PipeModule,
    NgxBarcodeModule
  ],
  exports: [
    MyMembershipClientPage
  ],
  providers: [
    EncriptationService
  ]
})
export class MyMembershipClientPageModule {}