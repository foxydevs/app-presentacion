import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogClientPage } from './blog-client';
 
@NgModule({
  declarations: [
    BlogClientPage,
  ],
  imports: [
    IonicPageModule.forChild(BlogClientPage),
  ],
  exports: [
    BlogClientPage
  ]
})
export class BlogClientPageModule {}