import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../pipes/pipes.module';
import { CallNumber } from '../../../../node_modules/@ionic-native/call-number';
import { AndroidPermissions } from '../../../../node_modules/@ionic-native/android-permissions';
import { InformationTwoClientPage } from './information-two-client';
 
@NgModule({
  declarations: [
    InformationTwoClientPage,
  ],
  imports: [
    IonicPageModule.forChild(InformationTwoClientPage),
    PipeModule,
  ],
  exports: [
    InformationTwoClientPage
  ],
  providers: [
    CallNumber,
    AndroidPermissions
  ]
})
export class InformationTwoClientPageModule {}