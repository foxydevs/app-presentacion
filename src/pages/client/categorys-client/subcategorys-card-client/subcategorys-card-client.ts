import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, IonicPage, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { CategorysService } from '../../../../app/service/categorys.service';
import { UsersService } from '../../../../app/service/users.service';
import { path } from '../../../../app/config.module';

@IonicPage()
@Component({
  selector: 'subcategorys-card-client',
  templateUrl: 'subcategorys-card-client.html'
})
export class SubCategorysCardClientPage {
  //Propiedades
  public categorys:any[] = [];
  public idUser:any;
  public pictureCategories:any;
  public parameter:any;
  public baseId = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  designCategory = localStorage.getItem('currentDesignCategory');
  subcategory = localStorage.getItem('currentOpcion21');

  constructor(
    public navCtrl: NavController,
    public categorysService: CategorysService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public usersService: UsersService,
    public modalController: ModalController,
    public navParams: NavParams
  ) {
    this.idUser = localStorage.getItem("currentId");
    this.parameter = this.navParams.get('parameter');
    if(localStorage.getItem('currentState') == '21') {
      this.openModalCreate();
    }
    this.loadSingleUser();
    this.getAll(this.parameter);
  }

  public ngOnInit() {
  }

  public loadSingleUser(){
    if((this.baseId+'')!='null'){
      this.usersService.getSingle(this.baseId)
      .then(res => {
        this.pictureCategories = res.pic2
        //console.clear();
      }).catch(error => {
        //console.clear();
      })
    }
  }

  //Cargar los productos
  public getAll(id:any){
    this.categorysService.getSingle(id)
    .then(res => {
      console.log(res)
      this.categorys = [];
      this.categorys = res.subcategorys;
    }).catch(error => {
      console.clear
    })
  }

  //Ver Productos de la Categoria
  public seeProducts(parameter:any) {
    this.loading.create({
        content: "Cargando",
        duration: 750
    }).present();
    this.navCtrl.push('ProductsClientPage', { parameter });
  }

  //OPEN MODAL
  public openModal(parameter:any) {
    let chooseModal = this.modalController.create('CategoryFormUserPage', { parameter });
    chooseModal.onDidDismiss((data) => {
      if(data!='Close') {
        this.getAll(this.parameter);
      }
    });
    chooseModal.present();
  }

  //Eliminar Productos
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: 'Eliminar Categoría',
      message: '¿Deseas eliminar la categoría?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.categorysService.delete(id)
            .then(res => {
                this.loading.create({
                  content: "Eliminando Categoría...",
                  duration: 2000
                }).present();
                this.getAll(this.parameter);
                console.clear();
            }).catch(error => {
                console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    this.getAll(this.parameter);
  }
  //ABIR MODAL AGREGAR
  public openModalCreate() {
    let parameter = {
      state: '2',
      id: null
    }
    let chooseModal = this.modalController.create('ProfileConfigurationUserPage', { parameter });
    chooseModal.present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.loadSingleUser();
        this.getAll(this.parameter);
      refresher.complete();
    }, 2000);
  }

  //SUMAR ORDEN
  addOrder(p:any) {
    console.log(p)

    for (var x in this.categorys) {
      if (this.categorys[x] == p) {
        this.categorys[x].orden = +this.categorys[x].orden + 1;
        this.update(this.categorys[x]);
        console.log(this.categorys[x].orden)
      }
    }
  }

  //SUMAR ORDEN
  removeOrder(p:any) {
    console.log(p)
    for (var x in this.categorys) {
      if (this.categorys[x] == p) {
        if(this.categorys[x].orden > 1) {
          this.categorys[x].orden = +this.categorys[x].orden - 1;
          this.update(this.categorys[x]);
          console.log(this.categorys[x].orden)
        }
      }
    }
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.categorysService.update(formValue)
    .then(res => {
      console.log(res)
        this.getAll(this.parameter);
    }).catch(error => {
      console.clear
    });
  }
}
