import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategorysCardClientPage } from './categorys-card-client';
 
@NgModule({
  declarations: [
    CategorysCardClientPage,
  ],
  imports: [
    IonicPageModule.forChild(CategorysCardClientPage),
  ],
  exports: [
    CategorysCardClientPage
  ]
})
export class CategorysCardClientPageModule {}