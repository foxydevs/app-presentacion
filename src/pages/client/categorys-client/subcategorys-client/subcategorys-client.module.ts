import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubCategorysClientPage } from './subcategorys-client';
 
@NgModule({
  declarations: [
    SubCategorysClientPage,
  ],
  imports: [
    IonicPageModule.forChild(SubCategorysClientPage),
  ],
  exports: [
    SubCategorysClientPage
  ]
})
export class SubCategorysClientPageModule {}