import { Component } from '@angular/core';
import { NavController, Platform, IonicPage} from 'ionic-angular';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { BenefitsService } from '../../../app/service/benefits.service';
import { CommerceService } from '../../../app/service/commerce.service';
import { path } from '../../../app/config.module';
import { AlertController } from '../../../../node_modules/ionic-angular/components/alert/alert-controller';
import { MembershipService } from '../../../app/service/membership.service';

@IonicPage()
@Component({
  selector: 'benefits-client',
  templateUrl: 'benefits-client.html',
})
export class BenefitsClientPage {
  //PROPIEDADES
  public table:any[] = [];
  public table2:any[] = [];
  public idUser:any = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  selectItem:any = 'beneficios';
  nivelMembresia = localStorage.getItem('currentNivelMembresia');
  membresiaClient = localStorage.getItem('currentMembresia');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl: AlertController,
    public mainService: BenefitsService,
    public secondService: CommerceService,
    public thirdService: MembershipService,
    public plt: Platform
  ) {
    this.thirdService.calculateMembership();
    //MEMBRESIA
    if(this.plt.is('android')) {
      if(+this.membresiaClient < +this.nivelMembresia) {
        this.confirmation('Información', 'Adquiere una membresía que mejor se adapte a tus necesidades para poder gozar de los beneficios de ZFIT.');
      }
    }
  }

  openForm(parameter?:any) {
    this.navCtrl.push('DetailBenefitsClientPage', { parameter });
  }

  openForm2(parameter?:any) {
    this.navCtrl.push('DetailCommerceClientPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR LOS RETOS
  public getAllSecond(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.secondService.getAllUser(id)
    .then(response => {
      this.table2 = []
      this.table2 = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CONFIRMACIÓN
  public confirmation = (title: any, message?:any) => {
    let confirm = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    if(this.selectItem == 'beneficios') {
      this.getAll(this.idUser);        
    } else {
      this.getAllSecond(this.idUser)
    }
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.selectItem == 'beneficios') {
        this.getAll(this.idUser);        
      } else {
        this.getAllSecond(this.idUser)
      }
      refresher.complete();
    }, 2000);
  }

}
