import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BenefitsClientPage } from './benefits-client';
 
@NgModule({
  declarations: [
    BenefitsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(BenefitsClientPage),
  ],
  exports: [
    BenefitsClientPage
  ]
})
export class BenefitsClientPageModule {}