/**
 * @author    Ionic Bucket <ionicbucket@gmail.com>
 * @copyright Copyright (c) 2018
 * @license   Fulcrumy
 * 
 * This file represents admob configuration.
 * File path - '../src/config/admob'
 */

export const AdmobConfig = {
    "androidBanner": "ca-app-pub-************************",
    "androidInterstitial": "ca-app-pub-************************",
    "androidVideo": "ca-app-pub-************************",
    "iosBanner": "ca-app-pub-************************",
    "iosInterstitial": "ca-app-pub-************************",
    "iosVideo": "ca-app-pub-************************"
}