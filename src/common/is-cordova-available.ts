export let isCordovaAvailable = () => {
	if (!(<any>window).cordova) {
		//alert('Esta es una característica nativa. Por favor use un dispositivo D:');
		return false;
	}
	return true;
};
