import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";


@Injectable()
export class UsersService {
	headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  public basePath:string = path.path;

  constructor(public http:Http) {
  }

  public handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //Obtener Todos los Usuarios
  public getAll():Promise<any> {
    let url = `${this.basePath}users`;
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Evento
  public getClients(id:number):Promise<any> {
    let url = `${this.basePath}user/clients/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Actualizar Contraseña
  public changePassword(form):Promise<any> {
    let url = `${this.basePath}users/${form.id}/changepassword`;
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Recuperar Contraseña users/password/reset
  public resetPassword(form):Promise<any> {
    let url = `${this.basePath}users/password/reset`;
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Evento
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}users/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Evento
  public findCommerce(id:string):Promise<any> {
    let url = `${this.basePath}find/users/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Create User
  public create(form):Promise<any> {
    let url = `${this.basePath}users`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Actualizar Categoria
  public update(form):Promise<any> {
    let url = `${this.basePath}users/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }
}