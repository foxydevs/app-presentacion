import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class AccessService {
  headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  public basePath:string = path.path;

  constructor(public http:Http){
  } 

  public handleError(error:any):Promise<any> {
  console.error("ha ocurrido un error")
  console.log(error)
  return Promise.reject(error.message || error)
  }

  public getAll():Promise<any> {
    let url = `${this.basePath}accesos`
      return this.http.get(url)
                      .toPromise()
                        .then(response => {
                          //console.log(response.json())
                          return response.json()
                        })
                        .catch(this.handleError)
  }

  public getModuleAccess(id:number,id2:number):Promise<any> {
      let url = `${this.basePath}users/${id}/modulos/${id2}`
        return this.http.get(url)
                        .toPromise()
                          .then(response => {
                            //console.log(response.json())
                            return response.json()
                          })
                          .catch(error => {
                            if(error.status==404){

                            }
                          })
  }

  public getModuleAccessSpecial(id:number):Promise<any> {
    let url = `${this.basePath}user/${id}/especial/modulos`
      return this.http.get(url)
                      .toPromise()
                        .then(response => {
                          //console.log(response.json())
                          return response.json()
                        })
                        .catch(this.handleError)
  }

  public getAccess(id:number):Promise<any> {
        let url = `${this.basePath}users/${id}/modulos`
          return this.http.get(url)
                          .toPromise()
                            .then(response => {
                              //console.log(response.json())
                              return response.json()
                            })
                            .catch(error => {
                              if(error.status==404){

                              }
                            })
  }

  public create(form):Promise<any> {
    let url = `${this.basePath}accesos`
      return this.http.post(url,form)
                      .toPromise()
                        .then(response => {
                          //console.log(response.json())
                          return response.json()
                        })
                        .catch(this.handleError)
  }

  public delete(id):Promise<any> {
    let url = `${this.basePath}accesos/${id}`
      return this.http.delete(url)
                      .toPromise()
                        .then(response => {
                          //console.log(response.json())
                          return response.json()
                        })
                        .catch(this.handleError)
  }

  public update(form):Promise<any> {
    let url = `${this.basePath}accesos/${form.id}`
      return this.http.put(url,form)
                      .toPromise()
                        .then(response => {
                          //console.log(response.json())
                          return response.json()
                        })
                        .catch(this.handleError)
  }

  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}accesos/${id}`
      return this.http.get(url)
                      .toPromise()
                        .then(response => {
                          //console.log(response.json())
                          return response.json()
                        })
                        .catch(this.handleError)
  }

}