import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class AdoptionCatsService {
  headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  public basePath:string = path.path;

  constructor(public http:Http) {
  }

  public handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //Obtener Todo
  public getAll():Promise<any> {
    let url = `${this.basePath}adopcionesgatos`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Categorias Por Usuario
  public getAllClients(id:any):Promise<any> {
    let url = `${this.basePath}clients/${id}/adopcionesgatos`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //Obtener Categorias Por Usuario
  public getAllUser(id:any):Promise<any> {
    let url = `${this.basePath}users/${id}/adopcionesgatos`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //GET USER STATE
  public getAllUserState(id:any, state:any):Promise<any> {
    let url = `${this.basePath}users/${id}/adopcionesgatos/${state}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //Crear Categoria 
  public create(form):Promise<any> {
    let url = `${this.basePath}adopcionesgatos`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Eliminar Categoria
  public delete(id):Promise<any> {
    let url = `${this.basePath}adopcionesgatos/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Actualizar Categoria
  public update(form):Promise<any> {
    let url = `${this.basePath}adopcionesgatos/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Categoria
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}adopcionesgatos/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Enviar Correo
  public sendEmail(id):Promise<any> {
    let url = `${this.basePath}users/${id}/print/adopcionesgatos`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response
      })
    .catch(this.handleError)
  }

}