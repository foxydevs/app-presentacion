import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs/Rx';

interface IUser {
  id: string;
  picture: string;
  email: string;
  firstname: string;
  lastname: string;
}
export const DUMMY_DATA = [];

@Injectable()
export class UserAuthService {
  public usersSubject = new BehaviorSubject([]);
  public users: IUser[];

  constructor() { }

  getUsers(): Observable<IUser[]> {
    return this.usersSubject.asObservable();
  }

  public refresh() {
    // Emitir los nuevos valores para que todos los que dependan se actualicen.
    this.usersSubject.next(this.users);
  }

  createNewUser(user: IUser) {
    /**
    * Evitar hacer this.user.push() pues estaríamos modificando los valores directamente,
    * se debe generar un nuevo array !!!!.
    */
    this.users = [...this.users, user];
    this.refresh();
  }

  loadDummyData() {
    this.users = DUMMY_DATA;
    this.refresh();
  }

}