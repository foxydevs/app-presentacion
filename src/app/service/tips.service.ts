import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class TipsService {
  headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  public basePath:string = path.path;

  constructor(public http:Http) {
  }

  public handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //OBTENER TODAS LAS TIPSS
  public getAll():Promise<any> {
    let url = `${this.basePath}posts`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //OBTENER TIPSS POR USUARIO
  public getAllUser(id:any):Promise<any> {
    let url = `${this.basePath}users/${id}/posts`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //AGREGAR TIPS
  public create(form):Promise<any> {
    let url = `${this.basePath}posts`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //ELIMINAR TIPS
  public delete(id):Promise<any> {
    let url = `${this.basePath}posts/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //ACTUALIZAR TIPS
  public update(form):Promise<any> {
    let url = `${this.basePath}posts/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //OBTENER UNA TIPS
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}posts/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //GET TIPS
  public getAllComments():Promise<any> {
    let url = `${this.basePath}commentsposts`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //CREAR TIPS
  public createComment(form):Promise<any> {
    let url = `${this.basePath}commentsposts`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //OBTENER COMENTARIOS POR TIPS
  public getAllCommentsByTip(id:any):Promise<any> {
    let url = `${this.basePath}comments/${id}/posts`
    return this.http.get(url)
    .toPromise()
    .then(response => {
      return response.json()
    })
    .catch(this.handleError)
  }

}
