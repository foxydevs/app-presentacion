import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class DocumentsService {
	headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  public basePath:string = path.path;

  constructor(public http:Http) {
  }

  public handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //Obtener Todo
  public getAll():Promise<any> {
    let url = `${this.basePath}documentos`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  public getAllComment():Promise<any> {
    let url = `${this.basePath}commentsdocumentos`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Productos Por Usuario
  public getAllUser(id:any):Promise<any> {
    let url = `${this.basePath}users/${id}/documentos`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
    })
    .catch(this.handleError)
  }

  //Crear Producto 
  public create(form):Promise<any> {
    let url = `${this.basePath}documentos`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Crear Comentario
  public createComment(form):Promise<any> {
    let url = `${this.basePath}commentsdocumentos`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Eliminar Producto
  public delete(id):Promise<any> {
    let url = `${this.basePath}documentos/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Actualizar Producto
  public update(form):Promise<any> {
    let url = `${this.basePath}documentos/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Producto
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}documentos/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //OBTENER COMENTARIOS POR PRODUCTO
  public getAllCommentsByProduct(id:any):Promise<any> {
    let url = `${this.basePath}comments/${id}/documentos`
    return this.http.get(url)
    .toPromise()
    .then(response => {
      return response.json()
    })
    .catch(this.handleError)
  }

}