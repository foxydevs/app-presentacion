import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";

import { path } from "../config.module";

import "rxjs/add/operator/toPromise";

@Injectable()
export class MessagesService {
	headers = new Headers({'Access-Control-Allow-Origin':'*',
  'cache-control':'no-cache',
  'server':'Apache/2.4.18 (Ubuntu)',
  'x-ratelimit-limit':'60',
  'x-ratelimit-remaining':'59'});
  public basePath:string = path.path;

  constructor(public http:Http) {
  }

  public handleError(error:any):Promise<any> {
    console.error("ha ocurrido un error")
    console.log(error)
    return Promise.reject(error.message || error)
  }

  //Obtener Todo
  public getAll():Promise<any> {
    let url = `${this.basePath}commetsusers`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Crear Mensaje 
  public create(form):Promise<any> {
    let url = `${this.basePath}commetsusers`
    return this.http.post(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Eliminar Mensaje
  public delete(id):Promise<any> {
    let url = `${this.basePath}events/${id}`
    return this.http.delete(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Actualizar Mensaje
  public update(form):Promise<any> {
    let url = `${this.basePath}events/${form.id}`
    return this.http.put(url,form)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Mensaje
  public getSingle(id:number):Promise<any> {
    let url = `${this.basePath}events/${id}`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Crear Mensaje 
  public upload(form):Promise<any> {
    let url = `${this.basePath}commentsusers/only/upload`
    return this.http.post(url, form)
    .toPromise()
      .then(response => {
        return response
      })
    .catch(this.handleError)
  }

  //Obtener Un Pedido
  public getUserReceipt(id:number):Promise<any> {
    let url = `${this.basePath}users/${id}/receipt`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }

  //Obtener Un Pedido
  public getUserSend(id:number):Promise<any> {
    let url = `${this.basePath}users/${id}/send`
    return this.http.get(url)
    .toPromise()
      .then(response => {
        return response.json()
      })
    .catch(this.handleError)
  }
}