/**
 * @author    Ionic Bucket <ionicbucket@gmail.com>
 * @copyright Copyright (c) 2017
 * @license   Fulcrumy
 * 
 * This file represents a provider of storage.
 * File path - '../../../src/providers/storage/storage'
 */

import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class StorageProvider {

  constructor(
    public storage: Storage) {
  }

  set(video) {
    let promise = new Promise((resolve, reject) => {
      this.storage.set('favorite', JSON.stringify(video));
      resolve('success');
    });
    return promise;
  }

  get() {
    let promise = new Promise((resolve, reject) => {
      this.storage.get('favorite').then((data) => {
        resolve(JSON.parse(data));
      }).catch(err => {
        reject(err);
      })
    });
    return promise;
  };

  remove() {
    let promise = new Promise((resolve, reject) => {
      this.storage.remove('favorite').then(() => {
        this.storage.clear();
        resolve('success');
      }).catch(err => {
        reject(err);
      });
    });
    return promise;
  }
}
